package org.linkedopenactors.rdfpub.client2024;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class NonReaktiveWebClientHttpAdapter implements HttpAdapter {
	private WebClient client;

	public NonReaktiveWebClientHttpAdapter(WebClient client) {
		this.client = client;
	}

	@Override
	public Response get(String idOfTheResourceToRead, HttpHeaderAttribute... headerAttributes) {
		return get(MEDIA_TYPE_JSON_LD, idOfTheResourceToRead, headerAttributes);
	}
	
	@Override
	public Response get(MediaType mediaType, String idOfTheResourceToRead, HttpHeaderAttribute... headerAttributes) {
		log.trace("httpGetApString(" + idOfTheResourceToRead+")");
		
		RequestHeadersSpec<?> request = client
				.get()
				.uri(idOfTheResourceToRead)
				.accept(mediaType)
				.header("profile", "https://www.w3.org/ns/activitystreams");
		
		Arrays.stream(headerAttributes)
			.forEach(attr->request.header(attr.getName(), attr.getValue()));
		
		handleRequestAndCorrelation(request);

		Response res = request.exchangeToMono(response -> {
			if (response.statusCode().isSameCodeAs(NOT_FOUND)) {
				return Mono.just(
						new Response(null, response.statusCode().value(), convert(response.headers().asHttpHeaders())));
			} else if (response.statusCode().is2xxSuccessful()) {
				return response.bodyToMono(String.class).map(body -> new Response(body, response.statusCode().value(),
						convert(response.headers().asHttpHeaders())));
			} else {
				return response.createException().flatMap(Mono::error);
			}
		}).block();
		return res;
	}


	private void handleRequestAndCorrelation(RequestHeadersSpec<?> request) {
		String corellationId = "X-Correlation-ID";
		String requestId = "X-Request-ID";
		Optional.ofNullable(MDC.get(corellationId))
			.ifPresent(x->request.header(corellationId, x));		
		Optional.ofNullable(MDC.get(requestId))
			.ifPresent(x->request.header(requestId, x));
	}

	@Override
	public Response post(RdfPubObjectId url, String body, org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat rdfFormat) {
		return post(url.getIRIString(), body, toMediaType(rdfFormat));
	}

	@Override
	public Response post(String url, String body, org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat rdfFormat) {
		return post(url, body, toMediaType(rdfFormat));
	}

	@Override
	public Response post(String url, String body) {
		return post(url, body, MEDIA_TYPE_JSON_LD);
	}

	@Override
	public Response post(String url, String body, MediaType mediaType) {
		RequestHeadersSpec<?> request = client
				.post()				
				.uri(url)
				.contentType(mediaType)
				.bodyValue(body)
				.accept(mediaType)
				.header("profile", "https://www.w3.org/ns/activitystreams");
				
				handleRequestAndCorrelation(request);
				
		Response res = request.exchangeToMono(response -> {
			log.debug("HTTP post to: " + url + " -> " + response.statusCode());
				if (response.statusCode().isSameCodeAs(org.springframework.http.HttpStatus.CREATED)) {
						HttpHeaders asHttpHeaders = response.headers().asHttpHeaders();
						Response data = new Response(null, response.statusCode().value(), convert(asHttpHeaders));
						return Mono.just(data);
				} else if (response.statusCode().is2xxSuccessful()) {
					Mono<Response>  s = response.bodyToMono(String.class)
							.map(y->{
								HttpHeaders asHttpHeaders = response.headers().asHttpHeaders();
								return new Response(y, response.statusCode().value(), convert(asHttpHeaders));
							});
					return s;
					}
				else {
					return response.createException().flatMap(Mono::error);
					}
				}).block();
		if(res==null) {
			log.error("wie kann das null sein ????");
			log.error("url: " + url);
			log.error("mediaType: " + mediaType);
			log.error("body: " + body );
		}
		return res;				
	}

	private Map<String, Object> convert(HttpHeaders httpHeaders) {
		Map<String, Object> headers = new HashMap<>();
		httpHeaders.forEach((k,v)->{
			headers.put(k, v);
		});
		return headers;
	}
	
	private MediaType toMediaType(RdfFormat rdfFormat) {
		switch (rdfFormat) {		
		case JSONLD: 
			return MEDIA_TYPE_JSON_LD;
		case RDFXML:
			new MediaType("application", "rdf+xml");
		case TRIG:
			new MediaType("application", "trig");
		case TURTLE:
			return MEDIA_TYPE_TURTLE;			
		default:
			throw new IllegalArgumentException("Unexpected value: " + rdfFormat);
		}
	}
}
