package org.linkedopenactors.rdfpub.client2024;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.core.oidc.StandardClaimNames;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

	public class HttpAdapterMvcMock implements HttpAdapter {

		private MockMvc mockMvc;
		
		private String issuer = "http://example.com";
		private String userId;
		private String preferredUsername;

		public HttpAdapterMvcMock(MockMvc mockMvc, String userId, String preferredUsername) {
			this.userId = userId;
			this.preferredUsername = preferredUsername;
			this.mockMvc = mockMvc;
		}

		private boolean isUserKnown() {
			return Optional.ofNullable(userId).isPresent() && Optional.ofNullable(preferredUsername).isPresent(); 
		}
		
		@Override
		public Response get(String url, HttpHeaderAttribute... headerAttributes) {
			return get(MediaType.parseMediaType("application/ld+json"), url, headerAttributes);
		}

		@Override
		public Response get(MediaType mediaType, String url, HttpHeaderAttribute... headerAttributes) {
			MvcResult mvcResult;
			try {
				MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url)
						.header("profile", "https://www.w3.org/ns/activitystreams")
						.accept(mediaType);
				
				if(isUserKnown()) {
					requestBuilder.with(jwt(userId, preferredUsername));
				}
				
				mvcResult = mockMvc.perform(requestBuilder)
						.andExpect(status().isOk())
						.andReturn();
				Response response = toResponse(mvcResult);
				return response;
			} catch (Exception e) {
				throw new IllegalStateException("unable to call url ("+url+")", e);
			}				
		}

		@Override
		public Response post(String url, String body) {
			return post(url, body, new MediaType("application", "ld+json"));
		}

		@Override
		public Response post(RdfPubObjectId url, String body, RdfFormat rdfFormat) {
			return post(url.getPath(), body, toMediaType(rdfFormat));
		}

		@Override
		public Response post(String url, String body, RdfFormat rdfFormat) {
			return post(url, body, toMediaType(rdfFormat));
		}

		@Override
		public Response post(String url, String body, MediaType mediaType) {
			MvcResult mvcResult;
			try {
				MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
						.with(jwt(userId, preferredUsername))
						.content(body)
						.contentType(mediaType);

				if(isUserKnown()) {
					requestBuilder.with(jwt(userId, preferredUsername));
				}

				mvcResult = mockMvc.perform(requestBuilder)
//						.andDo(print())
						.andReturn();

				Response response = toResponse(mvcResult);
				return response;
				
			} catch (Exception e) {
				throw new IllegalStateException("unable to call url("+url+")", e);
			}				
		}

		private MediaType toMediaType(RdfFormat rdfFormat) {
			switch (rdfFormat) {		
			case JSONLD: 
				return MEDIA_TYPE_JSON_LD;
			case RDFXML:
				new MediaType("application", "rdf+xml");
			case TRIG:
				new MediaType("application", "trig");
			case TURTLE:
				return MEDIA_TYPE_TURTLE;			
			default:
				throw new IllegalArgumentException("Unexpected value: " + rdfFormat);
			}
		}
		private Response toResponse(MvcResult mvcResult) throws UnsupportedEncodingException {
			Map<String,Object> headers = new HashMap<>();
			for (String key : mvcResult.getResponse().getHeaderNames()) {
				headers.put(key, mvcResult.getResponse().getHeader(key));
			}
			Response response = new Response(mvcResult.getResponse().getContentAsString(), mvcResult.getResponse().getStatus(), headers);
			return response;
		}

		private JwtRequestPostProcessor jwt(String userId, String preferredUsername) {
			return SecurityMockMvcRequestPostProcessors.jwt().jwt(j -> {					
				j.claim(StandardClaimNames.SUB, userId);
				j.claim(StandardClaimNames.PREFERRED_USERNAME, preferredUsername);
				j.claim("iss", issuer);
			});
		}
	}
