package org.linkedopenactors.rdfpub.store;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

/**
 * Manages multiple {@link ActivityPubStore} by there name. 
 */
public interface ActivityPubStoreRepository {

	/**
	 * Finds a {@link ActivityPubStore}.
	 * @param storeOwner
	 * @param preferredUsername
	 * @return The store for the passed actor.
	 */
	Optional<ActivityPubStore> findActivityPubStore(RdfPubIRI storeOwner, String preferredUsername);
	
	/**
	 * Creates an empty {@link ActivityPubStore}.
	 * @param actorId
	 * @param preferredUsername
	 * @return The created store for the passed storeOwner.
	 */
	ActivityPubStore createActivityPubStore(RdfPubIRI actorId, String preferredUsername);
	
	/**
	 * Releases the resources of this store 
	 */
	void shutdown();
}
