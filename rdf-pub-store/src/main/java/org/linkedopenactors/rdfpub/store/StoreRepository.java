package org.linkedopenactors.rdfpub.store;

import java.util.Optional;

import org.apache.commons.rdf.api.IRI;

/**
 * Manages multiple {@link Store} by there name. 
 */
public interface StoreRepository {

	/**
	 * Finds a store.
	 * @param datasetId The unique id of the dataset. 
	 * @param loggableStoreName
	 * @return The store for the passed datasetId. 
	 */
	Optional<Store> findStore(IRI datasetId, String loggableStoreName);
	
	/**
	 * Creates an empty store.
	 * @param datasetId
	 * @param loggableStoreName
	 * @return The created store for the passed datasetId.
	 */
	Store createStore(IRI datasetId, String loggableStoreName);

	/**
	 * Releases the resources of this store 
	 */
	void shutdown();
}
