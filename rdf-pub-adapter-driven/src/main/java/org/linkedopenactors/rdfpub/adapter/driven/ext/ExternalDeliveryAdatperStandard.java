package org.linkedopenactors.rdfpub.adapter.driven.ext;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class ExternalDeliveryAdatperStandard extends ExternalDeliveryAdatperAbstract {

	protected String determineKeyId(Actor outboxOwner) {
		return outboxOwner.getPublicKey().orElseThrow().asRdfPubObjectId().orElseThrow().getExternalSubject().getIRIString();
	}

	protected String activityToJsonBody(Actor outboxOwner, Activity activity) {
		log.debug("-> activityToJsonBody");
		return activity.toString(RdfFormat.JSONLD, true);
	}
}
