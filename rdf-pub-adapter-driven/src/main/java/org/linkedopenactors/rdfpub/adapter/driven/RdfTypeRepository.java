package org.linkedopenactors.rdfpub.adapter.driven;

//import java.util.Set;
//
//import org.eclipse.rdf4j.model.IRI;
//import org.eclipse.rdf4j.model.Model;
//import org.linkedopenactors.rdfpub.domain.RdfTypeService;
//
///**
// * To use RDF, for example, to reference types in SPARQL queries, these types must be known. Unfortunately, not all type descriptions are available on the Internet. 
// * Therefore the RdfTypeRepository offers the possibility to manage type definitions. The RdfTypeRepository is for instance administrators. 
// *  
// * @author naturzukunft@mastodon.social
// */
//public interface RdfTypeRepository extends /*SubclassValidator,*/ RdfTypeService {
//	/**
//	 * Adds the passed type definitions (RDF schema) to the passed connection unsing
//	 * the passe <a href=
//	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
//	 * 
//	 * @param definitionAsTurtle a RDF string that contains type definitions.
//	 * @param context See https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext
//	 * @return the IRI of the context
//	 */
//	IRI addTurtleTypeDefinition(String definitionAsTurtle, IRI context);
//
//	/**
//	 * Checks if the passed context is already managed. 
//	 * @param context The IRI of the context.
//	 * @return true, if the passed <a href=
//	 *         "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>
//	 *         is part of that repository, otherwise false.
//	 */
//	boolean containsContext(IRI context);
//
//	/**
//	 * Remove all {@link org.eclipse.rdf4j.model.Statement}s in the passed <a href=
//	 * "https://rdf4j.org/documentation/programming/repository/#using-named-graphscontext">context</a>.
//	 * @param context The IRI of the context.
//	 * @return false if the passed context is not available.
//	 */
//	boolean removeAll(IRI context);
//	
//	/**
//	 * Reads a definition.
//	 * @param context the subject/context of the definition.
//	 * @return The definition as Model (set of statements)
//	 */
//	Model read(IRI context);
//	
//	/**
//	 * Gives you all subjects from the passed model, that are derived (rdfs:subClassOf) from {@link de.naturzukunft.rdf4j.vocabulary.AS#Object}.
//	 * @param model the model to analyse
//	 * @return All subjects from the passed model, that are derived (rdfs:subClassOf) from {@link de.naturzukunft.rdf4j.vocabulary.AS#Object}.
//	 */
//	public Set<IRI> getAllSubjectsThatAreASubclassOfAsObject(Model model);
//	
//	/**
//	 * Checks, if the passed types contain a {@link IRI} that is a subClass of the passed expected type.
//	 * @param types the types to validate.
//	 * @param expected the type that is expected in the passed types.
//	 * @return True, if the passed types contain a {@link IRI} that is a subClass of the passed expected type, otherwise false.   
//	 */
//	public boolean isSubclassOf(Set<IRI> types, IRI expected);
//	
//	/**
//	 * Checks if the passed type is a subClass of the passed expected type.
//	 * @param type the type to validate. 
//	 * @param expected the type that is expected in the passed types.
//	 * @return True, if the passed type is a subClass of the passed expected type, otherwise false.
//	 */
//	public boolean isSubclassOf(IRI type, IRI expected);
//	
//	/**
//	 * Gives you a {@link Set} of all managed type definitions. 
//	 * @return A {@link Set} of all managed type definitions.
//	 */
//	Set<IRI> getContextsOfAddedTypeDefinitions();
//	
//	/**
//	 * Returns the context by collecting all added contexts. 
//	 * @return The context by collecting all added contexts. 
//	 */
//	IRI getInternalTypeContext();
//}
