package org.linkedopenactors.rdfpub.adapter.driven.ext;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.domain.Actor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import lombok.extern.slf4j.Slf4j;
import reactor.util.retry.Retry;

@Slf4j
@Component
class SoftwareDeterminator {
	
	private WebClient webClient;

	public SoftwareDeterminator(WebClient webClient) {
		this.webClient = webClient;
	}
	
	public SupportedSoftware findSoftware(Actor receiver) {
		
		URL receiverUrl = toUrl(receiver.getSubject().getIRIString());
		return getNodeInfoUlr(receiverUrl)
			.flatMap(u-> {
				return httpGet(u.toString());
			}).map(nodeInfoJson->{
				SupportedSoftware sv = SupportedSoftware.unknown;
				DocumentContext parsed = JsonPath.parse(nodeInfoJson);
				String name = parsed.read("$['software']['name']");
				String nameAndVersion = name + "_" + parsed.read("$['software']['version']");
				List<String> supportedSoftwareList = Arrays.asList(SupportedSoftware.values()).stream()
						.map(Enum::name)
						.collect(Collectors.toList());
				if( supportedSoftwareList.contains(nameAndVersion) ) {
					sv = SupportedSoftware.valueOf(nameAndVersion);
				} else if( supportedSoftwareList.contains(name) ) {
					sv = SupportedSoftware.valueOf(name);
				}					
				return sv;
			}).orElse(SupportedSoftware.unknown);
	}
	
	private Optional<URL> getNodeInfoUlr(URL receiverUrl)  {
		Optional<URL> result = httpGet(determinateWellKnownNodeInfoUrl(receiverUrl)).map(responseString->{
			String url = JsonPath.parse(responseString).read("$['links'][0]['href']");
			return toUrl(url);
		});
		return result;
	}

	private String determinateWellKnownNodeInfoUrl(URL receiverUrl) {
		String wellKnownNodeinfoUrlAsString = "uninitialized";
		String path = receiverUrl.getPath();
		path = receiverUrl.toString().replace(path, "");
		wellKnownNodeinfoUrlAsString = path + "/.well-known/nodeinfo";
		return wellKnownNodeinfoUrlAsString;
	}

	private Optional<String> httpGet(String wellKnownNodeinfoUrlAsString) {
		var request =
                webClient
                        .get()
                        .uri(toUrl(wellKnownNodeinfoUrlAsString).toString());
		String responseString = null;
        try {
			responseString = request
			        .retrieve()
			        .onStatus(org.springframework.http.HttpStatusCode::isError, ClientResponse::createException)
			        .bodyToMono(String.class)
			        .retryWhen(Retry.backoff(1, Duration.ofSeconds(2))
			                .filter(throwable -> throwable instanceof ResponseStatusException))
			        .block();
		} catch (WebClientResponseException e) {
			log.error("ResponseBodyAsString: " + e.getResponseBodyAsString());
			e.printStackTrace();
		}
		return Optional.ofNullable(responseString);
	}
	
	private URL toUrl(String urlAsString) {
		try {
			return URI.create(urlAsString).toURL();
		} catch (MalformedURLException e) {
			throw new IllegalStateException("no url: " + urlAsString);
		}
	}
}
