package org.linkedopenactors.rdfpub.adapter.driven.ext;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.Actor;

import lombok.Data;

@Data
class CmdExternalDelivery {
	private final Actor outboxOwner;
	private final Actor receiver;
	private final Activity activity;
}
