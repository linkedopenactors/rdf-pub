package org.linkedopenactors.rdfpub.adapter.driven.ext;

import java.net.URI;
import java.net.URL;
import java.security.PrivateKey;
import java.time.Duration;
import java.util.Map;

import org.linkedopenactors.rdfpub.adapter.http.FredysSignature;
import org.linkedopenactors.rdfpub.adapter.http.HttpHeaders;
import org.linkedopenactors.rdfpub.app.service.actor.KeyPairGenerator;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;
import reactor.util.retry.Retry;

@Slf4j
@Component
abstract class ExternalDeliveryAdatperAbstract  implements ExternalDeliveryAdatper {

	@Autowired
	private KeyPairGenerator keyPairGenerator;

	@Autowired
	private WebClient webClient;
	
	@Autowired
	private FredysSignature fredysSignature;

	public void deliver(CmdExternalDelivery cmd) {
		String receiversInbox = cmd.getReceiver().getInbox().getIRIString();
		Actor outboxOwner = cmd.getOutboxOwner();
		URL url;
		try {
			url = new URI(receiversInbox).toURL();
		} catch (Exception e) {
			throw new IllegalStateException("unable to make URL from receiversInbox: " + receiversInbox, e);
		}
		RdfPubObjectIdActor outbowOwnerId = outboxOwner.getSubject().asActor().orElseThrow();
		PrivateKey privateKey = keyPairGenerator.getPrivateKey(outbowOwnerId.getIdentifier());
		
		String keyId = determineKeyId(outboxOwner);
		String jsonLdBody = activityToJsonBody(outboxOwner, cmd.getActivity());
		org.linkedopenactors.rdfpub.adapter.http.HttpHeaders headers = fredysSignature.getSignatureHeadersPost(keyId, privateKey, url, jsonLdBody);
		Map<String, String> signatureHeaders = headers.asMap();
		
		deliver(signatureHeaders, receiversInbox, jsonLdBody);
	}

    protected abstract String activityToJsonBody(Actor outboxOwner, Activity activity);

	protected abstract String determineKeyId(Actor outboxOwner);

	private void deliver(Map<String, String> signatureHeaders, String receiversInbox, String jsonLdBody) {    	

		log.debug("deliver - jsonLdBody: " + jsonLdBody);
        var request = webClient
        		.post()
                .uri(receiversInbox);
        
        addSignatureHeaders(signatureHeaders, request);
        
        try {
        	ResponseEntity<String> responseEntity = request
					.body(BodyInserters.fromValue(jsonLdBody))
			        .retrieve()
			        .onStatus(org.springframework.http.HttpStatusCode::isError, ClientResponse::createException)
			        .toEntity(String.class)
			        .retryWhen(Retry.backoff(1, Duration.ofSeconds(2))
			                .filter(throwable -> throwable instanceof ResponseStatusException))
			        .block();
			log.debug("deliver ("+receiversInbox+") - response ("+responseEntity.getStatusCode()+"): " + responseEntity.toString());
		} catch (WebClientResponseException e) {
	        log.error("signature headers:");
	        signatureHeaders
	    		.forEach((k,v) -> log.error(k + " -> " + v));
			log.error("ResponseBodyAsString: " + e.getResponseBodyAsString());
			e.printStackTrace();
		}
    }

	private void addSignatureHeaders(Map<String, String> signatureHeaders, RequestBodySpec request) {
		log.debug("signature headers:");
        signatureHeaders
    		.forEach((k,v) -> log.debug(k + " -> " + v));
        
        request.header(HttpHeaders.CONTENT_TYPE, "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"");
        
        signatureHeaders
        	.forEach((k,v) -> request.header(k, v));
	}	
}
