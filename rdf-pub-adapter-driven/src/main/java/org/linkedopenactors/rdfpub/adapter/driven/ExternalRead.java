package org.linkedopenactors.rdfpub.adapter.driven;

import java.time.Duration;

import org.linkedopenactors.rdfpub.app.activity.DetermineReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;
import reactor.util.retry.Retry;

@Service
@Slf4j
public class ExternalRead //implements //org.linkedopenactors.rdfpub.app.activity.ExternalDelivery,
//		ApplicationListener<CreateActivityOutboxReceived> 
{

	@Autowired
	private DetermineReceiverService determineReceiverService; 

//	@Autowired
//	private HttpSignature httpSignature;
	
	@Autowired
	private WebClient webClient;
	
	@Value("${instance.domain}") 
	private String instanceDomain;
	
//	@Override
//	public void onApplicationEvent(CreateActivityOutboxReceived event) {
		public void onApplicationEvent(org.linkedopenactors.rdfpub.domain.Activity activity, org.linkedopenactors.rdfpub.domain.Actor actor) {
//		org.linkedopenactors.rdfpub.domain.commonsrdf.Activity activity =  event.getActivity();
//		ActivityPubObject object = event.getObject();
		
//		String body = activityPubObjectTranslater.translate(activity, object);
		String body = activity.toString();
//		org.linkedopenactors.rdfpub.domain.commonsrdf.Actor actor = event.getActor();
		determineReceiverService.getExternalReceiverInboxIris(activity).forEach(receiver->{
			deliver(actor.getSubject().toString(), actor.getSubject().toString(), receiver.toString(), body);	
		});		
	}
		
    private void deliver(String actorsPublicKeyId, String actorsIdentifier, String inbox, String body) {    	

    	actorsPublicKeyId = actorsPublicKeyId + "/publicKey";
    	// TODO ^^ i did not like this distributed url thing, how do we solve this ??
    	
//      inbox = "http://pasture_http_signature/";
        
//        var signatureHeaders = "TODO";// TODO httpSignature.getSignatureHeaders(actorsPublicKeyId, actorsPublicKeyId, inbox, actorsPublicKeyId, HttpMethod.POST);
        
        var request =
                webClient
                        .post()
                        .uri(inbox);

//        signatureHeaders
//        	.forEach((k,v) -> request.header(k, v));

        String responseString = "";
        try {
			responseString = request
					.body(BodyInserters.fromValue(body))
			        .retrieve()
			        .onStatus(org.springframework.http.HttpStatusCode::isError, ClientResponse::createException)
			        .bodyToMono(String.class)
			        .retryWhen(Retry.backoff(1, Duration.ofSeconds(2))
			                .filter(throwable -> throwable instanceof ResponseStatusException))
			        .block();
		} catch (WebClientResponseException e) {
			log.debug("ResponseBodyAsString: " + e.getResponseBodyAsString());
			e.printStackTrace();
		}

        log.debug("response: " + responseString);
        
    }
}
