package org.linkedopenactors.rdfpub.adapter.driven;

import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;

public enum Prefix {

	RDF(org.eclipse.rdf4j.model.vocabulary.RDF.PREFIX, org.eclipse.rdf4j.model.vocabulary.RDF.NAMESPACE),
	SCHEMA(VocSchema.PREFIX, VocSchema.NS),
	AS(VocAs.PREFIX, VocAs.NS),
	OWL(org.eclipse.rdf4j.model.vocabulary.OWL.PREFIX, org.eclipse.rdf4j.model.vocabulary.OWL.NAMESPACE),
	XML("xml", "http://www.w3.org/XML/1998/namespace"),
	XSD("xsd", "http://www.w3.org/2001/XMLSchema#"),
	RDFS(org.eclipse.rdf4j.model.vocabulary.RDFS.PREFIX, org.eclipse.rdf4j.model.vocabulary.RDFS.NAMESPACE);
	private String prefix;

	private Prefix(String prefix, String namespace) {
		this.prefix = "PREFIX "+prefix+": <"+namespace+">";
	}
	
	public String value() {
		return prefix;
	}
}
