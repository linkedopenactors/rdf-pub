package org.linkedopenactors.rdfpub.adapter.http;

public enum HttpMethod {
	POST, PUT, DELETE, GET;

}
