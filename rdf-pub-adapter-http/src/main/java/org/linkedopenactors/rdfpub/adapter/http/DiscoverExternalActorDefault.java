package org.linkedopenactors.rdfpub.adapter.http;

import java.security.PublicKey;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.app.external.DiscoverExternalActor;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DiscoverExternalActorDefault implements DiscoverExternalActor {

//	private ActivityPubObjectFactory activityPubObjectFactory;
//	
//	private ObjectMapper mapper;

//	private WebClient webClient;

	private ExternalObjectRepository externalObjectRepository;
	private VocabContainer vocabContainer;

	public DiscoverExternalActorDefault(//ActivityPubObjectFactory activityPubObjectFactory, //WebClient webClient,
			VocabContainer vocabContainerParam,
			ExternalObjectRepository externalObjectRepository) {
		this.vocabContainer = vocabContainerParam;
		//		this.activityPubObjectFactory = activityPubObjectFactory;
//		this.webClient = webClient;
		this.externalObjectRepository = externalObjectRepository;
//		mapper = new ObjectMapper();
	}
	
	@Override
	public Optional<Actor> discover(String identifier) {
		if(identifier.startsWith("http")) {
			return discoverByUrl(identifier);
		} else {
			return discoverByUrl(webfinger(identifier));
		}
    }

	@Override
	public Optional<PublicKey> discoverPublicKey(String keyIdOfSignature) {
//		Optional<PublicKey> publicKeyResponse = Optional.empty();
		
		return externalObjectRepository.readWithInstanceSignature(keyIdOfSignature)
				.map(this::toPublicKey)
				.flatMap(org.linkedopenactors.rdfpub.domain.PublicKey::getPublicKeyPem);
		
//		Optional<PublicKey> publicKeyResponse = externalObjectRepository.readWithInstanceSignature(keyIdOfSignature)
//				.map(ActivityPubObject::asConvertable)
//				.map(ActivityPubObjectConvertable::asPublicKey)
//				.flatMap(org.linkedopenactors.rdfpub.domain.PublicKey::getPublicKeyPem);

		
//		ResponseEntity<String> response = httpGet(keyIdOfSignature);
//		if(response.getStatusCode().isError()) {
//			throw new IllegalStateException("unable to reading " + keyIdOfSignature + " -> status: " + response.getStatusCode() + ", body: " + response.getBody());
//		}
		
//		String httpResponse = response.getBody();
//		if(!StringUtils.hasText(httpResponse)) {
//			return Optional.empty();
//		}
//		log.debug("httpResponse: " + httpResponse);		
		// Try to interpret is as an Actor
//		try {
//			publicKeyResponse = convertToActor(httpResponse)
//					.resolvePublicKey()
//					.flatMap(org.linkedopenactors.rdfpub.domain.PublicKey::getPublicKeyPem);
//			
//		} catch(Throwable th) {
//			// Last chance, try raw key.
//			PublicKey pk = NativePublicKey.readX509PublicKey(httpResponse);
//			publicKeyResponse = Optional.ofNullable(pk);
//		}			
//		return publicKeyResponse;
	}

	private String webfinger(String identifier) {
		identifier = Optional.ofNullable(identifier)
				.orElseThrow(()->new IllegalStateException("identifier is mandatory"));
		if(!identifier.startsWith("@")) {
			identifier = "@" + identifier;
		}
		String host = identifier.substring(identifier.lastIndexOf("@"));
		
		String url = "https://" + host + "/.well-known/webfinger?resource=acct:"+identifier;
		
		// TODO we have to get the actors url/subject  by the webfinger result !!! ;-)
		
		return url;
	}

	private Optional<Actor> discoverByUrl(String url) {
//		ResponseEntity<String> httpGetResponse;
//		try {
//				httpGetResponse = httpGet(url);
		
				return externalObjectRepository.readWithInstanceSignature(url)
						.map(ActivityPubObject::asConvertable)
						.map(ActivityPubObjectConvertable::asActor);
//			
//			
//		      ObjectMapper mapper = new ObjectMapper();
//		      Object json = mapper.readTree(httpGetResponse.getBody());
//		      String jsonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
//		      log.debug("httpGetResponse: " + jsonStr);
//		      
//		      
//		} catch (Exception e) {
//			log.warn("ignoring exception while reading url: " + url, e);
//			return Optional.empty();
//		}
//		
//		if(HttpStatus.GONE.equals(httpGetResponse.getStatusCode())) {
//			log.warn("TODO: 410 GONE not yet implemented!");
//			return Optional.empty();
//		}
//
//		if(HttpStatus.NOT_FOUND.equals(httpGetResponse.getStatusCode())) {
//			return Optional.empty();
//		}
//
//		if(httpGetResponse.getStatusCode().isError()) {
//			throw new IllegalStateException("unable to reading " + url + " -> status: " + httpGetResponse.getStatusCode() + ", body: " + httpGetResponse.getBody());
//		}
//
//		return Optional.of(convertToActor(httpGetResponse.getBody()));
	}

	
	private org.linkedopenactors.rdfpub.domain.PublicKey toPublicKey(ActivityPubObject ao) {
		if(ao.getTypes().contains(vocabContainer.vocAs().Person())
				|| ao.getTypes().contains(vocabContainer.vocAs().Application())
				|| ao.getTypes().contains(vocabContainer.vocAs().Service())
				|| ao.getTypes().contains(vocabContainer.vocAs().Group())
				|| ao.getTypes().contains(vocabContainer.vocAs().Organization())) {
			return ao.asConvertable().asActor().resolvePublicKey().orElseThrow(
					() -> new IllegalStateException("public key not resolvable for " + ao.getSubject().getIRIString()));
		} else { // Public Key ActivityPubObject is of rdf type Object, so we cannot validate if it is a pk here !
			return ao.asConvertable().asPublicKey();
		}
	}
//	private Actor convertToActor(String httpResponse) {
//		String subject = extractSubject(httpResponse);
//		log.debug(subject);
//		return activityPubObjectFactory.create(RdfFormat.JSONLD, httpResponse, subject)
//				.asConvertable()
//				.asActor();
//	}

//	private ResponseEntity<String> httpGet(String url) {
//		
//		ResponseEntity<String> responseEntity;
//		try {
//			responseEntity = webClient.get()
//				      .uri(url)
//				      .accept(MediaType.APPLICATION_JSON)
//				      .retrieve()
//				      .toEntity(String.class)
//				      .onErrorResume(Mono::error)
//				      .block();
//		} catch (WebClientResponseException e) {
//			return new ResponseEntity<String>(e.getMessage(), e.getStatusCode());
//		}
//		return responseEntity;		
//	}

//	private String extractSubject(String response) {		
//        String subject = "";
//        JsonNode node;
//		try {
//			node = mapper.readTree(response);
//			subject = node.get("id").asText();
//		} catch (JsonProcessingException e) {
//			throw new IllegalStateException("unable to extractSubject", e);
//		}
//		return subject;
//	}
}
