package org.linkedopenactors.rdfpub.adapter.http.ext;

import org.linkedopenactors.rdfpub.domain.Actor;

public interface SoftwareDeterminator {
	SupportedSoftware findSoftware(Actor receiver);
}