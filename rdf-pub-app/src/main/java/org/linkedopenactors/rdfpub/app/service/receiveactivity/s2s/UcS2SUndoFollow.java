package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

/**
 * An external actor want's to undo a follow request to an actor on this server is receiver. 
 */
class UcS2SUndoFollow extends UcS2SSignatureVerfified {
	
	private InboxUseCaseContext ucCtx;
	private boolean internalDelivery;

	public UcS2SUndoFollow(InboxUseCaseContext ucCtx) {
		this(ucCtx, false);
	}
	
	public UcS2SUndoFollow(InboxUseCaseContext ucCtx, boolean internalDelivery) {
		super(ucCtx.getSignatureVerifier());
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	public void process() throws BadRequestException {
		if(!internalDelivery) {
			verify();
		}
		ucCtx.getReceiverStore().addToInbox(ucCtx.getActivity(), "todo");
		
		ucCtx.getPendingFollowerCache().removePendingFollowActivity(getActivityIriToRemove());
		RdfPubIRI actorIriToRemove = getActorIriToRemove();

		ucCtx.getReceiverStore().removeFollower(actorIriToRemove);
	}
	
	private RdfPubBlankNodeOrIRI getActivityIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		RdfPubBlankNodeOrIRI activityIriToRemove = activity.getSubject();
		return activityIriToRemove;
	}
	
	private RdfPubIRI getActorIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		RdfPubIRI objectIri = activity.getActor().stream().findFirst().orElseThrow();
		return objectIri;
	}
}
