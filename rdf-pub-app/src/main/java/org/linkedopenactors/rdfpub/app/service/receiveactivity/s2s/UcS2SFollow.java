package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import java.util.Set;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

import lombok.extern.slf4j.Slf4j;

/**
 * An external actor want's to follow an actor on this server. 
 */
@Slf4j
class UcS2SFollow extends UcS2SSignatureVerfified {

	private InboxUseCaseContext ucCtx;
	private boolean internalDelivery;

	public UcS2SFollow(InboxUseCaseContext ucCtx) {
		this(ucCtx, false);
	}
	
	public UcS2SFollow(InboxUseCaseContext ucCtx, boolean internalDelivery) {
		super(ucCtx.getSignatureVerifier());
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	@Override
	public void process() throws BadRequestException {
		if(!internalDelivery) {
			verify();
		}
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// TODO check, if it's already following !!!!
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		boolean isPendingexists = ucCtx.getPendingFollowerCache().contains(ucCtx.getActivity());
		log.warn(ucCtx.getActivity().getActor() + " is already a PendingFollower!");
//		ucCtx.getRecipientActorStore().getFollowers();
		
		ucCtx.getReceiverStore().addToInbox(ucCtx.getActivity(), "todo"); // TODO do we want to have this activities in the inbox ?
		
		Set<RdfPubBlankNodeOrIRI> objects = ucCtx.getActivity().getObject();
		if(objects.size()!=1) {
			throw new IllegalArgumentException("one actor as object expected! activity: \n" + ucCtx.getActivity());
		}
		ucCtx.getPendingFollowerCache().addPendingFollowActivity(ucCtx.getActivity());
		ucCtx.getPendingFollowerCache().getPendingFollower().forEach(pf->log.debug("PendingFollower: " + pf));
		log.debug("");
	}
	
}
