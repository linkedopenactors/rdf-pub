package org.linkedopenactors.rdfpub.app.object;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CmdSparqlPublic {
	private final String query;
	private final String acceptHeader;
}
