package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.GoneException;

/**
 * The service that handles S2S interactions.
 */
public interface InboxReceiveActivityService {
	/**
	 * This method is designed for call from the http controller.
	 * @param cmd The incomming data.
	 * @throws BadRequestException if we cannot understand / process the request.
	 * @throws GoneException If there is no recipientActorId in the systemn
	 */
	void perform(CmdReceiveActivityInbox cmd) throws BadRequestException, GoneException;
	
	/**
	 * This method is designed for internal delivery.
	 * @param cmd The incomming data
	 * @throws BadRequestException if we cannot understand / process the request.
	 * @throws GoneException If there is no recipientActorId in the systemn 
	 */
	void perform(CmdReceiveActivityInboxTyped cmd) throws BadRequestException, GoneException;
}
