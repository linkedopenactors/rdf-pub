package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;

/**
 * An external actor updates an object and an actor on this server is receiver. 
 */
class UcS2SUpdateObject extends UcS2SSignatureVerfified {
	
	public UcS2SUpdateObject(InboxUseCaseContext ucCtx) {
		super(ucCtx.getSignatureVerifier());
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	@Override
	public void process() throws BadRequestException {
		verify();
	}
}
