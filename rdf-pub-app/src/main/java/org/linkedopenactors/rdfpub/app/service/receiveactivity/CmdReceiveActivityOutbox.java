package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;

import lombok.ToString;

/**
 * Command Object, that holds the data, that was received by an http controller.
 */
@ToString
public class CmdReceiveActivityOutbox extends CmdQueryBaseAbstract {
	
	/**
	 * The URL of the actor to whom the activity was sent. Also known as inbox owner.
	 */
	private final String recipientActorsId;
	
	/**
	 * The received activity as String. The HTTP Payload / Body.
	 */
	private final String receivedActivity;
	
	/**
	 * The {@link RdfFormat} of the payload, this is determined on the basis of the HTTP content type.
	 */
	private final RdfFormat rdfFormat;

	public CmdReceiveActivityOutbox(AuthenticatedActorHolder authenticatedActorHolder, String recipientActorsId,
			String receivedActivity, RdfFormat rdfFormat, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.recipientActorsId = recipientActorsId;
		this.receivedActivity = receivedActivity;
		this.rdfFormat = rdfFormat;
	}

	public String getRecipientActorsId() {
		return recipientActorsId;
	}

	public String getReceivedActivity() {
		return receivedActivity;
	}

	public RdfFormat getRdfFormat() {
		return rdfFormat;
	}
}
