package org.linkedopenactors.rdfpub.app;

import java.util.Optional;

import org.slf4j.MDC;

public class RdfPubMDC {
	
	private static final String ucKey = "uc" ;
	
	public static void put(String uc) {
		String newOne = Optional.ofNullable(MDC.get(ucKey)).map(existing-> existing + "." + uc).orElse(uc);
		MDC.put(ucKey, newOne);
	}
	
	public static void remove() {		
		Optional<String> existing = Optional.ofNullable(MDC.get(ucKey));
		if(existing.isPresent() && existing.get().contains(".")) {
			String old = existing.get().substring(0, existing.get().lastIndexOf("."));
			MDC.put(ucKey, old);
		} else {
			MDC.remove(ucKey);
		}
	}
}
