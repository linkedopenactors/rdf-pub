package org.linkedopenactors.rdfpub.app.service.actor;

import java.net.URI;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CmdCreatePerson {

	private RdfPubObjectId actorId;
	private String issuerUserId;
	private String issuerPreferredUsername;
	private URI issuer;
	private String logMsg;

	public CmdCreatePerson(RdfPubObjectId actorId,
			String issuerUserId, String issuerPreferredUsername, URI issuer, String logMsg) {
		this.actorId = actorId;
		this.issuerUserId = issuerUserId;
		this.issuerPreferredUsername = issuerPreferredUsername;
		this.issuer = issuer;
		this.logMsg = logMsg;
	}
}
