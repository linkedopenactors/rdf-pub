package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;

import lombok.Data;

/**
 * Command Object, that holds the data, that was received by an http controller.
 */
@Data
public class CmdReceiveActivityInbox {
    /**
     * Fully qualified name of the address returned by getRemoteHost(). 
     * Or, if the remote engine cannot or chooses not to resolve, the hostname, 
     * this is the IP address.
     */
    public final String remoteHost;

	/**
	 * The URL of the actor to whom the activity was sent. Also known as inbox owner.
	 */
	private final String recipientActorsId;
	
	/**
	 * The received activity as String. The HTTP Payload / Body.
	 */
	private final String receivedActivity;
	
	/**
	 * The {@link RdfFormat} of the payload, this is determined on the basis of the HTTP content type.
	 */
	private final RdfFormat rdfFormat;
	
	/**
	 * An instance of a {@link SignatureVerifier} that holds all data, that is needed for HTTPSignature validation.
	 * The validation is done in the use case, as it can only be determined in the useCase whether a signature needs to be verified.  
	 */
	private final SignatureVerifier signatureVerifier;
}
