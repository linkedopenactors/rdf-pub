package org.linkedopenactors.rdfpub.app.service.readcollection;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

import lombok.Data;

/**
 * Query object.
 */
@Data
public class QueryReadCollection {
	private Integer pageSize;
	private Integer startIndex;
	private RdfPubObjectIdCollection collectionInfo;
}
