package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.app.BadRequestException;

/**
 * Interface for S2S (inbox) UseCases. 
 */
public interface InboxUsecaseProcessor {

	/**
	 * Executes the useCase.
	 * @throws BadRequestException
	 */
	void process() throws BadRequestException;
}
