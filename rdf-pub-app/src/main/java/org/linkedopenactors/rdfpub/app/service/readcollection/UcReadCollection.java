package org.linkedopenactors.rdfpub.app.service.readcollection;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.readobject.QueryReadObject;
import org.linkedopenactors.rdfpub.app.service.readobject.UcReadObject;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UcReadCollection {

	@Autowired
	private PublicActorStore publicActorStore;;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private UcReadObject ucReadObject;
	
	@Autowired
	private VocabContainer vocabContainer;
	
	/**
	 * If {@link VocAs#Public()} is requested it will be returned, otherwise an empty collection will be returned.
	 * @param query
	 * @return If {@link VocAs#Public()} is requested it will be returned, otherwise an empty collection will be returned.
	 */
	public org.linkedopenactors.rdfpub.domain.OrderedCollectionPage perform(QueryReadCollection query) {
		RdfPubMDC.put(this.getClass().getSimpleName());
		try {
			OrderedCollectionPage found; 
			if (!isPublic(query)) {
				logUseCase("EMPTY");
				found = publicActorStore.getEmptyCollection(query.getStartIndex(), query.getPageSize());				
			} else {
				logUseCase("PUBLIC");
				found = publicActorStore.getPublicCollection(query.getStartIndex(), query.getPageSize());
			} 
			log.debug("found: " + found.totalItems() + " for: " + query.getCollectionInfo());
			return found;
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	/**
	 * If {@link VocAs#Public()} is requested it will be returned, otherwise an empty collection will be returned.
	 * @param query
	 * @return If {@link VocAs#Public()} is requested it will be returned, otherwise an empty collection will be returned.
	 */
	public org.linkedopenactors.rdfpub.domain.OrderedCollectionPage perform(QueryReadCollection4Actor query) {
		RdfPubMDC.put(this.getClass().getSimpleName());
		try {
			OrderedCollectionPage found = performInternal(query);
			log.debug("found: " + found.totalItems() + " for: " + query.getCollectionInfo());
			return found;
		} finally {
			RdfPubMDC.remove();
		}	
	}
		
	private org.linkedopenactors.rdfpub.domain.OrderedCollectionPage performInternal(QueryReadCollection4Actor query) {
		Optional<AuthenticatedActor> authenticatedActor = query.getAuthenticatedActorHolder().getAuthenticatedActor();
		Actor actor = authenticatedActor.map(AuthenticatedActor::getActor).orElse(null); 
		Optional<RdfPubObjectIdCollection> collectionOpt = getCollectionId(actor, query.getCollectionId());
		logUseCase(collectionOpt
				.map(RdfPubObjectIdCollection::getIRIString)
				.orElse(query.getCollectionId()));
		RdfPubObjectIdCollection collectionIri = query.getCollectionInfo();
		List<RdfPubIRI> items = new ArrayList<>();
		switch (query.getCollectionId()) { 
			case "pendingFollowers": 
			{
				PendingFollowerCache cache = authenticatedActor
						.map(AuthenticatedActor::getPendingFollowerCache)
						.orElse(null);
				return getCached(collectionIri, query.getPageSize(), query.getStartIndex(), cache);
			}
			case "pendingFollowing": 
			{
				PendingFollowerCache cache = authenticatedActor
						.map(AuthenticatedActor::getPendingFollowingCache)
						.orElse(null);
				return getCached(collectionIri, query.getPageSize(), query.getStartIndex(), cache);
			}
			default:
				log.debug("CollectionId: " + query.getCollectionId());
				items = authenticatedActor
					.map(AuthenticatedActor::getActorsStore)
					.map(as->as.getCollection(collectionIri, query.getStartIndex(), query.getPageSize()))					
					.orElse(new ArrayList<>());
								
				return getCollection(collectionIri, items, 1, query);
		}
	}

	private OrderedCollectionPage getCached(RdfPubObjectIdCollection collectionIri, Integer pageSize,
			Integer startIndex, PendingFollowerCache cache) {
		Set<Activity> activities = Optional.ofNullable(cache)
			.map(PendingFollowerCache::getPendingFollowerActivities)
			.map(Set::copyOf)
			.orElse(Collections.emptySet());
		OrderedCollectionPage orderedCollectionPage = toOrderedCollectionPage(collectionIri, pageSize, startIndex,
				activities);
		return orderedCollectionPage;
	}
	
	private boolean isPublic(QueryReadCollection query) {
		return Optional.ofNullable(query)
			.map(QueryReadCollection::getCollectionInfo)
			.map(RdfPubObjectIdCollection::getIRIString)
			.map(x->x.equals(vocabContainer.vocAs().Public().getIRIString()))
			.orElse(false);
	}

	protected void logUseCase(String collectionId) {
		log.debug("\n######################################\n" 
				+ this.getClass().getSimpleName()
				+ "\ncollection: " + collectionId + ")"
				+ "\n######################################\n");		
	}

	private OrderedCollectionPage getCollection(RdfPubObjectIdCollection collectionIri, Collection<RdfPubIRI> irisToResolve, Integer deep, QueryReadCollection4Actor query) {
		Set<RdfPubBlankNodeOrIRI> irisToResolvePaged = getPagedSubCollection(irisToResolve, query.getPageSize(), query.getStartIndex());
		Set<ActivityPubObject> resolvedPagedObjects = resolveObjects(irisToResolvePaged, deep, query.getAuthenticatedActorHolder(), query.getUserAgent());
		Set<ActivityPubObject> resolvedPagedActivities = orderByPublishedDate(resolvedPagedObjects);
		OrderedCollectionPage orderedCollectionPage = toOrderedCollectionPage(collectionIri, query.getPageSize(), query.getStartIndex(), resolvedPagedActivities);
		return orderedCollectionPage;
	}

	private OrderedCollectionPage toOrderedCollectionPage(RdfPubObjectIdCollection collectionIri, Integer pageSize,
			Integer startIndex, Set<? extends ActivityPubObject> activities) {
		String subjectOfOrderedCollection = String.format("%s?pageSize=%s&startIndex=%s", collectionIri.getIRIString(), pageSize, startIndex);
		ActivityPubObject activityPubObject = activityPubObjectFactory.create(activityPubObjectIdBuilder.createFromUrl(subjectOfOrderedCollection));
		OrderedCollectionPage orderedCollectionPage = activityPubObject.asConvertable().asOrderedCollectionPage();
		orderedCollectionPage.setType(Set.of(vocabContainer.vocAs().OrderedCollectionPage()));
		orderedCollectionPage.setSummary("collection: " +collectionIri.getIRIString () + "; pageSize: " + pageSize + "; startIndex: " + startIndex);
		orderedCollectionPage.setPartOf(collectionIri);
		orderedCollectionPage.setTotalItems(activities.size());
		activities.forEach(orderedCollectionPage::addObject);
		return orderedCollectionPage;
	}

	private Set<ActivityPubObject> orderByPublishedDate(Set<? extends ActivityPubObject> findAll) {
		Set<ActivityPubObject> activities = findAll.stream()
				.sorted((o1, o2) -> compareByPublished(o1,o2))
				.collect(Collectors.toSet());
		return activities;
	}

	private Set<ActivityPubObject> resolveObjects(Set<RdfPubBlankNodeOrIRI> irisToResolvePaged, Integer deep, AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		Set<ActivityPubObject> findAll = irisToResolvePaged.stream()
				.map(urlOfObjectToRead->findObject(deep, authenticatedActorHolder, urlOfObjectToRead, userAgent))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toSet());
		return findAll;
	}
	
	private Set<RdfPubBlankNodeOrIRI> getPagedSubCollection(Collection<RdfPubIRI> irisToResolve, Integer pageSize, Integer startIndex) {
		Set<RdfPubBlankNodeOrIRI> irisToResolvePaged = new HashSet<>();
		int max = startIndex+pageSize > irisToResolve.size() ? irisToResolve.size() : startIndex+pageSize;
		List<RdfPubBlankNodeOrIRI> irisToResolveList = new ArrayList<>(irisToResolve);
		for (int i = startIndex; i < max; i++) {
			irisToResolvePaged.add(irisToResolveList.get(i));
		}
		return irisToResolvePaged;
	}

	private Optional<ActivityPubObject> findObject(Integer deep, AuthenticatedActorHolder authenticatedActorHolder, RdfPubBlankNodeOrIRI urlOfObjectToRead, UserAgent userAgent) {
		QueryReadObject queryObject = new QueryReadObject(urlOfObjectToRead.getIRIString(), deep, authenticatedActorHolder, userAgent); 
		return ucReadObject.perform(queryObject);
	}

	private int compareByPublished(ActivityPubObject o1, ActivityPubObject o2) {
		Instant _1970 = Instant.ofEpochMilli(0);
		Instant o1p = o1.getPublished().orElse(_1970);
		Instant o2p = o2.getPublished().orElse(_1970);
		return o1p.compareTo(o2p);
	}

	protected Optional<RdfPubObjectIdCollection> getCollectionId(Actor authenticatedActor, String collectionId) {
		return Optional.ofNullable(authenticatedActor)
			.map(Actor::getSubject)
			.flatMap(RdfPubBlankNodeOrIRI::asActor)
			.map(actor->activityPubObjectIdBuilder.createCollection(actor, collectionId));
	}
}
