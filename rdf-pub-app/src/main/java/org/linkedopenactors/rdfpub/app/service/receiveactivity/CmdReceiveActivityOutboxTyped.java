package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

/**
 * Command Object, that holds the data, that was received by an http controller.
 */
public class CmdReceiveActivityOutboxTyped extends CmdQueryBaseAbstract {
	
	/**
	 * The URL of the actor to whom the activity was sent. Also known as inbox owner.
	 */
	private RdfPubObjectIdActor recipientActorId;

	/**
	 * The activity/object that is sended to the outbox.
	 */
	private ActivityPubObject activityPubObject;
	
	/**
	 * Creates an instance.
	 * @param recipientActorId The id of an actor to whom the activity was sent. Also known as inbox owner.
	 * @param activityPubObject The activity or object that is send to the outbox.
	 */
	public CmdReceiveActivityOutboxTyped(RdfPubObjectIdActor recipientActorId, ActivityPubObject activityPubObject,
			AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.activityPubObject = activityPubObject;
		this.recipientActorId = recipientActorId;
	}

	public RdfPubObjectIdActor getRecipientActorId() {
		return recipientActorId;
	}

	public ActivityPubObject getActivityPubObject() {
		return activityPubObject;
	}
}
