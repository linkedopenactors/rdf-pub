package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.ReceiverStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper Class for {@link InboxReceiveActivityServiceDefault} to determine the
 * useCase type and provide an {@link InboxUseCaseContext}
 */
@Component
@Slf4j
class InboxUseCasesDeterminator extends AbstractUseCasesDeterminator {
	
	private ActorsStoreRepository actorsStoreRepository;

	public InboxUseCasesDeterminator(VocabContainer vocabContainer, ActorsStoreRepository actorsStoreRepository,
			ExternalObjectRepository externalObjectRepository, PublicActorStore publicActorStore) {
		super(vocabContainer, publicActorStore, externalObjectRepository);
		this.actorsStoreRepository = actorsStoreRepository;
	}
	
	public InboxUseCaseContext determinateInbox(Activity activity, ReceiverStore receiverStore, RdfPubObjectId receiverId, String loggableReceiverName, SignatureVerifier signatureVerifier) {		
		Set<UseCase> usecases = new HashSet<>();
		Set<ActivityPubObject> objects = getObjects(activity, activity.resolveObject(), receiverStore);
		
		if(isDeleteRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.s2s_deleteRequest);
		}
		if(isFollowingRequest(activity, activity.getTypes())) {
			if(signatureVerifier == null) {
				usecases.add(UseCase.s2s_followInternal);
			} else {
				usecases.add(UseCase.s2s_followRequest);
			}
		}
		if(isCreateObjectRequest(activity, activity.getTypes())) {
			if(signatureVerifier == null) {
				usecases.add(UseCase.s2s_createObjectInternal);
			} else {
				usecases.add(UseCase.s2s_createObject);
			}
		}
		if(isUpdateObjectRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.s2s_updateObject);
		}
		if(isUndoFollowingRequest(activity, activity.getTypes(), objects)) {
			if(signatureVerifier == null) {
				usecases.add(UseCase.s2s_undoFollowInternal);
			} else {
				usecases.add(UseCase.s2s_undoFollow);
			}
		}
		if(isLikeRequest(activity, activity.getTypes())) {
			usecases.add(UseCase.s2s_like);
		}		
		
		if(isAcceptFollower(getPendingFollowerCache(receiverId.getBaseSubject(), loggableReceiverName), receiverStore, activity, activity.getTypes())) {
			usecases.add(UseCase.s2s_accept_follower);
		}
		
		if(isAcceptFollowing(getPendingFollowingCache(receiverId.getBaseSubject(), loggableReceiverName), receiverStore, activity, activity.getTypes())) {
			usecases.add(UseCase.s2s_accept_following);
		}

		if(usecases.size()!=1) {
			// Puh, that can happen!! If we need to read something from a remote server and it's not accessable at the moment!!
			// Do we need a cache?? thats a difficult question
			// or should we introduce async processing with a queue ?
			log.error("activity that makes trouble:\n" + activity);
			throw new IllegalStateException("currently we expect exactly one usecase. But is: " + usecases);
		}
		logUseCase(loggableReceiverName, usecases, activity);
		UseCase uc = usecases.stream().findFirst().orElseThrow();
		PendingFollowerCache pendingFollowerCache = getPendingFollowerCache(receiverId.getBaseSubject(), loggableReceiverName);
		PendingFollowerCache pendingFollowingCache = getPendingFollowingCache(receiverId.getBaseSubject(), loggableReceiverName);
		if (Optional.ofNullable(signatureVerifier).isPresent()) {
			return new InboxUseCaseContext(pendingFollowerCache, pendingFollowingCache, uc, receiverStore, activity,
					objects, signatureVerifier);
		} else {
			return new InboxUseCaseContext(pendingFollowerCache, pendingFollowingCache, uc, receiverStore, activity,
					objects);
		}
	}

	private PendingFollowerCache getPendingFollowerCache(RdfPubIRI receiverId, String loggableStoreName) {
		// TODO das ist hier noch irgendwie unglücklich!
		// SEE: RequestDataDefault#getActorsStore
		return actorsStoreRepository.getPendingFollowerCache(receiverId, loggableStoreName);		
	}
	
	private PendingFollowerCache getPendingFollowingCache(RdfPubIRI receiverId, String loggableStoreName) {
		// TODO das ist hier noch irgendwie unglücklich!
		// SEE: RequestDataDefault#getActorsStore
		return actorsStoreRepository.getPendingFollowingCache(receiverId, loggableStoreName);
	}
	
}
