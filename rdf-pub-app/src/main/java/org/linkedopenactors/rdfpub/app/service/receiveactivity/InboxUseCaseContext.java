package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.ReceiverStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.security.SignatureVerifier;

import lombok.Data;

/**
 * Container for the attributes that an InboxUsecaseProcessor needs to execute the UseCase.
 */
@Data
public class InboxUseCaseContext {
	private final PendingFollowerCache pendingFollowerCache;
	private final PendingFollowerCache pendingFollowingCache;
	private final UseCase useCase;
	private final ReceiverStore receiverStore;
	private final Activity activity;
	private Set<ActivityPubObject> objects;
	private SignatureVerifier signatureVerifier;
	
	/**
	 * Constructs a {@link InboxUseCaseContext}
	 * @param pendingFollowerCache
	 * @param pendingFollowingCache
	 * @param useCase The type of the useCase.
	 * @param receiverStore The {@link ActorsStore} of the recipient {@link Actor}. 
	 * @param activity The activity to process 
	 * @param objects The objects of the activity to process
	 * @param signatureVerifier HTTP Signature verifier.
	 */
	public InboxUseCaseContext(PendingFollowerCache pendingFollowerCache, PendingFollowerCache pendingFollowingCache, UseCase useCase, ReceiverStore receiverStore, Activity activity,
			Set<ActivityPubObject> objects, SignatureVerifier signatureVerifier) {
		this(pendingFollowerCache, pendingFollowingCache, useCase, receiverStore, activity, objects);
		this.signatureVerifier = signatureVerifier;
	}
	
	/**
	 * Constructs a {@link InboxUseCaseContext}
	 * @param pendingFollowerCache
	 * @param pendingFollowingCache	 * @param useCase The type of the useCase.
	 * @param receiverStore The {@link ActorsStore} of the recipient {@link Actor}. 
	 * @param activity The activity to process 
	 * @param objects The objects of the activity to process
	 */
	public InboxUseCaseContext(PendingFollowerCache pendingFollowerCache, PendingFollowerCache pendingFollowingCache, UseCase useCase, ReceiverStore receiverStore, Activity activity, Set<ActivityPubObject> objects) {
		this(pendingFollowerCache, pendingFollowingCache, useCase, receiverStore, activity);
		this.objects = objects;
	}

	/**
	 * Constructs a {@link InboxUseCaseContext}
	 * @param pendingFollowerCache
	 * @param pendingFollowingCache 
	 * @param useCase The type of the useCase.
	 * @param receiverStore The {@link ActorsStore} of the recipient {@link Actor}. 
	 * @param activity The activity to process 
	 */
	public InboxUseCaseContext(PendingFollowerCache pendingFollowerCache, PendingFollowerCache pendingFollowingCache, UseCase useCase, ReceiverStore receiverStore, Activity activity) {
		this.pendingFollowerCache = pendingFollowerCache;
		this.pendingFollowingCache = pendingFollowingCache;
		this.useCase = useCase;
		this.receiverStore = receiverStore;
		this.activity = activity;
	}
}
