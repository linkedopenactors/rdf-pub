package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.ReceiverStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
abstract class AbstractUseCasesDeterminator {
	
	private PublicActorStore publicActorStore;
	private ExternalObjectRepository externalObjectRepository;
	private VocabContainer vocabContainer;
	
	public AbstractUseCasesDeterminator(VocabContainer vocabContainer, PublicActorStore publicActorStore, ExternalObjectRepository externalObjectRepository) {
		this.vocabContainer = vocabContainer;
		this.publicActorStore = publicActorStore;
		this.externalObjectRepository = externalObjectRepository;
	}
	
	protected void logUseCase(String loggableReceiverName, Set<UseCase> usecases, Activity activity) {
		if(!activity.getTypes().contains(vocAs().Delete())) {
			// TODO remove sometimes see: https://github.com/mastodon/mastodon/issues/33290
			log.debug("\n######################################\n" 
					+ usecases.stream().findFirst()
					+ "\nreceiver: " + loggableReceiverName 
					+ "\n######################################\n"
					+ activity.toString());		
		} else {
//			log.debug("log deaktivated for delete activities!");
		}
	}

	protected boolean isDeleteRequest(Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Delete())) {
			return false;
		}
		return true;	
	}

	protected boolean isFollowingRequest(Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Follow())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		Optional<RdfPubObjectId> objectIri = activity.getObject().stream().findFirst().orElseThrow().asRdfPubObjectId();
		if(objectIri.isPresent()) {
			if(!objectIri.orElseThrow().isActor()) {
				return false;
			}
		}
		// TODO should we read external objects to find out, that it is an actor ??
		// it can happen that external sources are not accessible !
		return true;	
	}

	protected boolean isLikeRequest(Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Like())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		return true;	
	}

	protected boolean isAcceptFollower(AuthenticatedActor authenticatedActor, Activity activity, Set<RdfPubIRI> types)  {
		return isAcceptFollower(authenticatedActor.getPendingFollowerCache(), authenticatedActor.getActorsStore() , activity, types);

	}
	
	protected boolean isAcceptFollower(PendingFollowerCache pendingFollowerCache, ReceiverStore receiverStore, Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Accept())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		RdfPubBlankNodeOrIRI objectIri = activity.getObject().stream().findFirst().orElseThrow();
		
		boolean containsFollowActivity = pendingFollowerCache.contains(objectIri.getIRIString());
		
		if(!containsFollowActivity) {
			if(getFollowersAsStringSet(receiverStore).contains(objectIri.getIRIString())) {
				log.warn(objectIri.getIRIString() + " is already a follower! Maybe there was a redundant accept request?!");
			}				
		}
		return containsFollowActivity;
	}

	protected boolean isAcceptFollowing(PendingFollowerCache pendingFollowerCache, ReceiverStore receiverStore, Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Accept())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		RdfPubBlankNodeOrIRI objectIri = activity.getObject().stream().findFirst().orElseThrow();
		
		boolean containsFollowActivity = pendingFollowerCache.contains(objectIri.getIRIString());
		
		if(!containsFollowActivity) {
			if(getFollowingsAsStringSet(receiverStore).contains(objectIri.getIRIString())) {
				log.warn(objectIri.getIRIString() + " is already a following! Maybe there was a redundant accept request?!");
			}				
		}
		return containsFollowActivity;
	}

	private Set<String> getFollowingsAsStringSet(ReceiverStore recipientActorStore) {
		return recipientActorStore.getFollowings().stream()
			.map(RdfPubBlankNodeOrIRI::getIRIString)
			.collect(Collectors.toSet());
	}

	private Set<String> getFollowersAsStringSet(ReceiverStore recipientActorStore) {
		return recipientActorStore.getFollowers().stream()
			.map(RdfPubBlankNodeOrIRI::getIRIString)
			.collect(Collectors.toSet());
	}

	protected boolean isUndoFollowingRequest(Activity activity, Set<RdfPubIRI> types, Set<ActivityPubObject> objects)  {
		if(!types.contains(vocAs().Undo())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		if(!new ArrayList<>(objects).get(0).isActivity()) {
			return false;
		}
		Set<RdfPubBlankNodeOrIRI> objectTypes = extractActivityObjectTypes(objects);
		if(!objectTypes.contains(vocAs().Follow())) {
			return false;
		}
		return true;	
	}

	protected boolean isRemoveFollowerRequest(Actor currentActor, Activity activity, Set<RdfPubIRI> types, Set<ActivityPubObject> objects)  {
		if(!types.contains(vocAs().Remove())) {
			return false;
		}
		if( activity.getObject().size() != 1 ) {
			return false;
		}
		
		boolean targetIsFollowerCollection = activity.getTarget()
				.map(target->target.equals(currentActor.getFollowers()))
				.orElse(false);
		if(!targetIsFollowerCollection) {
			return false;
		}
		return true;	
	}

	protected boolean isCreateObjectRequest(Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Create())) {
			return false;
		}
		if( activity.getObject().isEmpty() ) {
			return false;
		}
		return true;	
	}

	protected boolean isUpdateObjectRequest(Activity activity, Set<RdfPubIRI> types)  {
		if(!types.contains(vocAs().Update())) {
			return false;
		}
		if( activity.getObject().isEmpty() ) {
			return false;
		}
		return true;	
	}

	/**
	 * Searches for the transferred objects in the database. If they cannot be
	 * found, the activity is searched for.
	 * 
	 * @param activity
	 * @param resolvedObjects
	 * @param receiverStore
	 * @return All objects referenced in the parameter resolvedObjects.
	 */
	protected Set<ActivityPubObject> getObjects(Activity activity, Set<ActivityPubObject> resolvedObjects,
			ReceiverStore receiverStore) {
		Set<ActivityPubObject> res = new HashSet<>();
		for (RdfPubBlankNodeOrIRI rdfPubIRI : activity.getObject()) {
			Optional<ActivityPubObject> existingOpt = receiverStore.findBySubject(rdfPubIRI);
			if(existingOpt.isEmpty()) { // Not in the actors store
				existingOpt = publicActorStore.findBySubject(rdfPubIRI);	
			}
			if(existingOpt.isEmpty()) {// Not in the public store
				existingOpt = 
				resolvedObjects.stream()
					.filter(obj->obj.getSubject().getIRIString().equals(rdfPubIRI.getIRIString()))					
					.findFirst();
			} if(existingOpt.isEmpty()) { // Not in the graph
				existingOpt = resolveExternal(rdfPubIRI.getIRIString());
			}
			existingOpt.ifPresent(res::add);
		}
		return res;
	}
	
	// Is it meaningful?
	// it can happen that external sources are not accessible !
	private Optional<ActivityPubObject> resolveExternal(String url) {
		 Instance instance = publicActorStore.getInstanceActor();		
		log.trace("resolveExternal("+url+", " + instance.getSubject() + ")");
		return externalObjectRepository.readWithSignature(url, instance);
	}

	private Set<RdfPubBlankNodeOrIRI> extractActivityObjectTypes(Set<ActivityPubObject> objects) {
		return objects.stream()
			.map(ActivityPubObject::getTypes)
			.flatMap(Collection::stream)
			.collect(Collectors.toSet());
	}

	protected VocAs vocAs() {
		return vocabContainer.vocAs();
	}
}
