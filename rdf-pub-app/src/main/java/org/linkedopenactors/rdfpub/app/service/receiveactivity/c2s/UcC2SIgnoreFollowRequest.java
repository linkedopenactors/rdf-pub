package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

/**
 * An actor on this server wants to ignore a follower request. 
 */
public class UcC2SIgnoreFollowRequest extends AbstractUcC2SCreateObject {

	private OutboxUseCaseContext ucCtx;
	private InternalDelivery internalDelivery;
	private ExternalDelivery externalDelivery;

	/**
	 * 
	 * @param ucCtx
	 * @param internalDelivery
	 * @param externalDelivery
	 * @param publicActorStore
	 */
	public UcC2SIgnoreFollowRequest(OutboxUseCaseContext ucCtx, InternalDelivery internalDelivery,
			ExternalDelivery externalDelivery, PublicActorStore publicActorStore) {
		super();
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
		this.externalDelivery = externalDelivery;
	}

	/**
	 * Process the usecase.
	 * @return the subject / location of the created activity
	 */
	@Override
	public RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder) {
		adjustActivityAndObjects(ucCtx.getOutboxOwner().getSubject().asActor().orElseThrow(), ucCtx.getActivity());
		RdfPubObjectIdActivity addedActivitySubject = ucCtx.getRecipientActorStore().addToOutbox(ucCtx.getActivity(), "ReceiveActivityOutboxServiceDefault#perform create");
		String outboxOwnerLoggableName = ucCtx.getOutboxOwner().getPreferredUsername().orElse(ucCtx.getOutboxOwner().getSubject().getIRIString());
		internalDelivery.deliver(ucCtx.getRecipientActorStore(), addedActivitySubject, outboxOwnerLoggableName);
		externalDelivery.deliver(ucCtx.getRecipientActorStore(), ucCtx.getOutboxOwner(), addedActivitySubject);
		RdfPubObjectIdActivity activityIri = addedActivitySubject;

		ucCtx.getPendingFollowerCache().removePendingFollowActivity(getIriToRemove());
		return activityIri;
	}

	private RdfPubBlankNodeOrIRI getIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		return ucCtx.getActivity().getObject().stream().findFirst().orElseThrow();
	}
}
