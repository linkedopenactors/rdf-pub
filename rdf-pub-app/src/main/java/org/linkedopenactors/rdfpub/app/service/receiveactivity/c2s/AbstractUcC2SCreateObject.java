 package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUsecaseProcessor;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

abstract class AbstractUcC2SCreateObject implements OutboxUsecaseProcessor {

	/**
	 * Do some adjustments
	 * <ul>
	 *   <li>{@link Activity#unify()}</li>
	 *   <li>{@link Activity#replaceSubjects(RdfPubObjectIdActor)}</li>
	 *   <li>{@link Activity#resolveObject()} forEach {@link ActivityPubObject#setAttributedTo(java.util.Set)}</li>
	 * </ul>
	 * @param actorsBaseSubject
	 * @param activity
	 */
	protected void adjustActivityAndObjects(RdfPubObjectIdActor actorsBaseSubject, org.linkedopenactors.rdfpub.domain.Activity activity) {
		activity.unify();
		activity.replaceSubjects(actorsBaseSubject);
		activity.resolveObject().stream().forEach(o->{
			Set<RdfPubBlankNodeOrIRI> actor = activity.getActor().stream()
					.map(RdfPubBlankNodeOrIRI.class::cast)
					.collect(Collectors.toSet());
			o.setAttributedTo(actor);
		});
	}
}
