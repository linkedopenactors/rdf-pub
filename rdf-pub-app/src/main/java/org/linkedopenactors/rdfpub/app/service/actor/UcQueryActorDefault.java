package org.linkedopenactors.rdfpub.app.service.actor;

import java.net.URI;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.adapter.ObjectAdapter;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.auth.Authentication;
import org.linkedopenactors.rdfpub.app.instance.InstanceService;
import org.linkedopenactors.rdfpub.app.service.readobject.QueryReadObject;
import org.linkedopenactors.rdfpub.app.service.readobject.UcReadObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

/**
 * Application Service for querying Actors.
 */
@Service
@Slf4j
class UcQueryActorDefault implements UcQueryActor {

	@Autowired
	private ObjectAdapter objectAdapter;
	
	@Autowired
	private RDF rdf;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private PublicActorStore publicActorStore;

	@Autowired
	private UcCreateActorDefault actorFactory;
	
	@Autowired
	private UcReadObject ucReadObject;
	
	@Autowired
	private ContentTypeHeaderMappings contentTypeHeaderMappings;
	
	@Autowired
	private InstanceService instanceService;
	
	@Autowired
	private VocabContainer vocabContainer;
	
	@Override
	@Synchronized
	public Actor perform(QueryByAuthentication query) {
		RdfPubMDC.put(query.getClass().getSimpleName());
		try {
			return performInternal(query);
		} finally {
			RdfPubMDC.remove();
		}
	}
		
	private Actor performInternal(QueryByAuthentication query) {
		AuthenticatedActorHolder authenticatedActorHolder = query.getAuthenticatedActorHolder();
		Authentication authentication = authenticatedActorHolder.getAuthentication();
		if(!authentication.isAuthenticated()) {
			throw new IllegalStateException("Don't call me, if there is nothing to query!");
		} else {
			CheckExistingUserResult checkExistingUserResult = checkExistingUser(authentication.getIssuerPreferredUsername(), authentication);
			log.debug("findByByAuthentication - " + checkExistingUserResult.getMatch().name());
			Actor foundActor = null;
			switch (checkExistingUserResult.getMatch()) {
				case userWithThatNameExistsAndItMatchesWithTheAuthenticated: {					
					foundActor = checkExistingUserResult.getFoundActor().orElseThrow();
					break;
				}
				case authenticatedUserExistsButHasDifferentUsername: {
					foundActor = checkExistingUserResult.getFoundActor().orElseThrow();
					break;
				}
				case noUserWithThatName: {
					foundActor = handleUserNameIsAvailable(authenticatedActorHolder, authentication, query.getUserAgent());
					break;
				}
			}
			if(foundActor == null) {
				throw new IllegalArgumentException("Unexpected value: " + checkExistingUserResult);
			} else {
				log.debug("findByByAuthentication - " + checkExistingUserResult.getMatch().name() + " -> "
						+ foundActor.getPreferredUsername());
					return foundActor;
			}
		}		
	}
		
	private Actor handleUserNameIsAvailable(AuthenticatedActorHolder authenticatedActorHolder, Authentication authentication, UserAgent userAgent) {
		
		// TODO here we should not use the issuerUserId as userId!
		// i only changed this because i delete the database on the development server
		// from time to time without deleting users. and mastodon, for example, cannot
		// be stopped from filling up the inbox of old users. hence the attempt to use
		// the issuerId so that keycloack users always get the same url. This should be
		// adapted for production !!!
		log.debug("handleUserNameIsAvailable. Now reading actor");
		RdfPubObjectIdActor createdActorId = activityPubObjectIdBuilder.createActor(authentication.getIssuerUserId());
		actorFactory.perform(createCommand(authentication, createdActorId));
		Actor created = perform(new QueryByRdfPubObjectId(authenticatedActorHolder, createdActorId, userAgent)).orElseThrow();
		return created;
	}

	private CmdCreatePerson createCommand(Authentication authentication, RdfPubObjectIdActor createdActorId) {
		String msg = "ActorServiceDefault#queryActor - no existing actor for '"
				+ authentication.getIssuerPreferredUsername() + "' found. A new one will created.";
		String issuerPreferredUsername = authentication.getIssuerPreferredUsername();
		URI issuer = authentication.getIssuer();
		String issuerUserId = authentication.getIssuerUserId();
		CmdCreatePerson cmd = new CmdCreatePerson(createdActorId, issuerUserId, issuerPreferredUsername, issuer, msg);
		return cmd;
	}
	
	public Optional<Actor> perform(QueryByOauth2IssuerUserId query) {
		RdfPubMDC.put(query.getClass().getSimpleName());
		try {
			return performInternal(query);
		} finally {
			RdfPubMDC.remove();
		}
	}	

	private Optional<Actor> performInternal(QueryByOauth2IssuerUserId query) {
		String issuerUserId = query.getId();
		log.debug("findByOauth2IssuerUserId ("+issuerUserId+")");
						
		Set<ActivityPubObject> oauth2IssuerUserIdTriples = publicActorStore.findByPredicateAndObject(vocabContainer.vocRdfPub().oauth2IssuerUserId(),rdf.createLiteral(issuerUserId));
		// ^^ this ActivityPubObjects does contain only the 'oauth2IssuerUserId' Triple !
		if(oauth2IssuerUserIdTriples.size()>1) {			
			oauth2IssuerUserIdTriples.forEach(ao->log.error(ao.toString()));			
			throw new IllegalStateException("more than one hit for issuerUserId: " + issuerUserId);
		} 
		
		Optional<Actor> foundActorByOauth2IssuerUserId = oauth2IssuerUserIdTriples.stream().findFirst()
			.flatMap(this::getBaseSubject)
			.flatMap(subject->publicActorStore.findBySubject(subject, 0))
			.map(ActivityPubObject::asConvertable)
			.map(ActivityPubObjectConvertable::asActor);
		
		return foundActorByOauth2IssuerUserId;
	}
	
	private Optional<RdfPubBlankNodeOrIRI> getBaseSubject(ActivityPubObject ao) {
		RdfPubBlankNodeOrIRI subject = ao.getSubject();
		Optional<RdfPubObjectId> asRdfPubObjectId = subject.asRdfPubObjectId();
		return asRdfPubObjectId.map(RdfPubObjectId::getBaseSubject);
	}

	/**
	 * Checks, if there is user with that name already exists and if it's matching to the authenticated user.
	 * @param authentication
	 * @return {@link CheckExistingUserResult}
	 */
	private CheckExistingUserResult checkExistingUser(String userName, Authentication authentication) {
		Optional<Actor> foundByOauth2IssuerUserIdOpt = perform(new QueryByOauth2IssuerUserId(authentication.getIssuerUserId()));		
		Optional<Actor> foundByPreferredUserNameOpt = perform(new QueryByPreferredUsername(userName));
		
		log.trace("foundByPreferredUserNameOpt("+userName+"): " + foundByPreferredUserNameOpt);
		
		Boolean match = foundByOauth2IssuerUserIdOpt.map(actor->{
			String oauth2Issuer = actor.getOauth2Issuer().orElse("not found");
			String oauth2IssuerUserId = actor.getOauth2IssuerUserId().orElse("not found");
			
			String authenticationIssuer = Optional.ofNullable(authentication)
					.map(Authentication::getIssuer)
					.map(URI::getRawAuthority)
					.orElse("not found");
						
			boolean issuerMatch = authenticationIssuer.equals(oauth2Issuer);			
			
			String authenticationdIssuerUserId = Optional.ofNullable(authentication)
					.map(Authentication::getIssuerUserId)
					.orElse("not found");

			boolean issuerIdMatch = authenticationdIssuerUserId.equals(oauth2IssuerUserId);
			boolean result = issuerMatch && issuerIdMatch;
			return result;
		}).orElse(false);
		if(foundByPreferredUserNameOpt.isPresent()) {
			if(match) {
				return new CheckExistingUserResult(Match.userWithThatNameExistsAndItMatchesWithTheAuthenticated, foundByPreferredUserNameOpt.orElse(null));
			} else {
				return new CheckExistingUserResult(Match.authenticatedUserExistsButHasDifferentUsername, foundByPreferredUserNameOpt.orElse(null));
			}
		} else {
			return new CheckExistingUserResult(Match.noUserWithThatName, foundByPreferredUserNameOpt.orElse(null));
		}
	}
	
	@Override
	public Optional<Actor> perform(QueryById query) {
		RdfPubMDC.put(query.getClass().getSimpleName());
		try {
			return findById(query.getAuthenticatedActorHolder(), activityPubObjectIdBuilder.createActor(query.getId()), query.getUserAgent());
		} finally {
			RdfPubMDC.remove();
		}
	}

	public Optional<String> perform(QueryByUrl queryByUrl) {
		RdfPubMDC.put(queryByUrl.getClass().getSimpleName());
		try {			
			return performInternal(queryByUrl);
		} catch(Exception ex) {
			log.warn("nothing found for " + queryByUrl);
			return Optional.empty();
		}
		finally {
			RdfPubMDC.remove();
		}
	}

	private Optional<String> performInternal(QueryByUrl queryByUrl) {		
		String requestedUrl = queryByUrl.getRequestedUrl();
		log.debug("findByUrl("+requestedUrl+")");
		if(requestedUrl.startsWith("/")) {
			requestedUrl = instanceService.getInternalUrnExtendedWith(requestedUrl).getIRIString();
		}
		String adjustedUrl = requestedUrl;
		
		RdfPubObjectIdActor id = activityPubObjectIdBuilder.createFromUrl(requestedUrl)
				.asActor()
				.orElseThrow(()->new IllegalArgumentException("not found " + adjustedUrl));
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(queryByUrl.getContentType());
		Optional<Actor> actor = perform(new QueryByRdfPubObjectId(queryByUrl.getAuthenticatedActorHolder(), id, queryByUrl.getUserAgent()));
		 
		Optional<String> objectAsStringOpt = actor.map(ao->objectAdapter.adjust(ao, rdfFormat, queryByUrl.getUserAgent()));
		
		return objectAsStringOpt;
	}
	
	@Override
	public Optional<Actor> perform(QueryByRdfPubObjectId query) {
		RdfPubMDC.put(query.getClass().getSimpleName());
		try {			
			return findById(query.getAuthenticatedActorHolder(), query.getId(), query.getUserAgent());
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	private Optional<Actor> findById(AuthenticatedActorHolder authenticatedActorHolder, RdfPubObjectId rdfPubObjectId, UserAgent userAgent) {		
		Optional<Actor> result = publicActorStore
				.findBySubject(rdfPubObjectId)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor);
		
		result = result.map(act->{
			act.getPublicKey()
				.map(pkIri->{
					ucReadObject.perform(new QueryReadObject(pkIri.getIRIString(), 0, authenticatedActorHolder, userAgent))
							.map(ActivityPubObject::asConvertable)
							.map(ActivityPubObjectConvertable::asPublicKey)
							.ifPresent(act::setPublicKey);
					return act;	
				});	
			return act;
		});
		
		log.trace("searching for: " + rdfPubObjectId.toString() + " -> found: " + result.isPresent());
		return result;
	}

	@Override
	public Optional<Actor> perform(QueryByPreferredUsername query) {
		RdfPubMDC.put(query.getClass().getSimpleName());
		try {			
			return performInternal(query);
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	private Optional<Actor> performInternal(QueryByPreferredUsername query) {		
		String preferredUsername = query.getPreferredUsername();
		log.debug("findByPreferredUsername("+preferredUsername+")");
		Literal userNameLiteral = rdf.createLiteral(preferredUsername);
		
		Optional<Actor> actorOpt = publicActorStore
				.findByTriple(null, vocabContainer.vocAs().preferredUsername(),userNameLiteral)
				.map(ActivityPubObject::getSubject)
				// Attention! publicActorStore.find delivers only the subject!!
				// we have to read the full object again!
				.flatMap(actorSubject->publicActorStore.findBySubject(actorSubject, 0))
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor);
		
		if(actorOpt.isPresent()) {
			Actor actor = actorOpt.get();
			actor.getEndpoints()
					.flatMap(publicActorStore::findBySubject)
					.map(ActivityPubObject::asConvertable)
					.map(ActivityPubObjectConvertable::asEndpoints)
					.ifPresent(actor::setEndpoints);
			actor.getPublicKey()
				.flatMap(publicActorStore::findBySubject)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asPublicKey)
				.ifPresent(actor::setPublicKey);
		}
		return actorOpt;		
	}

	@Data
	@AllArgsConstructor
	class CheckExistingUserResult{
		private Match match;
		private Actor foundActor;
		
		public Optional<Actor> getFoundActor() {
			return Optional.ofNullable(foundActor);
		}
	}	
	
	/**
	 * See values.
	 */
	enum Match {
		userWithThatNameExistsAndItMatchesWithTheAuthenticated, authenticatedUserExistsButHasDifferentUsername, noUserWithThatName;
		
		boolean userExists() {
			return userWithThatNameExistsAndItMatchesWithTheAuthenticated.equals(this) ||
					authenticatedUserExistsButHasDifferentUsername.equals(this);
		}
	}
}
