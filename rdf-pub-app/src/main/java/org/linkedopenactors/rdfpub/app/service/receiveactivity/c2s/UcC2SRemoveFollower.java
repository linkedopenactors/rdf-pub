package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

/**
 * An actor on this server wants to remove a follower from his follower collection. 
 */
class UcC2SRemoveFollower extends AbstractUcC2SCreateObject {

	private OutboxUseCaseContext ucCtx;
	private InternalDelivery internalDelivery;
	private ExternalDelivery externalDelivery;

	public UcC2SRemoveFollower(OutboxUseCaseContext ucCtx, InternalDelivery internalDelivery,
			ExternalDelivery externalDelivery) {
		super();
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
		this.externalDelivery = externalDelivery;
	}

	/**
	 * Process the usecase.
	 * @return the subject / location of the created activity
	 */
	@Override
	public RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder) {
		adjustActivityAndObjects(ucCtx.getOutboxOwner().getSubject().asActor().orElseThrow(), ucCtx.getActivity());
		RdfPubObjectIdActivity addedActivitySubject = ucCtx.getRecipientActorStore().addToOutbox(ucCtx.getActivity(), "ReceiveActivityOutboxServiceDefault#perform create");
		String outboxOwnerLoggableName = ucCtx.getOutboxOwner().getPreferredUsername().orElse(ucCtx.getOutboxOwner().getSubject().getIRIString());
		internalDelivery.deliver(ucCtx.getRecipientActorStore(), addedActivitySubject, outboxOwnerLoggableName);
		externalDelivery.deliver(ucCtx.getRecipientActorStore(), ucCtx.getOutboxOwner(), addedActivitySubject);
		RdfPubObjectIdActivity activityIri = addedActivitySubject;

		ucCtx.getRecipientActorStore().removeFollower(getActivityIriToRemove());
		return activityIri;
	}

	private RdfPubIRI getActivityIriToRemove() {
		if(ucCtx.getObjects().size() != 1) {
			throw new IllegalStateException();
		}
		Activity activity = ucCtx.getObjects().stream().findFirst().orElseThrow().asConvertable().asActivity();
		RdfPubIRI activityIriToRemove = (RdfPubIRI)activity.getSubject();
		return activityIriToRemove;
	}
}
