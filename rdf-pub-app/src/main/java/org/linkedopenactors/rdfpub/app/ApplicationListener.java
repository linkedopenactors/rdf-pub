package org.linkedopenactors.rdfpub.app;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ApplicationListener implements ApplicationContextAware {
	
	@Override
	public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
		started();
	}
	
	private void started() {
		log.info("RDF-PUB Server Started !");
	}
}
