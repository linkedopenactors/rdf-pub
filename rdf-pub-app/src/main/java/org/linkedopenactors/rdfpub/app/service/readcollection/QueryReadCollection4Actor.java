package org.linkedopenactors.rdfpub.app.service.readcollection;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

public class QueryReadCollection4Actor extends CmdQueryBaseAbstract {
	private Integer pageSize;
	private Integer startIndex;
	private String actorId;
	private RdfPubObjectIdCollection collectionInfo;
	private String collectionId;

	public QueryReadCollection4Actor(AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public String getActorId() {
		return actorId;
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}

	public RdfPubObjectIdCollection getCollectionInfo() {
		return collectionInfo;
	}

	public void setCollectionInfo(RdfPubObjectIdCollection collectionInfo) {
		this.collectionInfo = collectionInfo;
	}

	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}
}
