package org.linkedopenactors.rdfpub.app.service.actor;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

import lombok.Getter;

@Getter
public class QueryByAuthentication extends CmdQueryBaseAbstract {

	/**
	 * 
	 * @param authenticatedActorHolder Information about a authenticated user.
	 * @param userAgent Info about the caller of the service.
	 */
	public QueryByAuthentication(AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
	}
}
