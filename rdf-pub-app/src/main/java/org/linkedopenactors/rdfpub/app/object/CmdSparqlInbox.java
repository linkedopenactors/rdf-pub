package org.linkedopenactors.rdfpub.app.object;

public class CmdSparqlInbox extends CmdSparqlCollection {
	public CmdSparqlInbox(String query, String acceptHeader, org.linkedopenactors.rdfpub.domain.Actor authenticatedActor) {
		super(query, acceptHeader, authenticatedActor);
	}
}
