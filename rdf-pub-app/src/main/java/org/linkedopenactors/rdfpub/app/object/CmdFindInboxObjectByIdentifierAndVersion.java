package org.linkedopenactors.rdfpub.app.object;

import java.math.BigInteger;

import lombok.Data;

@Data
public class CmdFindInboxObjectByIdentifierAndVersion {
	private org.linkedopenactors.rdfpub.domain.Actor actor;
	private BigInteger version;
	private String identifier;
}
