package org.linkedopenactors.rdfpub.app.service.actor;

import lombok.Data;

@Data
public class QueryByOauth2IssuerUserId {
	private final String id;
}
