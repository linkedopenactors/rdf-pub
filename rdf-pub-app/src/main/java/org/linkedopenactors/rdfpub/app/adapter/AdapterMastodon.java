package org.linkedopenactors.rdfpub.app.adapter;

import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class AdapterMastodon {

	private GraphToStringConverter graphToStringConverter;
	private VocabContainer vocabContainer;

	public AdapterMastodon(VocabContainer vocabContainerParam, GraphToStringConverter graphToStringConverter) {
		this.vocabContainer = vocabContainerParam;
		this.graphToStringConverter = graphToStringConverter;		
	}
	
	public String adjustMastodon(ActivityPubObject activityPubObject, RdfFormat rdfFormat) {
		adustActivityPubObject(activityPubObject);
		String objectAsStringOpt = toString(activityPubObject, rdfFormat);
		log.debug("adjustMastodon: " + objectAsStringOpt);
		return objectAsStringOpt;
	}

	private String toString(ActivityPubObject activityPubObject, RdfFormat rdfFormat) {
		String objectAsStringOpt = graphToStringConverter.convert(rdfFormat, activityPubObject.asGraph(), true);
		objectAsStringOpt = objectAsStringOpt.replace("as:", "");
		return objectAsStringOpt;
	}

	private void adustActivityPubObject(ActivityPubObject activityPubObject) {
		log.debug("adjust: " + activityPubObject.getSubject());
		
		log.debug("adjust: " + activityPubObject);

		adjust(activityPubObject);
		
		activityPubObject.getReferences(false)
			.forEach(x->adjust(activityPubObject, x));
		
//		activityPubObject.removeSameAs();
//		activityPubObject.removeWasAttributedTo();
//		activityPubObject.removeWasGeneratedBy();
		
//		Set<RdfPubIRI> types = activityPubObject.getTypes();
//		types.remove(VocProv.Entity());
//		types.remove(VocProv.Activity());
//		activityPubObject.setType(types);
		
//		if(activityPubObject.isActivity()) {
//			Activity act = activityPubObject.asConvertable().asActivity();
//					act.resolveObject().forEach(this::adustActivityPubObject);
//		}
		
//		activityPubObject.getTags().forEach(a->{
//			activityPubObject.asConvertable().resolve(a).ifPresent(this::adustActivityPubObject);
//		});
//		
//		activityPubObject.getSubject().asActor().ifPresent(a->{
//			activityPubObject.asConvertable().asActor().resolvePublicKey().ifPresent(this::adustActivityPubObject);
//		});

	}
	
	private void adjust(ActivityPubObject original, RdfPubBlankNodeOrIRI subject) {
		Optional<ActivityPubObject> a = original.asConvertable().resolve(subject);
		a.ifPresent(this::adjust);
	}

	private void adjust(ActivityPubObject ao) {
		Set<RdfPubIRI> types = ao.getTypes();
		types.remove(vocabContainer.vocProv().Entity());
		types.remove(vocabContainer.vocProv().Activity());
		ao.setType(types);
		ao.removeSameAs();
		ao.removeWasAttributedTo();
		ao.removeWasGeneratedBy();
//			ao.getTags().forEach(a->{
//				ao.asConvertable().resolve(a).ifPresent(this::adustActivityPubObject);
//			});
//			
//			activityPubObject.getSubject().asActor().ifPresent(a->{
//				activityPubObject.asConvertable().asActor().resolvePublicKey().ifPresent(this::adustActivityPubObject);
//			});
	}	
}
