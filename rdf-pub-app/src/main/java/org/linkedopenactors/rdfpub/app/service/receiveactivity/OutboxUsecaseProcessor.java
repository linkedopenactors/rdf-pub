package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

/**
 * Interface for C2S (outbox) UseCases. 
 */
public interface OutboxUsecaseProcessor {

	/**
	 * Executes the useCase.
	 * @return  The id / location of the created activity.
	 */
	RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder);
}
