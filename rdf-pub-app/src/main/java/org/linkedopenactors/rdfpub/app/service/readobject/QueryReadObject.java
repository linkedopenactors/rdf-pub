package org.linkedopenactors.rdfpub.app.service.readobject;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

public class QueryReadObject extends CmdQueryBaseAbstract {

	private final int deep;
	private final String url;
	
	public QueryReadObject(String url, int deep, AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.url = url;
		this.deep = deep;
	}

	public int getDeep() {
		return deep;
	}

	public String getUrl() {
		return url;
	}
}
