package org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.InboxUseCaseContext;

/**
 * An external actor like an object on this server. 
 */
public class UcS2SLikeObject extends UcS2SSignatureVerfified {
	
	private InboxUseCaseContext ucCtx;

	/**
	 * 
	 * @param ucCtx
	 */
	public UcS2SLikeObject(InboxUseCaseContext ucCtx) {
		super(ucCtx.getSignatureVerifier());
		this.ucCtx = ucCtx;
	}

	/**
	 * Process the usecase.
	 * @throws BadRequestException
	 */
	@Override
	public void process() throws BadRequestException {
		verify();
		ucCtx.getReceiverStore().addToInbox(ucCtx.getActivity(), "todo");
	}
}
