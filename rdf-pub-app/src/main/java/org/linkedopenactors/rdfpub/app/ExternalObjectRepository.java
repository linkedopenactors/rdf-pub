package org.linkedopenactors.rdfpub.app;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

public interface ExternalObjectRepository {
	Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> read(RdfPubBlankNodeOrIRI subject);
	Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> readWithSignature(String url, Actor currentAuthenticatedActor);
	Optional<ActivityPubObject> readWithInstanceSignature(String url);
}
