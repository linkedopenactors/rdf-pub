package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.time.Instant;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.BadRequestException;
import org.linkedopenactors.rdfpub.app.GoneException;
import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.s2s.UsecaseProcessorFactoryS2S;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.linkedopenactors.rdfpub.domain.ReceiverStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
class InboxReceiveActivityServiceDefault implements InboxReceiveActivityService {

	private ActorsStoreRepository actorsStoreRepository;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private UsecaseProcessorFactoryS2S usecaseProcessorFactory;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private InboxUseCasesDeterminator useCasesDeterminatorReceiveInbox;

	public InboxReceiveActivityServiceDefault(ActorsStoreRepository actorsStoreRepository,
			ActivityPubObjectFactory activityPubObjectFactory, UsecaseProcessorFactoryS2S usecaseProcessorFactory,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder,
			InboxUseCasesDeterminator useCasesDeterminatorReceiveInbox) {
		this.actorsStoreRepository = actorsStoreRepository;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.usecaseProcessorFactory = usecaseProcessorFactory;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
		this.useCasesDeterminatorReceiveInbox = useCasesDeterminatorReceiveInbox;
	}
	
	@Override
	public void perform(CmdReceiveActivityInbox cmd) throws BadRequestException, GoneException {
		RdfPubObjectIdActor recipientActorId =  activityPubObjectIdBuilder.createActor(cmd.getRecipientActorsId());
		org.linkedopenactors.rdfpub.domain.ActivityPubObject activityPubObject = activityPubObjectFactory.create(cmd.getRdfFormat(),cmd.getReceivedActivity());
		Activity activity  = activityPubObject.asConvertable().asActivity();
	
		String receiverLoggableName = recipientActorId.getBaseSubject().getIRIString();
		CmdReceiveActivityInboxTyped cmdTyped = new CmdReceiveActivityInboxTyped(recipientActorId, receiverLoggableName, activity, cmd.getSignatureVerifier());
		cmdTyped.setRemoteHost(cmd.getRemoteHost());
		perform(cmdTyped);
	}
	
	@Override
	public void perform(CmdReceiveActivityInboxTyped cmd) throws BadRequestException, GoneException {
		RdfPubMDC.put("inbox receive");
		try {
			performInternal(cmd);
		} finally {
			RdfPubMDC.remove();
		}
	}
		
	private void performInternal(CmdReceiveActivityInboxTyped cmd) throws BadRequestException, GoneException {		
		
		RdfPubObjectId receiverId = cmd.getReceiverId();
		Optional<ReceiverStore> receiverStoreOpt = actorsStoreRepository.findReceiverStore(cmd.getReceiverId());
		if(receiverStoreOpt.isPresent()) {
			ReceiverStore receiverStore = receiverStoreOpt.get(); 
			Activity activity = cmd.getActivity();
			
			InboxUseCaseContext ucCtx = useCasesDeterminatorReceiveInbox.determinateInbox(activity, receiverStore,
					receiverId, cmd.getReceiverLoggableName(), cmd.getSignatureVerifier().orElse(null));
	
			process(activity, ucCtx);
		} else {
			log.trace("no receiverStore for: " + cmd.getReceiverLoggableName());
			throw new GoneException("no receiverStore for: " + cmd.getReceiverLoggableName());
		}
	}

	private void process(Activity activity, InboxUseCaseContext ucCtx) throws BadRequestException {
		try {
			RdfPubMDC.put(ucCtx.getUseCase().name());
			// Some instances did not set Published date. thats awefull, i do that. I don't
			// want activities without Published, because then i cannot sort them by date.
			if (activity.getPublished().isEmpty()) {
				activity.setPublished(Instant.now());
			}
			InboxUsecaseProcessor usecaseProcessor = usecaseProcessorFactory.create(ucCtx);
			usecaseProcessor.process();
		} finally {
			RdfPubMDC.remove();
		}
	}
}
