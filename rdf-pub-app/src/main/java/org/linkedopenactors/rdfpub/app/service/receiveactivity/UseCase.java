package org.linkedopenactors.rdfpub.app.service.receiveactivity;

/**
 * Enumeration for supported UseCases.
 */
public enum UseCase {

	/**
	 * Another Server sends a follow request.
	 */
	s2s_followRequest, 
	
	/**
	 * An Actor on the same Server sends a follow request to receivers. We do not need to verify a HTTP Signature.
	 */
	s2s_followInternal, 
	
	/**
	 * Another Server sends an create activity with a object.
	 */
	s2s_createObject, 
	
	/**
	 * An Actor on the same Server sends a create request with an object to receivers. We do not need to verify a HTTP Signature.
	 */
	s2s_createObjectInternal, 
	
	/**
	 * Another Server sends an update activity with a object.
	 */
	s2s_updateObject,
	
	/**
	 * Another Server sends an undo activity with a follow request as object.
	 */
	s2s_undoFollow, 
	
	/**
	 * An Actor on the same Server sends a follow request to receivers. We do not need to verify a HTTP Signature.
	 */
	s2s_undoFollowInternal, 
	
	/**
	 * Another Server sends a delete request.
	 */
	s2s_deleteRequest, 
	
	/**
	 * Another Server sends a like request.
	 */
	s2s_like, 
	
	/**
	 * A Client of rdf-pub sends an C2S_Accept_follow, that is delivered to a remotes server!
	 */
	@Deprecated // it think this is done in UcC2SAcceptFollow. so this is maybe dead code ?!
	s2s_accept_follower,
	
	/**
	 * A remote server sends an accept_follow as response of a follow_request from rdf-pub
	 */
	s2s_accept_following,
	
	/**
	 * One of our actors sends a follow request.
	 */
	c2s_followRequest, 
	
	/**
	 * One of our actors want's to ignore a follow request.
	 */
	c2s_ignoreFollowRequest, // TODO remove 
	
	/**
	 * One of our actors want's to reject a follow request.
	 */
	c2s_rejectFollowRequest, 

	/**
	 * One of our actors want's to create an object.
	 */
	c2s_createObject, 
	
	/**
	 * One of our actors want's to update an object.
	 */
	c2s_updateObject, 
	
	/**
	 * One of our actors want's to undo a follow request.
	 */
	c2s_undoFollow, 

	/**
	 * One of our actors want's to undo a follow request.
	 */
	c2s_removeFollower, 

	/**
	 * One of our actors sends a delete request.
	 */
	c2s_deleteRequest,
	
	/**
	 * One of our actors accepts a follow request.
	 */
	c2s_accept_follow
}
