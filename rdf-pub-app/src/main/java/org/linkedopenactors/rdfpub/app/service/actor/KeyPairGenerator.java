package org.linkedopenactors.rdfpub.app.service.actor;

import java.security.PrivateKey;
import java.security.PublicKey;

public interface KeyPairGenerator {
	PublicKey generate(String identifier);
	PrivateKey getPrivateKey(String identifier);
	PublicKey getPublicKey(String identifier);
	String getPublicKeyBase64Encoded(String identifier);
}