package org.linkedopenactors.rdfpub.app.service.receiveactivity;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s.UsecaseProcessorFactoryC2S;
import org.linkedopenactors.rdfpub.domain.AT;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

import io.sentry.spring.jakarta.tracing.SentrySpan;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class OutboxReceiveActivityServiceDefault implements OutboxReceiveActivityService {

	private org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory activityPubObjectFactory;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private UsecaseProcessorFactoryC2S usecaseProcessorFactoryC2S;
	private OutboxUseCasesDeterminator useCasesDeterminatorReceiveOutbox;
	private VocabContainer vocabContainer;
	
	public OutboxReceiveActivityServiceDefault(
			VocabContainer vocabContainer,
			org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory activityPubObjectFactory,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder,
			UsecaseProcessorFactoryC2S usecaseProcessorFactoryC2S,
			OutboxUseCasesDeterminator useCasesDeterminatorReceiveOutbox) {
		this.vocabContainer = vocabContainer;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
		this.usecaseProcessorFactoryC2S = usecaseProcessorFactoryC2S;
		this.useCasesDeterminatorReceiveOutbox = useCasesDeterminatorReceiveOutbox;
	}

	@SentrySpan
	@Override
	public RdfPubObjectIdActivity perform(CmdReceiveActivityOutbox cmd) {
		RdfPubObjectIdActor recipientActorId =  activityPubObjectIdBuilder.createActor(cmd.getRecipientActorsId());
		org.linkedopenactors.rdfpub.domain.ActivityPubObject activityPubObject = activityPubObjectFactory.create(cmd.getRdfFormat(),cmd.getReceivedActivity());
		return perform(new CmdReceiveActivityOutboxTyped(recipientActorId, activityPubObject, cmd.getAuthenticatedActorHolder(), cmd.getUserAgent()));
	}		

	@SentrySpan
	@Override
	public RdfPubObjectIdActivity perform(CmdReceiveActivityOutboxTyped cmd) {
		RdfPubMDC.put("outbox receive");
		try {
			return performInternal(cmd);
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	private RdfPubObjectIdActivity performInternal(CmdReceiveActivityOutboxTyped cmd) {
		
		// TODO is there a match between the actorId in the url and the current one ??
		// actorId == actorIdParam

		AuthenticatedActor authenticatedActor = cmd.getAuthenticatedActorHolder().getAuthenticatedActor().orElseThrow();
		org.linkedopenactors.rdfpub.domain.ActivityPubObject rootAo = cmd.getActivityPubObject();
		org.linkedopenactors.rdfpub.domain.Activity activity;

		RdfPubObjectIdActor authenticatedActorId = authenticatedActor.getActorId();
		
		if(rootAo.isActivity()) {
			activity = rootAo.asConvertable().asActivity();
			activity.addAttributedTo(authenticatedActorId.getBaseSubject());
			activity.addActor(authenticatedActorId.getBaseSubject()); // TODO redundant check !?!?!
		} else {
			activity = createSurroundingActivity(authenticatedActorId, vocabContainer.vocAs().Create(), rootAo);
			activity.setObjects(Set.of(rootAo));
		}
		log.debug("received activity: \n" + activity);

		AuthenticatedActorHolder authenticatedActorHolder = cmd.getAuthenticatedActorHolder();
		OutboxUseCaseContext ucCtx = useCasesDeterminatorReceiveOutbox.determinateOutbox(authenticatedActorHolder.getAuthenticatedActor().orElseThrow(), activity);
		ucCtx.setOutboxOwner(authenticatedActor.getActor());
		OutboxUsecaseProcessor processor = usecaseProcessorFactoryC2S.create(ucCtx);		
		RdfPubObjectIdActivity a = processor.process(authenticatedActorHolder);
		return a;
	}

	/**
	 * Creates a surrounding activity if the client-app was not sending one.
	 * @param actorId The {@link RdfPubObjectIdActor} of the actor that was sending the activity
	 * @param type the type of the surrounding activity.
	 * @param root The object that have to be 'surrounded'.
	 * @return A newly created surrounding {@link Activity}.
	 */
	private Activity createSurroundingActivity(RdfPubObjectIdActor actorId, RdfPubIRI type, ActivityPubObject root) {
		
		RdfPubObjectIdActivity rdfPubObjectIdActivity = activityPubObjectIdBuilder.createActivity(actorId);
		org.linkedopenactors.rdfpub.domain.Activity activity = activityPubObjectFactory.create(rdfPubObjectIdActivity).asConvertable().asActivity();
		activity.setType(Set.of(type));
		String activityType = vocabContainer.vocAs().withoutNameSpace(type);
		String activityName = activityType + "." + root.getName().orElse("someObject");
		
		activity.setName(activityName);
		activity.setSummary("This activity wraps an object that was created without a surrounding activity.");
		activity.setActor(Set.of(actorId.getBaseSubject()));
		activity.addAttributedTo(actorId.getBaseSubject());
		activity.setObject(Set.of(root.getSubject()));
		activity.setPublished(Instant.now());
		return activity;
	}

	@Data
	public class VersionActivityPair {
	    private final BigInteger version;
	    private final org.linkedopenactors.rdfpub.domain.Activity activity;
	}
}