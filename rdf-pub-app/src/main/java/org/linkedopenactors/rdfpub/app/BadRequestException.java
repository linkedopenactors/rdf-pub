package org.linkedopenactors.rdfpub.app;

/**
 * An actor has sent a message that we cannot process.
 */
public class BadRequestException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BadRequestException() {
		super();
	}

	public BadRequestException(String message) {
		super(message);
	}
}
