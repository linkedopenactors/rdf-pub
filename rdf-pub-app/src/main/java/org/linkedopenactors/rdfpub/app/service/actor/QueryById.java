package org.linkedopenactors.rdfpub.app.service.actor;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

import lombok.Getter;

@Getter
public class QueryById extends CmdQueryBaseAbstract {
	
	private final String id;

	/**
	 * 
	 * @param authenticatedActorHolder Information about a authenticated user.
	 * @param id
	 * @param userAgent Info about the caller of the service.
	 */
	public QueryById(AuthenticatedActorHolder authenticatedActorHolder, String id, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.id = id;
	}
}
