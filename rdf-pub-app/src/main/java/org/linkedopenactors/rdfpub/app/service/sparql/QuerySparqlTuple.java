package org.linkedopenactors.rdfpub.app.service.sparql;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;

public class QuerySparqlTuple extends CmdQueryBaseAbstract {

	private final String query;
	
	public QuerySparqlTuple(String query, AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.query = query;
	}

	public String getQuery() {
		return query;
	}
}
