package org.linkedopenactors.rdfpub.app.object;

import lombok.Data;

@Data
public class CmdReadPublicCollection {
	private Integer pageSize;
	private Integer startIndex;
}
