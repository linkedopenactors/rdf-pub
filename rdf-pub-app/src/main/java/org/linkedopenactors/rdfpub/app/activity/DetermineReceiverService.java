package org.linkedopenactors.rdfpub.app.activity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.IRI;
import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DetermineReceiverService {

	@Value("${instance.domain}")
	private String instanceDomain; // TODO centralise ?
	
	@Autowired
	private ExternalObjectRepository externalObjectRepository;
	
	@Autowired
	private VocabContainer vocabContainer; 
	
	@Autowired
	private PublicActorStore actorsStore;

	/**
	 * The receivers are already unified !!! It's ok, just pay attention to the activity!
	 * @param activity
	 * @return {@link IRI}s of the internal receivers.
	 */
	public Set<RdfPubBlankNodeOrIRI> getInternalReceiverIris(org.linkedopenactors.rdfpub.domain.Activity activity) {
		
		// specialReceivers like AS.Public, Followers,...
		Set<RdfPubBlankNodeOrIRI> specialReceivers = Set.of(vocabContainer.vocAs().Public());
		
		Set<RdfPubBlankNodeOrIRI> receivers = activity.getReceivers();
		// ^^ ACHTUNG hier sind nun auch externe, oder ?
		
		Set<RdfPubBlankNodeOrIRI> internalReceivers = receivers.stream()
				.filter(r->isInstanceIri(r))
				.collect(Collectors.toSet());
				
		Set<RdfPubBlankNodeOrIRI> specialReceiversToUse = receivers.stream()
				.filter(specialReceivers::contains)
				.collect(Collectors.toSet());
		
		Set<RdfPubBlankNodeOrIRI> receiversCopy = new HashSet<>(internalReceivers); 
		receiversCopy.removeAll(specialReceivers); // remove these, they are in the way when determining the inbox endpoints
		// ^^ should not be the case any more, because we only have instance Domain Iris !
		Set<RdfPubBlankNodeOrIRI> receiversEndpoints = determineInternalReceiversInboxEndpoints(receiversCopy);

		Set<RdfPubBlankNodeOrIRI> result = new HashSet<>();		
		result.addAll(specialReceiversToUse);
		result.addAll(receiversEndpoints);
		log.trace("receiversEndpoints: " + receiversEndpoints);
		return result;
	}
	
	/**
	 * The receivers are already unified !!! It's ok, just pay attention to the activity!
	 * @param activity
	 * @return {@link IRI}s of the external receivers
	 */
	public Set<RdfPubBlankNodeOrIRI> getExternalReceiverInboxIris(org.linkedopenactors.rdfpub.domain.Activity activity) {
		
		// specialReceivers like AS.Public, Followers,...
		Set<RdfPubBlankNodeOrIRI> specialReceivers = Set.of(vocabContainer.vocAs().Public());
		
		Set<RdfPubBlankNodeOrIRI> receivers = activity.getReceivers();
		
		Set<RdfPubBlankNodeOrIRI> externalReceivers = receivers.stream()
				.filter(rec->!isInstanceIri(rec))
				.collect(Collectors.toSet());
				
		Set<RdfPubBlankNodeOrIRI> specialReceiversToUse = receivers.stream()
				.filter(specialReceivers::contains)
				.collect(Collectors.toSet());
		
		Set<RdfPubBlankNodeOrIRI> externalReceiversCopy = new HashSet<>(externalReceivers); 
		externalReceiversCopy.removeAll(specialReceivers); // remove these, they are in the way when determining the inbox endpoints
		Set<RdfPubBlankNodeOrIRI> receiversEndpoints = determineExternalReceiversInboxEndpoints(externalReceiversCopy);

		Set<RdfPubBlankNodeOrIRI> result = new HashSet<>();		
		result.addAll(specialReceiversToUse);
		result.addAll(receiversEndpoints);
		log.trace("receiversEndpoints: " + receiversEndpoints);
		return result;
	}

	
	private Set<RdfPubBlankNodeOrIRI> determineExternalReceiversInboxEndpoints(Set<RdfPubBlankNodeOrIRI> receivers) {
		Set<org.linkedopenactors.rdfpub.domain.Actor> receiversSet = receivers.stream()
				.map(this::findExternal)
				.flatMap(Optional<org.linkedopenactors.rdfpub.domain.Actor>::stream)
				.collect(Collectors.toSet());
		// TODO sharedInbox !!
		
		Set<RdfPubBlankNodeOrIRI> receiversInboxEndpoints = receiversSet.stream()
				.map(org.linkedopenactors.rdfpub.domain.Actor::getInbox)
				.collect(Collectors.toSet());
		
		return receiversInboxEndpoints;
	}
	
	private Set<RdfPubBlankNodeOrIRI> determineInternalReceiversInboxEndpoints(Set<RdfPubBlankNodeOrIRI> receivers) {
		// TODO is it still working with AS.public, etc?
		Set<org.linkedopenactors.rdfpub.domain.Actor> receiversSet = receivers.stream()
				.map(actorsStore::findBySubject)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor)
				.collect(Collectors.toSet());

		Set<RdfPubBlankNodeOrIRI> receiversInboxEndpoints = receiversSet.stream()
				.map(org.linkedopenactors.rdfpub.domain.Actor::getInbox)
				.collect(Collectors.toSet());
		
		Set<RdfPubBlankNodeOrIRI> receiversEndpoints = receiversSet.stream()
				.map(org.linkedopenactors.rdfpub.domain.Actor::getSubject)
				.collect(Collectors.toSet());
		receiversInboxEndpoints.addAll(receiversEndpoints);
		return receiversInboxEndpoints;

	}

	private Optional<org.linkedopenactors.rdfpub.domain.Actor> findExternal(RdfPubBlankNodeOrIRI actorId) {
		return externalObjectRepository.read(actorId)
					.map(org.linkedopenactors.rdfpub.domain.ActivityPubObject::asConvertable)
					.map(org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable::asActor);
	}
	
	private boolean isInstanceIri(RdfPubBlankNodeOrIRI iri) {
		return iri.getIRIString().startsWith(instanceDomain);
	}
}
