package org.linkedopenactors.rdfpub.app.service.readobject;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.ExternalObjectRepository;
import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.instance.InstanceService;
import org.linkedopenactors.rdfpub.app.object.CmdFindObjectById;
import org.linkedopenactors.rdfpub.app.object.QueryService;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UcReadObject {

	@Autowired
	private ExternalObjectRepository externalObjectRepository;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private InstanceService instanceService;
	
	@Autowired
	private QueryService queryService;
	
	public Optional<ActivityPubObject> perform(QueryReadObject queryReadObject) {
		RdfPubMDC.put(this.getClass().getSimpleName());
		try {
			Optional<ActivityPubObject> found = performInternal(queryReadObject);
			log.debug("found("+queryReadObject.getUrl()+"): " + found.isPresent());
			return found;
		} finally {
			RdfPubMDC.remove();
		}
	}
		
	private Optional<ActivityPubObject> performInternal(QueryReadObject queryReadObject) {		
		Actor currentAuthenticatedActor = queryReadObject.getAuthenticatedActorHolder().getAuthenticatedActor().map(AuthenticatedActor::getActor).orElse(null);
		
		String requestedUrl = queryReadObject.getUrl();
		if(requestedUrl.startsWith("/")) {
			requestedUrl = instanceService.getInternalUrnExtendedWith(requestedUrl).getIRIString();
		}
		RdfPubBlankNodeOrIRI requestedIri = activityPubObjectIdBuilder.createFromUrl(requestedUrl);
		boolean isIntern = requestedIri.isRdfPubObjectId();
		Optional<ActivityPubObject> result;
		if(isIntern) {
			result = read(requestedIri, queryReadObject.getDeep(), currentAuthenticatedActor, RdfFormat.JSONLD);
		} else {
			result = externalObjectRepository.readWithSignature(requestedIri.getIRIString(), currentAuthenticatedActor);
		}
		return result;
	}

	private Optional<ActivityPubObject> read(RdfPubBlankNodeOrIRI requestedIri, int deep, Actor actorParam, RdfFormat format) {
		CmdFindObjectById cmd = new CmdFindObjectById();
		Optional<Actor> actorOpt = Optional.ofNullable(actorParam);
		actorOpt.ifPresent(actor->cmd.setAuthenticatedActor(actor));
		cmd.setId(requestedIri);
		cmd.setDeep(deep);
		actorOpt.ifPresent(actor->cmd.setOwnerId(actor.getSubject().asActor().orElseThrow()));
		
		Optional<ActivityPubObject> object = queryService.perform(cmd);
		return object;		
	}
}
