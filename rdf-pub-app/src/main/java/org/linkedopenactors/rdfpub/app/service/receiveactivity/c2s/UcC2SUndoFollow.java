package org.linkedopenactors.rdfpub.app.service.receiveactivity.c2s;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.receiveactivity.OutboxUseCaseContext;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;

/**
 * An actor on this server wants to undo a following request. 
 */
class UcC2SUndoFollow extends AbstractUcC2SCreateObject {

	private OutboxUseCaseContext ucCtx;
	private InternalDelivery internalDelivery;
	private ExternalDelivery externalDelivery;

	public UcC2SUndoFollow(OutboxUseCaseContext ucCtx, InternalDelivery internalDelivery,
			ExternalDelivery externalDelivery) {
		super();
		this.ucCtx = ucCtx;
		this.internalDelivery = internalDelivery;
		this.externalDelivery = externalDelivery;
	}

	/**
	 * Process the usecase.
	 * @return the subject / location of the created activity
	 */
	@Override
	public RdfPubObjectIdActivity process(AuthenticatedActorHolder authenticatedActorHolder) {
		adjustActivityAndObjects(ucCtx.getOutboxOwner().getSubject().asActor().orElseThrow(), ucCtx.getActivity());
		RdfPubObjectIdActivity addedActivitySubject = ucCtx.getRecipientActorStore().addToOutbox(ucCtx.getActivity(), "ReceiveActivityOutboxServiceDefault#perform create");
		String outboxOwnerLoggableName = ucCtx.getOutboxOwner().getPreferredUsername().orElse(ucCtx.getOutboxOwner().getSubject().getIRIString());
		internalDelivery.deliver(ucCtx.getRecipientActorStore(), addedActivitySubject, outboxOwnerLoggableName);
		externalDelivery.deliver(ucCtx.getRecipientActorStore(), ucCtx.getOutboxOwner(), addedActivitySubject);
		RdfPubObjectIdActivity activityIri = addedActivitySubject;
		
		ucCtx.getPendingFollowingCache().removePendingFollowActivity(activityIri);
		return activityIri;
	}
}
