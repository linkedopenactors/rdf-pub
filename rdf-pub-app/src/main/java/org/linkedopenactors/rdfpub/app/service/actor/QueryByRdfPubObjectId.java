package org.linkedopenactors.rdfpub.app.service.actor;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.service.CmdQueryBaseAbstract;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;

import lombok.Getter;

/**
 * Query object.
 */
@Getter
public class QueryByRdfPubObjectId extends CmdQueryBaseAbstract {

	private  RdfPubObjectId id;

	/**
	 * Constructor.
	 * @param authenticatedActorHolder Information about a authenticated user.
	 * @param id The {@link RdfPubObjectId} that represents a unique id of an object.
	 * @param userAgent Info about the caller of the service.
	 */
	public QueryByRdfPubObjectId(AuthenticatedActorHolder authenticatedActorHolder, RdfPubObjectId id, UserAgent userAgent) {
		super(authenticatedActorHolder, userAgent);
		this.id = id;
	}
}
