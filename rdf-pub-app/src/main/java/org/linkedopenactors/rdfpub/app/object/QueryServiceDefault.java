package org.linkedopenactors.rdfpub.app.object;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.service.readcollection.QueryReadCollection;
import org.linkedopenactors.rdfpub.app.service.readcollection.QueryReadCollection4Actor;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class QueryServiceDefault implements QueryService {
	
	public enum QueriableCollection {
		SparqlOutboxCommand, SparqlInboxCommand;
	}
			
	private ActorsStoreRepository actorsStoreRepository;
	private PublicActorStore publicActorStore;
	private VocabContainer vocabContainer;
	
	public QueryServiceDefault(VocabContainer vocabContainer, ActorsStoreRepository actorsStoreRepository, PublicActorStore publicActorStore) {
		this.vocabContainer = vocabContainer;
		this.actorsStoreRepository = actorsStoreRepository;
		this.publicActorStore = publicActorStore;
	}
	
	boolean isPublic(QueryReadCollection query) {
		return Optional.ofNullable(query)
			.map(QueryReadCollection::getCollectionInfo)
			.map(RdfPubObjectIdCollection::getIRIString)
			.map(x->x.equals(vocabContainer.vocAs().Public().getIRIString()))
			.orElse(false);
	}

	boolean isPublic(QueryReadCollection4Actor query) {
		return Optional.ofNullable(query)
			.map(QueryReadCollection4Actor::getCollectionInfo)
			.map(RdfPubObjectIdCollection::getIRIString)
			.map(x->x.equals(vocabContainer.vocAs().Public().getIRIString()))
			.orElse(false);
	}

	@Override
	public OrderedCollectionPage perform(CmdReadActors c) {
		return publicActorStore.getPersons(c.getPageSize(), c.getStartIndex());
	}
	
	@Override
	public Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> perform(CmdFindObjectById cmd) {
		RdfPubBlankNodeOrIRI requestedUrlIri = cmd.getId();
		Optional<org.linkedopenactors.rdfpub.domain.Actor> authenticatedActorIdOpt = Optional.ofNullable(cmd.getAuthenticatedActor());
		Optional<org.linkedopenactors.rdfpub.domain.ActivityPubObject> activityPubObjectOpt = Optional.empty();
		
		if(authenticatedActorIdOpt.isPresent()) {
			org.linkedopenactors.rdfpub.domain.Actor authenticatedActor = authenticatedActorIdOpt.orElseThrow();
			if(Objects.isNull(authenticatedActor.getSubject())) {
				throw new IllegalStateException("authenticatedActor.getSubject() is mandatory!");
			}
			
			RdfPubBlankNodeOrIRI authenticatedActorId = authenticatedActor.getSubject();
			log.trace("Reading '" + requestedUrlIri + "' for userId: " + authenticatedActor.getOauth2IssuerUserId().orElse("")
					+ "- name: " + authenticatedActor.getName().orElse("") + " - actorSubject: " + authenticatedActorId);
			
			ActorsStore actorsStore = actorsStoreRepository.find(authenticatedActor).orElseThrow();

			
			activityPubObjectOpt =  actorsStore.findBySubject(requestedUrlIri);
			
			// Read for foreign requester
			if(activityPubObjectOpt.isEmpty()) {
				activityPubObjectOpt = findForForeigUser(cmd.getOwnerId(), authenticatedActor.getSubject().asActor().orElseThrow(), requestedUrlIri);
			}
			log.trace("found: " + activityPubObjectOpt);			
		} 
		if(activityPubObjectOpt.isEmpty()) {
			activityPubObjectOpt = findPublic(requestedUrlIri, cmd.getDeep());
		}
		return activityPubObjectOpt;
	}

	
	private Optional<ActivityPubObject> findPublic(RdfPubBlankNodeOrIRI requestedUrlIri, int deep) {
		// actor profile is in public, but others are in the owner repo!
		Optional<ActivityPubObject> found = publicActorStore.findBySubject(requestedUrlIri, deep);
 		return found;
	}
	
	private Optional<ActivityPubObject> findForForeigUser(RdfPubObjectIdActor owner, RdfPubObjectIdActor authenticatedActor, RdfPubBlankNodeOrIRI requestedUrlIri) {
		Optional<ActorsStore> ownersStoreOpt = actorsStoreRepository.find(owner);
		Optional<ActivityPubObject> foundOpt = ownersStoreOpt.flatMap(ownersStore -> ownersStore.findBySubject(requestedUrlIri));
		
		if(foundOpt.isPresent()) {
			Set<RdfPubBlankNodeOrIRI> receivers = foundOpt.get().getReceivers();
			boolean isReceiver = receivers.contains(authenticatedActor.getExternalSubject());
			if(!isReceiver) {
				log.debug("authenticatedActor ("+authenticatedActor+") is no receiver !!");	
			}			
			if(isReceiver) {
				ActivityPubObject found = foundOpt.get();
				found.hideBlindReceivers();
				return Optional.ofNullable(found); 
			}
		}
		return Optional.empty();
	}

//	@Override
//	public OutputStream perform(CmdSparqlPublic cmd) throws IOException {
//		throw new UnsupportedOperationException();
//	}
//
//	@Override
//	public OutputStream perform(CmdSparqlCollection cmd) throws IOException {
//		throw new UnsupportedOperationException();
//		w
//	}
}