package org.linkedopenactors.rdfpub.app.instance;

import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.stereotype.Component;

@Component
class InstanceServiceDefault implements InstanceService {

	private final org.linkedopenactors.rdfpub.domain.InstanceProperties instanceProperties;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	public InstanceServiceDefault(org.linkedopenactors.rdfpub.domain.InstanceProperties instanceProperties,
			ActivityPubObjectFactory activityPubObjectFactory, ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.instanceProperties = instanceProperties;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}

	@Override
	public RdfPubBlankNodeOrIRI getInternalUrnExtendedWith(final String extensionParam) {
		String ids = instanceProperties.getInternaleIriPrefix();
		String domainString = ids;
		String extension = extensionParam.startsWith("/") ? extensionParam.substring(1, extensionParam.length()) : extensionParam;
		return activityPubObjectIdBuilder.createFromUrl(domainString + extension);
	}

	@Override
	public org.linkedopenactors.rdfpub.domain.InstanceProperties getInstanceProperties() {
		return instanceProperties;
	}
}
