package org.linkedopenactors.rdfpub.app.service;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;

public abstract class CmdQueryBaseAbstract {
	private final AuthenticatedActorHolder authenticatedActorHolder;
	private final UserAgent userAgent;
	
	public CmdQueryBaseAbstract(AuthenticatedActorHolder authenticatedActorHolder, UserAgent userAgent) {
		this.authenticatedActorHolder = authenticatedActorHolder;
		this.userAgent = userAgent;		
	}

	public AuthenticatedActorHolder getAuthenticatedActorHolder() {
		return authenticatedActorHolder;
	}

	public UserAgent getUserAgent() {
		return userAgent;
	}
}
