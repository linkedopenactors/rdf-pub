package org.linkedopenactors.rdfpub.app.auth;

import java.net.URI;

public interface Authentication {

	/**
	 * @return PreferredUsername
	 */
	String getIssuerPreferredUsername();

	/**
	 * @return UserId
	 */
	String getIssuerUserId();

	/**
	 * @return True, if a user is authenticated.
	 */
	boolean isAuthenticated();

	URI getIssuer();
}