package org.linkedopenactors.rdfpub.app.adapter;

import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;

public interface ObjectAdapter {
	String adjust(ActivityPubObject activityPubObject, RdfFormat rdfFormat, UserAgent userAgent);
}
