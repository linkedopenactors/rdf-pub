.uc_follow2_undoFollow-undo_activity
[source, json]
----
{
  "id" : "https://rdfpub.org/0816",
  "type" : [ "Undo" ],
  "actor" : "https://rdfpub.org/max",
  "attributedTo" : "https://rdfpub.org/max",
  "object" : {
    "id" : "https://rdfpub.org/0817",
    "type" : [ "Follow"],
    "actor" : "https://rdfpub.org/max",
    "attributedTo" : "https://rdfpub.org/max",
    "object" : "https://rdfpub.org/0815",
    "to" : "https://mast.org/jon"
  },
  "to" : "https://mast.org/jon",
  "@context" : [ "https://www.w3.org/ns/activitystreams" ]
}

----
