= Supported Use Cases

* link:webfinger.html[Resolving users] with Webfinger (link:https://datatracker.ietf.org/doc/html/rfc7033[rfc7033])
* link:serverProfile.html[Request the Profile of the Instance Actor]
* link:actorsProfile.adoc[Request the Profile of an  Actor]
* link:serverMetaData.html[OAuth 2.0 Authorization Server Metadata] link:https://www.rfc-editor.org/rfc/rfc8414[(rfc8414)]
* link:registeringInKeycloak.html[Register a user in Keycloak]
* link:postmanAccessToken.html[Request an access token using postman]
* link:postActivity.html[Posting Activities to an actor's outbox] 