:homepage: https://rdf-pub.org

:keywords: software-architecture, documentation, arc42, activity-pub

:numbered!:
**About rdf-pub**

[role="lead"]
rdf-pub is an activity-pub server implementation, that is not limited to the activity-stream vocabulary, but supports RDF per se.

Version {project-version}

Created, maintained and (C) by Fred Hauschel and contributors.
See https://rdfpub.org.


