= October 2024 - Testing, fixing, renew the client, Feditest


That took quite a while. That's because I actually wanted to take some time off. But now I'm back at the computer ;-)

But at least I'm in southern Spain by the sea.

Today i merged another big thing bacḱ into the develop branch!

=== Testing 
After the architecture and domainmodel refactoring, there was still a lot to do. Many manual tests revealed errors. These were fixed.

=== Renew the client
The old ‘loa-application’ application has had its day, I have a new one in the rdf-pub project: ‘rdf-pub-demo-app’

This no longer has the LOA focus, but should primarily support me in the development of rdf-pub and give other interested parties access to rdf-pub to play and test.

I am considering whether to implement ui admin functionalities directly in rdf-pub or in ‘rdf-pub-demo-app’. That will be decided soon.

=== Feditest
I have successfully integrated version 0.3 "https://feditest.org/" into my ci pipeline. The job is started manually and tests against https://dev.rdf-pub.org.

This is not optimal and somewhat misses the basic idea of ci. but it is a good start. 

At the moment I still have a webfinger problem, which is due to the type of deployment or the infrastructure configuration. I need to clarify this with my admin.

== Next Steps
I need to test more. e.g. with activitypub.academy. I don't really have an overview of the activities and objects that go back and forth. 

That's why I still need some admin functionalities that allow me to gain insight graphically.

I would like to implement the following process and the sending of notes.

The documentation should also be improved. I have already started on this, but it is just the beginning: https://rdf-pub.org/arc42-template.html

The Website https://rdf-pub.org should become a little more user-friendly and appeal to different user groups. I would like to have the support of a marketing ‘specialist’ here.

I would still be happy to hear from someone who would like to develop a web client for rdf-pub. It's still very early days. But the sooner it is started, the better. It must be clear that this is a big task, because C2S is not sufficiently described and a C2S-based web client will be a big challenge and will involve a lot of discussion and coordination with the community.

