# Fred Robert Hauschel
Geboren 30.1.1973
Staatsangehörigkeit: deutsch
Familienstand: ledig

**Mein Angebot**

- Softwarearchitektur
- Quality Engineering
- Java-Entwicklung
- Continuous Integration / Continuous Delivery
- Unterstützung in allen Projektphasen
- mehr als 20 Jahre Projekterfahrung als freiberuflicher IT Berater
- Sprachen D (Muttersprache), EN (B1)

| Know-how und Erfahrung |  |
| ------ | ------ |
| Ausbildung | Kommunikationselekroniker Fachrichtung Telekommunikation , staatlich anerkannter Industrieinformatiker |
| Zertifizierungen | ITIL® Foundation Certificate in IT Service Management, iSQI Certified Professional for Software Architecture, Certified Scrum Master  |
| Programmierung | Java, Spring-Boot, Webservices, Open API, Eclipse, JUnit, Mockito, RDF |
| Source Code Management | GIT, SVN, CVS |
| CI/CD | Gitlab-CI, Jenkins, Maven, Nexus, Sonar, Jfrog Artifactory, Docker |
| Datenbanken | Postgres, MySQL, H2, Oracle, Hibernate, JDBC, JPA, Squirrel, RDF4J |
| Betriebssysteme | Linux, Unix, Windows, MS-Dos |
| Sonstiges | Confluence Wiki, Domain Specific Language (DSL), UML, Enterprise Architect, Gliffy, Draw.io, Clear Quest, Jira, Mantis, JMS, Apache-Camel |
| Branchen | Automotive, Banken, Energieversorger, Medien, Telekommunikation, NPO |

## Schlüsselqualifikationen
- Mehr als 20 Jahre Erfahrung in Projekten verschiedener Konzerne und Branchen.
- Mehr als 10 Jahre Erfahrung in agilen Projekten
- Mehrere Jahre Erfahrung als Team-Lead bzw. Chef Designer & Architekt
- Mehr als 20 Jahre Erfahrung in der Planung, Konzeption, Gestaltung und technischen Umsetzung von Schnittstellen zwischen Systemen.
- Mehr als 20 Jahre Erfahrung in der Nutzung und Einbeziehung von Open Source Technologien und Frameworks für webbasierte Anwendungen und Services.
- Rund 10 Jahre Erfahrung in der Planung, Modellierung von Datenbanksystemen
- 20 Jahre Erfahrung in der Nutzung von SCM Systemen wie CVS, SVN, Git
- 10 Jahre Erfahrung in Continuous Integration und Continuous Delivery, Release Management

## Projekte

### November 2021 bis heute - rdf-pub (OSS)
Kunde: Non Profit Organisation

Projekt: https://gitlab.com/linkedopenactors/rdf-pub

Rolle: Software-Architekt, Software-Entwickler

Projektinhalt: Umsetzung einer Activity-Pub Implementierung für generische RDF Objekte

Aufgaben:

- Erarbeitung und Dokumentation der Anforderungen

- Umsetzung: Event Driven Architecture mit Enterprise Architecture Patterns

Technologien: Resource Description Framework (RDF), Json-LD, Turtle, Linked Open Data (LOD), RDF4J, Docker, Gitlab, Gitlab-CI, Gitlab-Pages (asciidoctor-maven-plugin), Sonatype (oss.sonatype.org), gitflow-maven-plugin, Java (17), Spring-Boot, Activity-Pub


### März 2020 bis heute - Linked Open Actors (OSS)
Kunde: Non Profit Organisation

Projekt: https://linkedopenactors.org/

Rolle: Software-Architekt, Software-Entwickler

Projektinhalt: Erarbeiten eines Standards der beschreibt wie Daten von Akteuren (Personen, Gruppen und Organisationen) präsentiert, gespeichert und ausgetauscht werden können.

Aufgaben:

- Abstimmung mit verschiedenen Initiativen/Stakeholdern

- Erarbeitung und Dokumentation der Anforderungen

- Erarbeitung/Auswahl des Datenmodels

- Implementierung eines Prototyps

- Anbindung der Plattformen WECHANGE und Karte von Morgen

- Auswahl der Technologie um Kategorien abzubilden (SKOS)

- Auswahl und Koordination der Bereitstellung eines Tools um Kategorien zu erfassen (iQvoc)

- Kompatibilität mit ActivityPup Protokoll sicherstellen

Technologien: Resource Description Framework (RDF), Json-LD, Turtle, Linked Open Data (LOD), SKOS, RDF4J, Docker, Gitlab, Gitlab-CI, Gitlab-Pages (asciidoctor-maven-plugin), JavaWuzzy (Levenshtein distance), Sonatype (oss.sonatype.org), gitflow-maven-plugin, Java (11), Spring-Boot, Spring-Webflux

### Oktober 2020 bis November 2021 - Plattform Smarte.Land.Regionen (geplant als OSS)
Kunde: Fraunhofer-Institut für Experimentelles Software Engineering IESE

Rolle: Software-Architekt, Software-Entwickler

Projektinhalt: Bereitstellen einer Plattform, die Kunden Lösungen in einem Marktplatz anbietet und das Management von Organisationen, Lösungen, Bestellungen und Angeboten übernimmt.

Aufgaben:

- Analyse einer bestehenden Plattform und Einschätzung der Wiederverwendbarkeit/Erweiterbarkeit

- Erarbeitung/Dokumentation einer Architektur für eine neue Plattform (Microservice)

- Umsetzung erster Microservices, die als Template dienen.

- Anbindung an Keycloak, Konfiguration über Keycloak REST-API

- Umsetzung eines asynchronen Taskmanagements auf Basis von ActivityPub

Technologien: Resource Description Framework (RDF), Json-LD, Turtle, RDF4J, Docker, Gitlab, Gitlab-CI, gitflow-maven-plugin, Java (11), Spring-Boot, Spring-Webflux, Keycloak, Testcontainers, Confluence, Jira, Spring-Config-Server, Spring-JPA


### März 2020 bis August 2020 - Plattform ernte-erfolg.de (OSS)
Kunde: Non Profit Organisation

Projekt: https://gitlab.com/ernte-erfolg/ernte-erfolg-service

Rolle: Software-Architekt, Software-Entwickler

Projektinhalt: Innerhalb der Kampagne der Bundesregierung (#wirvsvirus) entstand aus einem Hackathon das Projekt „ernte-erfolg“, das Betriebe und Erntehelfer zusammenbringen soll. (https://devpost.com/software/plattform-fur-bauern-und-helfer) Die Web-Anwendung wurde mit einem React Frontend realisiert. Die Businesslogik im Backend wurde per REST Service angesprochen und war als Spring-Boot/Jhipster Anwendung umgesetzt. 

Aufgaben
- Initiierung des Teams und Koordination des Hackathons
- Konsolidierung der Ergebnisse, Erstellung Datenmodell (JDL)
- Implementieren der CI Pipeline für das Deployment in AWS/Kubernetes
- Einführung von Clean Architecture
- Implementierung der Fachlichkeiten im Backend

Technologien
Java, Maven, JHipster, JHipster Domain Language (JDL), JDL-Studio, Spring Boot, SpringToolSuite, openAPI 3 (contractFirst), REST, Docker, AWS, Gitlab CI, Jira, Confluence

### Dezember 2018 – Dezember 2019 - MicroService zur Optimierung von Werbeblöcken
Kunde: Medienunternehmen
Rolle: Software-Architekt, Software-Entwickler

Projektinhalt
Die Positionierung von Werbespots innerhalb von Werbeblöcken unter Berücksichtigung einer Vielzahl von Regeln wird automatisiert. Die automatischen Änderungen können leicht über eine grafische Oberfläche kontrolliert werden. Manuelle Änderungen bleiben weiterhin möglich, Regelverletzungen werden aber vom System gemeldet.
Der Service wird in einer Amazon Virtual Private Cloud (VPC) betrieben.
Aufgaben
- Initiales Aufsetzen des Projektes, mit Continuous Delivery und Anbindung an Monitoring Systeme in enger Kooperation mit den Ops.
- Konzeption und Entwicklung der automatisierten Optimierung der Spots im Scrum Team
- Einlesen von Werbespots über REST-Schnittstellen
- Abstimmung und Umsetzen der REST-Schnittstelle für die UI

Technologien: Java, Maven, Spring Boot, REST, Docker, AWS, Gitlab CI, Jira, Confluence, Prometheus, ElastikSearch, Kibana, Terraform, Clean Code

