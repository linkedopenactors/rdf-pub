
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

/**
 * Metadata about server software in use.
 * 
 */
public class Software {

    /**
     * The canonical name of this server software.
     * (Required)
     * 
     */
    private String name;
    /**
     * The version of this server software.
     * (Required)
     * 
     */
    private String version;
    /**
     * The url of the source code repository of this server software.
     * 
     */
    private String repository;
    /**
     * The url of the homepage of this server software.
     * 
     */
    private String homepage;

    /**
     * The canonical name of this server software.
     * (Required)
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * The canonical name of this server software.
     * (Required)
     * 
     */
    public void setName(String name) {
        this.name = name;
    }

    public Software withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * The version of this server software.
     * (Required)
     * 
     */
    public String getVersion() {
        return version;
    }

    /**
     * The version of this server software.
     * (Required)
     * 
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public Software withVersion(String version) {
        this.version = version;
        return this;
    }

    /**
     * The url of the source code repository of this server software.
     * 
     */
    public String getRepository() {
        return repository;
    }

    /**
     * The url of the source code repository of this server software.
     * 
     */
    public void setRepository(String repository) {
        this.repository = repository;
    }

    public Software withRepository(String repository) {
        this.repository = repository;
        return this;
    }

    /**
     * The url of the homepage of this server software.
     * 
     */
    public String getHomepage() {
        return homepage;
    }

    /**
     * The url of the homepage of this server software.
     * 
     */
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public Software withHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

}
