package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Optional;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.instance.InstanceService;
import org.linkedopenactors.rdfpub.app.object.CmdFindObjectById;
import org.linkedopenactors.rdfpub.app.object.QueryService;
import org.linkedopenactors.rdfpub.app.service.actor.KeyPairGenerator;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
/**
 * Rest Controller for finding {@link Actor}s.
 */
class ActorsProfile extends AbstractController {

	@Autowired
	private KeyPairGenerator keyPairGenerator;
	
	@Autowired
	private QueryService queryObjectService;

	@Autowired
	private org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings contentTypeHeaderMappings;
	
	@Autowired
	private InstanceService instanceService;
	
	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder; 
	
	@GetMapping(value = "/api/whoami")
	@ResponseBody	
	public ResponseEntity<String> whoAmIJsonLd(HttpServletRequest request, final JwtAuthenticationToken authentication) {
		String acceptHeader = request.getHeader(HttpHeaders.ACCEPT);
		RdfFormat format = contentTypeHeaderMappings.determineRdfFormat(acceptHeader);
		AuthenticationWrapper authenticationWrapper = new AuthenticationWrapper(authentication);
		Optional<Actor> queryActor = Optional.empty(); 
		if(authenticationWrapper.isAuthenticated()) {
			Actor actor = authenticatedActorHolder.getAuthenticatedActor().orElseThrow().getActor();
			queryActor = Optional.of(actor);
			// You are not able to determine the root out of the model, so we transfer it in the header.
			HttpHeaders headers = new HttpHeaders();
			queryActor.ifPresent(act->headers.add("root", act.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString()));	        
			String responseBody = actor.toString(format, true);
			return new ResponseEntity<>(responseBody, headers, HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping(value = "/{objectId}/publicKey")
	@ResponseBody	
    public String publicKey(@PathVariable String objectId, HttpServletRequest request, final JwtAuthenticationToken authentication) {
		log.info("-> publicKey("+objectId+")");
		
		Optional<org.linkedopenactors.rdfpub.domain.Actor> actorOpt = findActor(objectId);
		log.debug("actor found: " + actorOpt.isPresent());
		
		Optional<String> keyOpt = actorOpt
				.map(this::getKey);
		log.debug("publicKey found: " + keyOpt.isPresent());//	private void privatePublicChangeCheck(ActivityPubObject activityPubObject) {
		
		String result  = keyOpt
				.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
		log.info("<- publicKey("+objectId+"):\n" + result);
		return result;
	}

	private Optional<org.linkedopenactors.rdfpub.domain.Actor> findActor(String objectId) {
		
		RdfPubBlankNodeOrIRI actorUrl = instanceService.getInternalUrnExtendedWith(objectId);		
		CmdFindObjectById cmd = new CmdFindObjectById();
		cmd.setId(actorUrl);
		Optional<org.linkedopenactors.rdfpub.domain.Actor> actorOpt = queryObjectService.perform(cmd)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor);
		return actorOpt;
	}

	private String getKey(org.linkedopenactors.rdfpub.domain.Actor actor) {
		log.trace("-> getKey()");
		String identifier = actor.getSubject().asActor().orElseThrow().getIdentifier();
		String publicKeyBase64String = keyPairGenerator.getPublicKeyBase64Encoded(identifier);
		
		log.trace("<- getKey()");		
		return publicKeyBase64String;
	}
}
