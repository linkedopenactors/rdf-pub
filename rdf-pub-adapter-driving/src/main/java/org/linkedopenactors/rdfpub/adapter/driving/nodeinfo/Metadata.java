
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Free form key value pairs for software specific values. Clients should not rely on any specific key present.
 * 
 */
public class Metadata {

    private Map<String, Object> additionalProperties = new LinkedHashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Metadata withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
