package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.linkedopenactors.rdfpub.adapter.http.HttpHeaders;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
public abstract class AbstractController {

	@Autowired
	private PrefixMapper prefixMapper;

	@Autowired
	private AuthenticatedActorHolder authenticatedActorHolder; 
	
	@Autowired
	private org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings contentTypeHeaderMappings;

	/**
	 * Finds the current authenticated actor, if it's a request for it.
	 * @param request
	 * @param authenticationWrapper
	 * @return The current authenticated actor.
	 * @throws ResponseStatusException if the request is forbidden.
	 */
	protected org.linkedopenactors.rdfpub.domain.Actor assertRequestIsForCurrentUser(HttpServletRequest request, AuthenticationWrapper authenticationWrapper) {
		Actor currentActor = null;
		if(authenticationWrapper.isAuthenticated()) {
			currentActor = authenticatedActorHolder.getAuthenticatedActor().orElseThrow().getActor();
		}
		return assertRequestIsForCurrentUser(request, authenticationWrapper, currentActor);
	}

	protected RdfFormat extractRdfFormat(HttpServletRequest request) {
		String contentType = request.getHeader(HttpHeaders.CONTENT_TYPE);
		contentType = contentType == null ? "application/ld+json" : contentType;
		RdfFormat rdfFormat = contentTypeHeaderMappings.determineRdfFormat(contentType);
		return rdfFormat;
	}

	protected AuthenticatedActorHolder getAuthenticatedActorHolder() {
		return authenticatedActorHolder;
	}

	/**
	 * Finds the current authenticated actor, if it's a request for it.
	 * @param request
	 * @param authenticationWrapper
	 * @param actor
	 * @return The current authenticated actor.
	 * @throws ResponseStatusException if the request is forbidden.
	 */
	private org.linkedopenactors.rdfpub.domain.Actor assertRequestIsForCurrentUser(
			HttpServletRequest request, AuthenticationWrapper authenticationWrapper,
			org.linkedopenactors.rdfpub.domain.Actor actor) {
		String requestedUrl = getRequestedUrl(request);
		if (!authenticationWrapper.isAuthenticated()) {
			String msg = "Forbidden - not authenticated user is not allowed to request ("+request.getMethod()+") " + requestedUrl;
			log.warn(msg);
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, msg);
		}
		Optional<RdfPubBlankNodeOrIRI> actorIdOpt = Optional.ofNullable(actor)
			.map(org.linkedopenactors.rdfpub.domain.Actor::getSubject);
		
		actorIdOpt
			.map(Object::toString)
			.map(requestedUrl::startsWith)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN,
						"Forbidden - current user '" + actorIdOpt + "' is not allowed to request " + requestedUrl));
		
		return actor;
	}

	/**
	 * Extracts the Requested url out of the {@link HttpServletRequest}.
	 * @param request
	 * @return The path of the requested url 
	 */
	private String getRequestedUrl(HttpServletRequest request) {
		String urlString = prefixMapper.getExternalIriPrefix() + "/" + request.getRequestURI();
		URI uri; 
		try {
			uri = new URI(urlString);
			return uri.toString();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("unable to cerate URI from " + urlString, e);
		}		
	}
	
	protected String extractRemoteServer(HttpServletRequest request) {
		return "https://" + request.getRemoteHost() + ":" + request.getRemotePort();
	}
	
	protected Optional<UserAgent> extractUserAgent(HttpServletRequest req) {
		return new UserAgentExtractor().extractUserAgent(req);
	}
}
