package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.app.instance.InstanceService;
import org.linkedopenactors.rdfpub.app.object.AdminService;
import org.linkedopenactors.rdfpub.app.object.CmdFindObjectById;
import org.linkedopenactors.rdfpub.app.object.CmdReadActorGraphNames;
import org.linkedopenactors.rdfpub.app.object.CmdReadActors;
import org.linkedopenactors.rdfpub.app.object.QueryService;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.OrderedCollectionPage;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class AdminInterfaceController {
	
	@Autowired
	private InstanceService instanceService;
	
	@Autowired
	private QueryService queryService;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	@GetMapping(value = "/admin/actors/persons", headers = { "Accept=text/turtle" })
    public ResponseEntity<String> persons() {
		CmdReadActors cmd = new CmdReadActors();
		cmd.setPageSize(500);
		cmd.setStartIndex(0);
		OrderedCollectionPage orderedCollectionPage = queryService.perform(cmd);
		String body = orderedCollectionPage.toString(RdfFormat.TURTLE, true);    	
        return ResponseEntity.ok()
			.header(HttpHeaders.CONTENT_TYPE, "text/turtle; charset=utf-8")
			.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
			.body(body);
    }
    
    @GetMapping(value = "/admin/{actorId}/graphNames", headers = { "Accept=application/json" })
    public ResponseEntity<String> graphNames(@PathVariable(value = "actorId") String actorId, HttpServletRequest request) {
    	
    	RdfPubBlankNodeOrIRI actorSubject = instanceService.getInternalUrnExtendedWith(request.getRequestURI().replace("/graphNames", ""));
    	CmdFindObjectById cmd = new CmdFindObjectById();
    	cmd.setId(actorSubject);
    	cmd.setOwnerId(activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME));
    	Optional<ActivityPubObject> aoOpt = queryService.perform(cmd);
    	
    	Actor actor = aoOpt.map(ActivityPubObject::asConvertable )
    		.map(ActivityPubObjectConvertable::asActor)
    		.orElseThrow();
    	    	
		CmdReadActorGraphNames cmdReadActor = new CmdReadActorGraphNames();
		cmdReadActor.setActor(actor);		
		Set<String> graphNames = adminService.perform(cmdReadActor);
        
        String jsonString;
		try {
			jsonString = new ObjectMapper().writeValueAsString(graphNames);
		} catch (JsonProcessingException e) {
			log.error("ubable to read graphNames for " + actorSubject, e);
			return ResponseEntity.internalServerError().build();
		}
        
        return ResponseEntity.ok()
			.header(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8")
			.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
			.body(jsonString);
    }
}
