package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.List;

import org.linkedopenactors.rdfpub.domain.InstanceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NodeInfo21Provider {

	@Autowired
	private InstanceProperties instanceProperties; 
	
	public static void main(String[] args) {
		log.info(new NodeInfo21Provider().provide());
	}

	public String provide() {
		Nodeinfo21 ni = new Nodeinfo21();
		Software sw = new Software();
		sw.setHomepage("https://rdf-pub.org");
		sw.setName("rdf-pub");
		sw.setRepository("https://gitlab.com/linkedopenactors/rdf-pub");
		sw.setVersion(instanceProperties.getMavenVersion());
		ni.setSoftware(sw);
		ni.setProtocols(List.of(Protocol.ACTIVITYPUB));
		ni.setOpenRegistrations(false);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ni);
		} catch (JsonProcessingException e) {
			log.error("unable to generate nodeinfo", e);
			return "{\n"
					+ "	 \"error\" : \""+e.getMessage()+"\"\n"
					+ "}";
		}
	}
}