package org.linkedopenactors.rdfpub.adapter.driving;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.linkedopenactors.rdfpub.app.UserAgent;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAgentExtractor {
	
	public Optional<UserAgent> extractUserAgent(HttpServletRequest req) {
		Optional<UserAgent> uaOpt = Optional.empty();
		try {			
			Optional<String> uaStringOpt = Optional.ofNullable(req.getHeader("User-Agent"));
			if(uaStringOpt.isPresent()) {
				String uaStringLowerCase = uaStringOpt.get().toLowerCase();
				if(uaStringLowerCase.contains("mobilizon")) {
					uaOpt = Optional.of(specialMobilizon(uaStringLowerCase));
				} else {
					String product = uaStringOpt.map(this::extractedProduct).orElse("");
					String productVersion = uaStringOpt.map(this::extractProductVersion).orElse("");
					String comment = uaStringOpt.map(this::extractedComment).orElse("");
					uaOpt = Optional.of(new UserAgent(product, productVersion, comment));
				}
			}
		} catch (Exception e) {
			log.error("not parsable: " + req.getHeader("User-Agent"), e);
		}
		return uaOpt ;
	}

	private UserAgent specialMobilizon(String uaString) {
		String d = uaString.substring(uaString.indexOf("mobilizon"));
		List<String> tokens = Collections.list(new StringTokenizer(d, " ")).stream()
			      .map(token -> (String) token)
			      .collect(Collectors.toList());
		return new UserAgent(tokens.get(0), tokens.get(1), "");
	}

	private String extractedProduct(String ua) {
		return ua.substring(0, ua.indexOf("/"));
	}
	
	private String extractedComment(String ua) {
		String rest = ua.substring(ua.indexOf("/"));
		if(rest.contains(" ")) {
			return rest.substring(ua.indexOf(" "));
		}
		return null;
	}

	private String extractProductVersion(String ua) {
		String rest = ua.substring(ua.indexOf("/")+1);
		if(rest.contains(" ")) {
			return rest.substring(0, ua.indexOf(" "));
		}
		return rest;
	}
}
