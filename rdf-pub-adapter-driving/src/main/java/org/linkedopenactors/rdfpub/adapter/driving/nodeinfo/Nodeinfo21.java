
package org.linkedopenactors.rdfpub.adapter.driving.nodeinfo;

import java.util.List;


/**
 * NodeInfo schema version 2.1.
 * 
 */
public class Nodeinfo21 {

    /**
     * Metadata about server software in use.
     * (Required)
     * 
     */
    private Software software;
    /**
     * The protocols supported on this server.
     * (Required)
     * 
     */
    private List<Protocol> protocols;
    /**
     * The third party sites this server can connect to via their application API.
     * (Required)
     * 
     */
    private Services services;
    /**
     * Whether this server allows open self-registration.
     * (Required)
     * 
     */
    private Boolean openRegistrations;
    /**
     * Usage statistics for this server.
     * (Required)
     * 
     */
    private Usage usage;
    /**
     * Free form key value pairs for software specific values. Clients should not rely on any specific key present.
     * (Required)
     * 
     */
    private Metadata metadata;

    /**
     * The schema version, must be 2.1.
     * (Required)
     * 
     */
    public String getVersion() {
        return "2.1";
    }

    /**
     * Metadata about server software in use.
     * (Required)
     * 
     */
    public Software getSoftware() {
        return software;
    }

    /**
     * Metadata about server software in use.
     * (Required)
     * 
     */
    public void setSoftware(Software software) {
        this.software = software;
    }

    public Nodeinfo21 withSoftware(Software software) {
        this.software = software;
        return this;
    }

    /**
     * The protocols supported on this server.
     * (Required)
     * 
     */
    public List<Protocol> getProtocols() {
        return protocols;
    }

    /**
     * The protocols supported on this server.
     * (Required)
     * 
     */
    public void setProtocols(List<Protocol> protocols) {
        this.protocols = protocols;
    }

    public Nodeinfo21 withProtocols(List<Protocol> protocols) {
        this.protocols = protocols;
        return this;
    }

    /**
     * The third party sites this server can connect to via their application API.
     * (Required)
     * 
     */
    public Services getServices() {
        return services;
    }

    /**
     * The third party sites this server can connect to via their application API.
     * (Required)
     * 
     */
    public void setServices(Services services) {
        this.services = services;
    }

    public Nodeinfo21 withServices(Services services) {
        this.services = services;
        return this;
    }

    /**
     * Whether this server allows open self-registration.
     * (Required)
     * 
     */
    public Boolean getOpenRegistrations() {
        return openRegistrations;
    }

    /**
     * Whether this server allows open self-registration.
     * (Required)
     * 
     */
    public void setOpenRegistrations(Boolean openRegistrations) {
        this.openRegistrations = openRegistrations;
    }

    public Nodeinfo21 withOpenRegistrations(Boolean openRegistrations) {
        this.openRegistrations = openRegistrations;
        return this;
    }

    /**
     * Usage statistics for this server.
     * (Required)
     * 
     */
    public Usage getUsage() {
        return usage;
    }

    /**
     * Usage statistics for this server.
     * (Required)
     * 
     */
    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public Nodeinfo21 withUsage(Usage usage) {
        this.usage = usage;
        return this;
    }

    /**
     * Free form key value pairs for software specific values. Clients should not rely on any specific key present.
     * (Required)
     * 
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * Free form key value pairs for software specific values. Clients should not rely on any specific key present.
     * (Required)
     * 
     */
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Nodeinfo21 withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }
}
