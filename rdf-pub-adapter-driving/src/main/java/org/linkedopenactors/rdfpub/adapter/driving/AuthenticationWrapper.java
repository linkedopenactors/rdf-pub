package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.URI;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.auth.Authentication;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

/**
 * 
 */
class AuthenticationWrapper implements Authentication {

	private final JwtAuthenticationToken authentication;

	/**
	 * @param authentication
	 */
	public AuthenticationWrapper(JwtAuthenticationToken authentication) {
		this.authentication = authentication;		
	}
	
	/**
	 * @return PreferredUsername
	 */
	@Override
	public String getIssuerPreferredUsername() {
		return Optional.ofNullable(authentication)
				.map(a->a.getToken().getClaim("preferred_username"))
				.map(String.class::cast)
				.orElse(null);
	}

	/**
	 * @return UserId
	 */
	@Override
	public String getIssuerUserId() {
		return Optional.ofNullable(authentication)
				.map(a->a.getToken().getSubject())
				.orElse(null);
	}

	@Override
	public URI getIssuer() {
		return Optional.ofNullable(authentication)
				.map(a->a.getToken().getClaim("iss"))
				.map(String.class::cast)
				.map(URI::create)
				.orElse(null);
	}

	/**
	 * @return True, if a user is authenticated.
	 */
	@Override
	public boolean isAuthenticated() {
		return Optional.ofNullable(authentication)
				.map(JwtAuthenticationToken::isAuthenticated)
				.orElse(false);
	}
}
