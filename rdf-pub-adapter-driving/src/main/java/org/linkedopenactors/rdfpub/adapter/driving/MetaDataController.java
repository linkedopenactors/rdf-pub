package org.linkedopenactors.rdfpub.adapter.driving;

import org.linkedopenactors.rdfpub.adapter.driving.nodeinfo.NodeInfo21Provider;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class MetaDataController {
	
	@Autowired
	private PrefixMapper prefixMapper;
	
	
	@Autowired
	private NodeInfo21Provider info21Provider; 
	
	@GetMapping(value = "/.well-known/nodeinfo")
    public String wellKnownNodeInfo() {
		String prefix = prefixMapper.getExternalIriPrefix();
		return "{\n"
				+ "    \"links\": [\n"
				+ "        {\n"
				+ "            \"rel\": \"http://nodeinfo.diaspora.software/ns/schema/2.1\",\n"
				+ "            \"href\": \"" + prefix + "/nodeinfo/2.1\"\n"
				+ "        }\n"
				+ "    ]\n"
				+ " }";
	}
	
	@GetMapping(value = "/nodeinfo/2.1", produces = "application/json; profile=\"http://nodeinfo.diaspora.software/ns/schema/2.1#\"")
    public String nodeInfo() {
		return info21Provider. provide();
	}

}
