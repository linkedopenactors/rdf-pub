package org.linkedopenactors.rdfpub.adapter.driving;

import static org.springframework.http.HttpHeaders.ACCEPT;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import org.linkedopenactors.rdfpub.app.object.QueryService;
import org.linkedopenactors.rdfpub.app.object.CmdSparqlCollection;
import org.linkedopenactors.rdfpub.app.object.CmdSparqlInbox;
import org.linkedopenactors.rdfpub.app.object.CmdSparqlOutbox;
import org.linkedopenactors.rdfpub.app.object.CmdSparqlPublic;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
class QueryObject extends AbstractController {
	
	public static final String QUERY = "query";
	
	private final QueryService queryObjectService;

	public QueryObject(QueryService queryObjectService) {
		this.queryObjectService = queryObjectService;
	}
	
//	@PostMapping(value = "/{userId}/{collection}/sparql")
//	public ResponseEntity<String> executeSparqlPost(@org.springframework.web.bind.annotation.RequestBody String query, @PathVariable String userId, @PathVariable String collection, HttpServletRequest request, final JwtAuthenticationToken authentication) {
//		org.linkedopenactors.rdfpub.domain.Actor autenticatedActor = assertRequestIsForCurrentUser(request, new AuthenticationWrapper(authentication));
//		return queryCollection(autenticatedActor, collection, query, request.getParameter(ACCEPT));
//	}
//
//	@GetMapping(value = "/{userId}/{collection}/sparql")
//	public ResponseEntity<String> executeSparqlGet(@PathVariable String userId, @PathVariable String collection, HttpServletRequest request, final JwtAuthenticationToken authentication) {
//		org.linkedopenactors.rdfpub.domain.Actor autenticatedActor = assertRequestIsForCurrentUser(request, new AuthenticationWrapper(authentication));
//		return queryCollection(autenticatedActor, collection, request.getParameter(QUERY), request.getParameter(ACCEPT));
//	}

//	private ResponseEntity<String> queryCollection(org.linkedopenactors.rdfpub.domain.Actor autenticatedActor, String collection, String query,  String acceptHeader) {
//		ResponseEntity<String> result;
//		try {
//			String response = queryObjectService.perform(getCommand(collection, query, acceptHeader, autenticatedActor)).toString();
//			result = new ResponseEntity<String>(response, getApplicationJsonHeader(), HttpStatus.OK);
//		} catch (IOException e) {
//			long errorId = UUID.randomUUID().getMostSignificantBits();
//			log.error("IOException (errorId_"+errorId+")", e);
//			result = ResponseEntity.internalServerError().body("errorId_"+errorId);
//		}
//		return result;
//	}
	
//	@PostMapping(value = "/asPublic/sparql")
//	public ResponseEntity<String> executePublicSparqlPost(@org.springframework.web.bind.annotation.RequestBody String query, HttpServletRequest request) {
//		return publicSparql(query, request.getHeader(HttpHeaders.ACCEPT));
//	}
//
//	@GetMapping(value = "/asPublic/sparql")
//	public ResponseEntity<String> executePublicSparqlGet(@org.springframework.web.bind.annotation.RequestBody String body, HttpServletRequest request) {		
//		return publicSparql(request.getHeader(QUERY), request.getHeader(HttpHeaders.ACCEPT));
//	}

//	private CmdSparqlCollection getCommand(String collection, String query, String acceptHeader, org.linkedopenactors.rdfpub.domain.Actor authenticatedActor) {
//		switch (collection) {
//		case "inbox": 
//			return new CmdSparqlInbox(query, acceptHeader, authenticatedActor);
//		case "outbox": 
//			return new CmdSparqlOutbox(query, acceptHeader, authenticatedActor);
//		default:
//			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No sparql endpoint for collection: " + collection);
//		}
//	}

	private HttpHeaders getApplicationJsonHeader() {
		final HttpHeaders httpHeaders= new HttpHeaders();
	    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		return httpHeaders;
	}

//	private ResponseEntity<String> publicSparql(String query, String acceptHeader) {
//		CmdSparqlPublic cmd = new CmdSparqlPublic(query, acceptHeader);
//		try (OutputStream sparqlResult = queryObjectService.perform(cmd)) {
//			final HttpHeaders httpHeaders= new HttpHeaders();
//			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//			return new ResponseEntity<String>(sparqlResult.toString(), httpHeaders, HttpStatus.OK);
//		} catch (IOException e) {
//			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error executing Sparql", e);
//		}
//	}
}
