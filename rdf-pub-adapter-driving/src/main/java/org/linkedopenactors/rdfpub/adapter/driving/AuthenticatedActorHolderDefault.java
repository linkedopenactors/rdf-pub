package org.linkedopenactors.rdfpub.adapter.driving;

import java.net.URI;
import java.util.Optional;

import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActor;
import org.linkedopenactors.rdfpub.app.auth.AuthenticatedActorHolder;
import org.linkedopenactors.rdfpub.app.auth.Authentication;
import org.linkedopenactors.rdfpub.app.service.actor.UcQueryActor;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
class AuthenticatedActorHolderDefault implements AuthenticatedActorHolder {

	private UcQueryActor ucQueryActor;
	
	private ActorsStoreRepository actorsStoreRepository;
		
	private Authentication authentication;
	
	private AuthenticatedActor authenticatedActor; 
	
	public AuthenticatedActorHolderDefault(UcQueryActor ucQueryActor, ActorsStoreRepository actorsStoreRepository) {
		this.ucQueryActor = ucQueryActor;
		this.actorsStoreRepository = actorsStoreRepository;
	}
	
	@Override
	public Optional<AuthenticatedActor> getAuthenticatedActor() {
		if(getAuthentication().isAuthenticated()) {
			if(authenticatedActor == null) {
				authenticatedActor = new AuthenticatedActorDefault(ucQueryActor, actorsStoreRepository, this);
			}
			return Optional.ofNullable(authenticatedActor);
		} else {
			return Optional.empty();
		}
	}

	public Authentication getAuthentication() {
		if(authentication==null) {
			authentication = new AuthenticationWrapper();
		}
		return authentication;
	}

	private class AuthenticationWrapper implements org.linkedopenactors.rdfpub.app.auth.Authentication {

		@Override
		public String getIssuerPreferredUsername() {
			return getJwtAuthentication()
					.map(a->a.getToken().getClaim("preferred_username"))
					.map(String.class::cast)
					.orElse(null);
		}

		@Override
		public String getIssuerUserId() {
			return getJwtAuthentication()
					.map(a->a.getToken().getSubject())
					.orElse(null);
		}

		@Override
		public boolean isAuthenticated() {
			return getJwtAuthentication()
							.map(JwtAuthenticationToken::isAuthenticated)
							.orElse(false);		
		}

		@Override
		public URI getIssuer() {
			return getJwtAuthentication()
					.map(a->a.getToken().getClaim("iss"))
					.map(String.class::cast)
					.map(URI::create)
					.orElse(null);
		}

		private Optional<JwtAuthenticationToken> getJwtAuthentication() {
			org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth instanceof JwtAuthenticationToken) {
				return Optional.ofNullable((JwtAuthenticationToken)auth);
			} else {
				return Optional.empty();
			}
		}
	}
}
