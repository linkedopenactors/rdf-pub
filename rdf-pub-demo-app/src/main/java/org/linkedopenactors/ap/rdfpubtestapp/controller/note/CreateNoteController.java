package org.linkedopenactors.ap.rdfpubtestapp.controller.note;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.linkedopenactors.ap.rdfpubtestapp.controller.BaseController;
import org.linkedopenactors.ap.rdfpubtestapp.controller.CollectionController;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Link;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdResource;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;

/**
 * 
 */
@Controller
public class CreateNoteController extends BaseController {
		
	@Autowired
	private CollectionController collectionController;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 * @param model
	 *  @return The name of the fragment to show
	 */
	@GetMapping("/notes/create")
	public String createUserForm(Model model) {
		model.addAttribute("note", new CreateNoteFormData());
		model.addAttribute("title", "Create Note");
		return "fragments/notes/edit";
	}

	/**
	 * 
	 * @param formData
	 * @param bindingResult
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @return The name of the fragment to show
	 */
	@PostMapping("/notes/create")
	public String doCreateUser(@Valid @ModelAttribute("note") CreateNoteFormData formData, BindingResult bindingResult,
			Model model, Authentication authentication, HttpSession httpSession) {
		if (bindingResult.hasErrors()) {	
			return "fragments/notes/edit";
		}
		Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));
		
		ActivityPubObject activityPubObject = toActivityPubObject(formData, currentActor);
		getRdfPubClient().sendToOutbox(currentActor, activityPubObject);
		return collectionController.collectionOutbox(model, authentication, httpSession);
	}

	private List<String> getAsList(String items) {
		return
				Collections.list(new StringTokenizer(items, ","))
				.stream()
				.map(Object::toString)
				.map(String::trim)
				.collect(Collectors.toList());
	}
	
	private ActivityPubObject toActivityPubObject(CreateNoteFormData formData, Actor currentActor) {
		ActivityPubObject ao = getRdfPubClient().createNewEmpty();
		
		ao.setType(Set.of(vocabContainer.vocAs().Note()));
		
		String to = formData.getTo();
		if(StringUtils.hasText(to)) {
			ao.setTo(toIriSet(to));
		}
		
		String cc = formData.getCc();
		if(StringUtils.hasText(cc)) {
			ao.setCc(toIriSet(cc));
		}
		
		String bto = formData.getBto();
		if(StringUtils.hasText(bto)) {
			ao.setBto(toIriSet(bto));
		}

		String bcc = formData.getBcc();
		if(StringUtils.hasText(bcc)) {
			ao.setBcc(toIriSet(bcc));
		}

		if(StringUtils.hasText(formData.getName())) {
			ao.setName(formData.getName());
		}

		if(StringUtils.hasText(formData.getSummary())) {
			ao.setSummary(formData.getSummary());
		}

		if(StringUtils.hasText(formData.getContent())) {
			ao.setContent(formData.getContent());
		}
		
		addMentions(currentActor, ao);
			
		return ao;
	}

	private Set<RdfPubBlankNodeOrIRI> toIriSet(String items) {
		return getAsList(items).stream()
			.map(item->getRdfPubClient().toIri(item))
			.map(activityPubObjectIdBuilder::createFromUrl)
			.collect(Collectors.toSet());
	}

	private void addMentions(Actor outboxOwner, ActivityPubObject note) {
		note.addTags(addMentions(outboxOwner, note.getTo()));
		note.addTags(addMentions(outboxOwner, note.getCc()));
		note.addTags(addMentions(outboxOwner, note.getAudience()));
	}

	private Set<Link> addMentions(Actor outboxOwner, Set<RdfPubBlankNodeOrIRI> mentions) {
		return mentions.stream()
				.map(r->createMention(outboxOwner, r))
				.collect(Collectors.toSet());
	}
	
	private Link createMention(Actor outboxOwner, RdfPubBlankNodeOrIRI mentioned) {
		RdfPubObjectIdResource itemSubject = activityPubObjectIdBuilder.createResource(outboxOwner.getSubject());
		Link link = activityPubObjectFactory.create(itemSubject).asConvertable().asLink();
//		getPreferredUsername(mentioned).ifPresent(link::setName);
		link.setHref(URI.create(mentioned.getIRIString()));
		link.setType(Set.of(vocabContainer.vocAs().Mention()));
		return link;
	}

//	private Optional<String> getPreferredUsername(RdfPubIRI actorId) {
//		Optional<ActivityPubObject> externalActorOpt = externalObjectRepository.readWithSignature(actorId.getIRIString(), publicActorStore.getInstanceActor());
//		if(externalActorOpt.isEmpty()) {
//			log.warn("unable to read actor: " + actorId.getIRIString());
//		}
//		return externalActorOpt
//			.map(ActivityPubObject::asConvertable)
//			.map(ActivityPubObjectConvertable::asActor)
//			.flatMap(Actor::getPreferredUsername);
//	}
}

