package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Service
@Slf4j
public class PredicateDescritionRepository {

	private Map<String, PredicateDescriptor> descriptions = new HashMap<>();

	/**
	 * 
	 */
	public PredicateDescritionRepository() {
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#published", "published", "The date and time at which the object was published"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#name", "name", "A simple, human-readable, plain-text name for the object. HTML markup MUST NOT be included. The name MAY be expressed using multiple language-tagged values."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#summary", "summary", "A natural language summarization of the object encoded as HTML. Multiple language tagged summaries MAY be provided."));
		add(new PredicateDescriptor("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "type", "Indicates the classes to which this instance belongs."));
		add(new PredicateDescriptor("http://www.w3.org/2002/07/owl#sameAs", "sameAs", "")); 
		add(new PredicateDescriptor("http://www.w3.org/ns/ldp#inbox", "inbox", "Links a resource to a container where notifications for the resource can be created and discovered."));
		add(new PredicateDescriptor("http://www.w3.org/ns/prov#wasAttributedTo", "wasAttributedTo", "Attribution is the ascribing of an entity to an agent."));
		add(new PredicateDescriptor("http://www.w3.org/ns/prov#wasGeneratedBy" , "wasGeneratedBy", "Generation is the completion of production of a new entity by an activity. This entity did not exist before generation and becomes available for usage after this generation."));
		add(new PredicateDescriptor("https://schema.rdf-pub.org/oauth2IssuerUserId" , "oauth2IssuerUserId", "The unique ID of an actor that identifies the actor in the oauth2 server."));
		add(new PredicateDescriptor("https://schema.rdf-pub.org/oauth2Issuer", "oauth2Issuer", "The oauth2 issuer host e.g. 'login.m4h.network'."));		
		add(new PredicateDescriptor("https://w3id.org/security#publicKeyPem" , "publicKeyPem", "A public key PEM property is used to specify the PEM-encoded version of the public key. This encoding is compatible with almost every Secure Sockets Layer library implementation and typically plugs directly into functions intializing public keys."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#endpoints" , "endpoints", "A json object which maps additional (typically server/domain-wide) endpoints which may be useful either for this actor or someone referencing this actor. This mapping may be nested inside the actor document as the value or may be a link to a JSON-LD document with these properties."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#oauthAuthorizationEndpoint", "oauthAuthorizationEndpoint", "If OAuth 2.0 bearer tokens [RFC6749] [RFC6750] are being used for authenticating client to server interactions, this endpoint specifies a URI at which a browser-authenticated user may obtain a new authorization grant."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#oauthTokenEndpoint", "oauthTokenEndpoint", "If OAuth 2.0 bearer tokens [RFC6749] [RFC6750] are being used for authenticating client to server interactions, this endpoint specifies a URI at which a client may acquire an access token."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#outbox" , "outbox", "The outbox stream contains activities the user has published, subject to the ability of the requestor to retrieve the activity."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#preferredUsername", "preferredUsername", "A short username which may be used to refer to the actor, with no uniqueness guarantees."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#actor", "actor", "Describes one or more entities that either performed or are expected to perform the activity. Any single activity can have multiple actors. The actor MAY be specified using an indirect Link."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#object", "object", "Describes an object of any kind. The Object type serves as the base type for most of the other kinds of objects defined in the Activity Vocabulary, including other Core types such as Activity, IntransitiveActivity, Collection and OrderedCollection."));
		add(new PredicateDescriptor("http://www.w3.org/ns/prov#wasAssociatedWith", "wasAssociatedWith", "An activity association is an assignment of responsibility to an agent for an activity, indicating that the agent had a role in the activity. It further allows for a plan to be specified, which is the plan intended by the agent to achieve some goals in the context of this activity."));
		add(new PredicateDescriptor("http://www.w3.org/ns/prov#wasRevisionOf", "wasRevisionOf", "A revision is a derivation for which the resulting entity is a revised version of some original."));
		add(new PredicateDescriptor("https://purl.archive.org/socialweb/pending#pendingFollowers", "pendingFollowers", "The pendingFollowers collection can be used to review incoming Follow activities to Accept or Reject them."));
		add(new PredicateDescriptor("https://purl.archive.org/socialweb/pending#pendingFollowing", "pendingFollowing", "The pendingFollowing collection can be used to review outgoing Follow activities to Undo them."));
		add(new PredicateDescriptor("https://schema.rdf-pub.org/activityPubObjectVersion", "activityPubObjectVersion", "The version of an object. Assuming an actor has version 5, the FEP-4ccd is now implemented in rdf-pub, which means that an actor also receives a pendingFollowers & pendingFollowing collection. Then the actor must be updated to version 6, which is extended by the 2 collections. Actor is just a sample, applies to all types of objects."));		
		add(new PredicateDescriptor("https://w3id.org/security#publicKey", "publicKey", "Mastodon specific thing?! It's somehow deprecated. Nowhere really specified."));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#updated", "updated", "The date and time at which the object was updated."));
		
		add(new PredicateDescriptor("http://ostatus.org#atomUri", "atomUri", "todo"));
		add(new PredicateDescriptor("http://ostatus.org#conversation", "conversation", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#attributedTo", "attributedTo", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#content", "content", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#first", "first", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#href", "href", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#likes", "likes", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#next", "next", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#partOf", "partOf", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#replies", "replies", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#sensitive", "sensitive", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#shares", "shares", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#tag", "tag", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#to", "to", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#totalItems", "totalItems", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#url", "url", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#followers", "followers", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#alsoKnownAs", "alsoKnownAs", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#following", "following", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#icon", "icon", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#image", "image", "todo"));
		add(new PredicateDescriptor("https://www.w3.org/ns/activitystreams#manuallyApprovesFollowers", "manuallyApprovesFollowers", "todo"));
	}
	
	private void add(PredicateDescriptor predicateDescriptor) {
		descriptions.put(predicateDescriptor.getId(), predicateDescriptor);
	}
	
	/**
	 * 
	 * @param predicate
	 * @return {@link PredicateDescriptor} for the passed predicate.
	 */
	public Optional<PredicateDescriptor> find(String predicate) {
		Optional<PredicateDescriptor> description = Optional.ofNullable(descriptions.get(predicate));
		if(description.isEmpty()) {
			log.warn("no mapping for: " + predicate);
		}
		return description;
	}
}
