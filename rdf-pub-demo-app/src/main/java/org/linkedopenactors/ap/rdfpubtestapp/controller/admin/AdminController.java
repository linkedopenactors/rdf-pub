package org.linkedopenactors.ap.rdfpubtestapp.controller.admin;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.linkedopenactors.ap.rdfpubtestapp.controller.BaseController;
import org.linkedopenactors.ap.rdfpubtestapp.model.Converter;
import org.linkedopenactors.ap.rdfpubtestapp.model.GenericUiActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Controller
public class AdminController extends BaseController {

	@Autowired
	private Converter converter; 

	/**
	 * 
	 * @param model
	 * @return The name of the fragment to show
	 */
	@GetMapping("/admin/query/actors/persons")	
    public String queryPersons(Model model) {
		List<Actor> actors;
		try {
			actors = getRdfPubAdminClient().discoverActors();
	    	List<GenericUiActivityPubObject> uis = actors.stream()
	    			.map(converter::convertUltimate)
	    			.collect(Collectors.toList());
	    	
	    	model.addAttribute("genericUiActivityPubObjects", uis);
		} catch (WebClientResponseException e) {
			log.error("error calling backend: " + e.getMessage());
			String message = "Come on, you know that. An unexpected error has occurred! Sorry for that.";
			if(e.getStatusCode().isSameCodeAs(org.springframework.http.HttpStatus.FORBIDDEN)) {
				message = "You are not allowed to see this content.";
			}
			model.addAttribute("isError", true);
			model.addAttribute("errorMessage", message);
		}
		return "fragments/showUltimateObjects";
    }

	/**
	 * 
	 * @param model
	 * @param actorId
	 * @return The name of the fragment to show
	 */
	@GetMapping("/admin/{actorId}/graphnames")	
    public String getGraphNames(Model model, @PathVariable(value = "actorId") String actorId) {
		Set<String> graphNames = getRdfPubAdminClient().getGraphNames(actorId);
		model.addAttribute("graphNames", graphNames);
		return "fragments/graphnames";
	}
}

