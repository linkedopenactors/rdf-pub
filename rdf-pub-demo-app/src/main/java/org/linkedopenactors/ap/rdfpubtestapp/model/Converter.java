package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.util.Optional;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Component
@Slf4j
public class Converter {

//	private static final PredicateDescriptor nullObjectPredicateDescriptor = new PredicateDescriptor(null, null, null)
	
	@Autowired
	private PredicateDescritionRepository predicateDescritionRepository;
	
	@Autowired
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 * @param activityPubObject
	 * @return passed data as {@link GenericUiActivityPubObjectDefault}
	 */
	public GenericUiActivityPubObject convertUltimate(ActivityPubObject activityPubObjectParam) {
		
		ActivityPubObject activityPubObject = activityPubObjectParam;//.asConvertable().isolated();
		
		GenericUiActivityPubObject genericUiActivityPubObject = new GenericUiActivityPubObjectWrapped(
				vocabContainer, predicateDescritionRepository, activityPubObject);
		return genericUiActivityPubObject;
	}
	
	private Property createProperty(Triple triple) {
		
//		log.debug("subject: " + triple.getSubject().getClass().getSimpleName());
//		if(triple.getSubject() instanceof BlankNode) {
//			log.debug("blanknode");
//		}
		
		Optional<PredicateDescriptor> desc = predicateDescritionRepository.find(triple.getPredicate().getIRIString());
		String predicate = desc.map(PredicateDescriptor::getId).orElse(triple.getPredicate().getIRIString());
		
		String humanReadableName = desc.map(PredicateDescriptor::getName).orElse(triple.getPredicate().getIRIString());
		String description = desc.map(PredicateDescriptor::getDescription).orElse("not available!");
//		log.debug("triple: " + triple.getPredicate().getIRIString() + " -> " + triple.getObject().getClass().getName() + "("+triple.getObject().toString()+")");
		RDFTerm value = triple.getObject();
		return new Property(vocabContainer, predicate, humanReadableName, description, value, triple.getObject() instanceof IRI, triple.getObject() instanceof BlankNode);
	}
}
