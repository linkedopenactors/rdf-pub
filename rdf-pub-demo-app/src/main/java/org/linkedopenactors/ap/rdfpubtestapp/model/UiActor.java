package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

public class UiActor {
	
	private Actor actor;

	public UiActor(Actor actor) {
		this.actor = actor;		
	}	
	
	public Actor getActor() {
		return actor;
	}
	
	public Optional<String> getSummary() {
		Optional<String> summary = actor.getSummary();
		System.out.println(summary.orElse("n.a."));
		return summary;
	}
	
	public Optional<String> getIconUrl() {
		Optional<String> fi = actor.getFirstIcon()
				.flatMap(this::getFirstUrl)
				.map(iri->iri.getIRIString());
		return fi;
	}
	
	public String getSubjectEncoded() {
		return URLEncoder.encode(actor.getSubject().getIRIString(), StandardCharsets.UTF_8);
	}
	
	private Optional<RdfPubBlankNodeOrIRI> getFirstUrl(ActivityPubObject ao) {
		return ao.getUrls().stream().findFirst();
	}

}
