package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.List;

import org.linkedopenactors.ap.rdfpubtestapp.model.UiTriple;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 */
@Data
@AllArgsConstructor
public class PropertyCollection {
	private String subject;
	private String name;
	private List<UiTriple> properties;
}
