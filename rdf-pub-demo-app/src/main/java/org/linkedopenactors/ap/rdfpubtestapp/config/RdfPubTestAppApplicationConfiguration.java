package org.linkedopenactors.ap.rdfpubtestapp.config;

import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Configuration
@Slf4j
public class RdfPubTestAppApplicationConfiguration {

	/**
	 * 
	 * @return ITemplateResolver
	 */
	@Bean
	public ITemplateResolver svgTemplateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setPrefix("classpath:/templates/svg/");
		resolver.setSuffix(".svg");
		resolver.setTemplateMode("XML");
		return resolver;
	}

	/**
	 * 
	 * @return {@link RDF4J}
	 */
	@Bean
	public org.apache.commons.rdfrdf4j.RDF4J rdf() {
		return new org.apache.commons.rdfrdf4j.RDF4J();
	}

	@Bean
	WebClient webClient(ClientRegistrationRepository clientRegistrationRepository,
			OAuth2AuthorizedClientService authorizedClientService,
			@Value("${app.artifactId}") String artifactId,
			@Value("${app.version}") String version) {
		var oauth = new ServletOAuth2AuthorizedClientExchangeFilterFunction(
				new AuthorizedClientServiceOAuth2AuthorizedClientManager(clientRegistrationRepository,
						authorizedClientService));
		oauth.setDefaultClientRegistrationId("loa-application");
		return WebClient.builder()
				.defaultHeader(HttpHeaders.USER_AGENT, artifactId + "/" + version)
//				.apply(oauth.oauth2Configuration())
				.filter(oauth)
				
				.codecs(codecs -> codecs
			            .defaultCodecs()
			            .maxInMemorySize(500 * 1024))
				.build();
	}
	
	@Bean
	PrefixMapper prefixMapper(@Value("${rdf-pub-server.url}") String rdfPubServerUrl) {
		return new PrefixMapper() {

			@Override
			public String getInternaleIriPrefix() {
				return "urn:rdfpub:";
			}

			@Override
			public String getExternalIriPrefix() {
				return rdfPubServerUrl;
			}
		};
	}
}
