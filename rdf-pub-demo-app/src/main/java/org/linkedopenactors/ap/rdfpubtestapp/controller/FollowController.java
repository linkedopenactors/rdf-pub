package org.linkedopenactors.ap.rdfpubtestapp.controller;

import java.util.Set;

import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
@Controller
public class FollowController extends BaseController {

	@Autowired
	private ReadObjectController readObjectController;
	
	@Autowired
	private CollectionController collectionController;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 * @param model
	 * @param authentication
	 * @param httpSession
	 * @param subject
	 * @return The name of the fragment to show
	 */
    @GetMapping("/follow")
    public String readObject(Model model, Authentication authentication, HttpSession httpSession, @RequestParam(name = "subject") String subject) {
    	log.debug("follow: " + subject);
    	
		Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));

		ActivityPubObject ao = getRdfPubClient().createNewEmpty();
		Activity activity = ao.asConvertable().asActivity();
		Set<RdfPubBlankNodeOrIRI> toBeFollowedAsSet = Set.of(activityPubObjectIdBuilder.createFromUrl(subject));
		activity.setTo(toBeFollowedAsSet);
		activity.addType(vocabContainer.vocAs().Follow());
		activity.setObject(toBeFollowedAsSet);
		
    	getRdfPubClient().sendToOutbox(currentActor, activity);
    	return readObjectController.readObject(model, subject);
//    	return "fragments/empty";
//    	return "fragments/showUltimateObjects"
    }

    
    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @param subject
     * @return The name of the fragment to show
     */
    @GetMapping("/undoFollowing")
	public String undoFollowing(Model model, Authentication authentication, HttpSession httpSession,
			@RequestParam(name = "subject") String subject, @RequestParam(name = "followed") String followed) {
    	log.debug("rejectFollow: " + subject);
    	
    	Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));
    	
//    	Activity activity = createRejectActivity(subject);
		ActivityPubObject ao = getRdfPubClient().createNewEmpty();
		Activity activity = ao.asConvertable().asActivity();
		activity.addType(vocabContainer.vocAs().Undo());    		
		RdfPubBlankNodeOrIRI activityId = activityPubObjectIdBuilder.createFromUrl(subject);
		activity.setObject(Set.of(activityId));

    	getRdfPubClient().sendToOutbox(currentActor, activity);
    	return collectionController.collectionPendingFollowing(model, authentication, httpSession);
    }

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @param subject
     * @return The name of the fragment to show
     */
    @GetMapping("/acceptFollow")
	public String acceptFollow(Model model, Authentication authentication, HttpSession httpSession,
			@RequestParam(name = "follower") String follower 
			) {
    	log.debug("acceptFollow: " + follower);
    	
		Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));

		
    	Set<Activity> pendingFollower = getRdfPubClient().readActivityCollection(currentActor.getPendingFollowers().asCollection().orElseThrow());
    	
    	ActivityPubObject followActivityToAccept = findForFollower(pendingFollower, follower).asConvertable().isolated();
    	
		
		RdfPubObjectIdActivity activityIri = activityPubObjectIdBuilder.createActivity(currentActor.getSubject());
		Activity acceptActivity = activityPubObjectFactory.create(activityIri).asConvertable().asActivity();
		acceptActivity.setType(Set.of(vocabContainer.vocAs().Accept()));
						
		acceptActivity.addObject(followActivityToAccept);
    	
		RdfPubObjectIdActivity activityLocation = getRdfPubClient().sendToOutbox(currentActor, acceptActivity);
    	
		log.debug("acceptFollow created activity: " + activityLocation);
		
    	return collectionController.collectionPendingFollowers(model, authentication, httpSession);
    }

    
    private Activity findForFollower(Set<Activity> pendingFollower, String follower) {
    	return pendingFollower.stream()
        		.filter(a->{
        			String actor = a.getActor().stream().findFirst()
        					.map(RdfPubBlankNodeOrIRI::getIRIString)
        					.orElse("");        			
        			return actor.equals(follower);
        		})
        		.findFirst().orElseThrow();

    }

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @param subject
     * @return The name of the fragment to show
     */
    @GetMapping("/rejectFollow")
	public String rejectFollow(Model model, Authentication authentication, HttpSession httpSession,
			@RequestParam(name = "subject") String subject, @RequestParam(name = "follower") String follower) {
    	log.debug("rejectFollow: " + subject);
    	
    	Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));
    	
    	Activity activity = createRejectActivity(subject);
    	RdfPubBlankNodeOrIRI followerId = activityPubObjectIdBuilder.createFromUrl(follower);
    	activity.addTo(followerId);    		
    	getRdfPubClient().sendToOutbox(currentActor, activity);
    	return collectionController.collectionPendingFollowers(model, authentication, httpSession);
    }

	private Activity createRejectActivity(String subject) {
		ActivityPubObject ao = getRdfPubClient().createNewEmpty();
		Activity activity = ao.asConvertable().asActivity();
		activity.addType(vocabContainer.vocAs().Reject());    		
		RdfPubBlankNodeOrIRI followActivityId = activityPubObjectIdBuilder.createFromUrl(subject);
		activity.setObject(Set.of(followActivityId));
		return activity;
	}

    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @param subject
     * @return The name of the fragment to show
     */
	@GetMapping("/ignoreFollow")
	public String ignoreFollow(Model model, Authentication authentication, HttpSession httpSession, @RequestParam(name = "subject") String subject) {
		log.debug("ignoreFollow: " + subject);

		Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));

		Activity activity = createRejectActivity(subject);
		
		getRdfPubClient().sendToOutbox(currentActor, activity);
		
		return collectionController.collectionPendingFollowers(model, authentication, httpSession);
	}
	
    /**
     * 
     * @param model
     * @param authentication
     * @param httpSession
     * @param subject
     * @return The name of the fragment to show
     */
	@GetMapping("/removeFollower")
	public String removeFollower(Model model, Authentication authentication, HttpSession httpSession, @RequestParam(name = "subject") String subject) {
		log.debug("removeFollower: " + subject);
		
		Actor currentActor = getCurrentActor(authentication,  httpSession)
				.orElseThrow(()->new IllegalStateException("user is not authenticated!"));

		ActivityPubObject ao = getRdfPubClient().createNewEmpty();
		Activity activity = ao.asConvertable().asActivity();
		activity.addType(vocabContainer.vocAs().Remove());
		RdfPubBlankNodeOrIRI followerToRemove = activityPubObjectIdBuilder.createFromUrl(subject);
		activity.addTo(followerToRemove);		
		activity.setObject(Set.of(followerToRemove));
		activity.setTarget(currentActor.getFollowers());
		
		getRdfPubClient().sendToOutbox(currentActor, activity);
		
		return collectionController.collectionFollowers(model, authentication, httpSession);
	}
}

	