package org.linkedopenactors.ap.rdfpubtestapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 */
@Data
@AllArgsConstructor
public class PredicateDescriptor {

	private String id;
	private String name;
	private String description;
}
