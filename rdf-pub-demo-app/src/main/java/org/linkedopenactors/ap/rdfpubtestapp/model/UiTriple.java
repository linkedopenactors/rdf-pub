package org.linkedopenactors.ap.rdfpubtestapp.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.Literal;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Data
@Slf4j
public class UiTriple {

	private String humanReadableName;
	private String predicate;
	private String description;
	private Object value;
	private boolean isIRI;
	private boolean isBlankNode;
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 * @param predicate
	 * @param humanReadableName
	 * @param description
	 * @param value
	 * @param isIRI
	 * @param isBlankNode
	 */
	public UiTriple(VocabContainer vocabContainer, String predicate, String humanReadableName, String description, Object value, boolean isIRI, boolean isBlankNode) {
		this.vocabContainer = vocabContainer;
		this.predicate = predicate;
		this.humanReadableName = humanReadableName;
		this.description = description;
		if(!(isIRI || isBlankNode)) {
			if(!(value instanceof String || value instanceof Literal || isPropertyList())) {
				String valueClass = Optional.ofNullable(value).map(Object::getClass).map(Class::getName).orElse("null");
				throw new IllegalStateException("value is: " + value + " / value class is: " + valueClass);
			}
		}
		this.value = value;		
		this.isIRI = isIRI;
		this.isBlankNode = isBlankNode;
	}

	/**
	 * 
	 * @return Value as String.
	 */
	public String getValue() {
//		if(isIRI) {
//			throw new IllegalStateException("You have to solve the IRI link! " + value.toString());
//		}
//		if(isBlankNode) {
//			throw new IllegalStateException("You have to solve the BlankNode link!" + value.toString());
//		}
		if("https://www.w3.org/ns/activitystreams#first".equals(predicate)) {
			log.debug("first pred");
		}
		
		if("first".equals(getHumanReadableName())) {
			log.debug("first");
		}
		
		if(value instanceof BlankNode) {
			log.debug("BlankNode");
		}
		
		if(value instanceof Literal) {
			Literal literalValue = (Literal)value;
			if(isDateTime(literalValue)) {
				return processDateTime(literalValue);
			}
		}
		if(value instanceof BlankNode) {
			return "unresolvable BlankNode";
		}
		if(isPropertyList()) {
			throw new IllegalStateException("You have to solve the properties!" + value.toString());
		}
		String string = value.toString();
		return string;
	}

	private String processDateTime(Literal literalValue) {
		Instant instant = Instant.parse(literalValue.getLexicalForm());
		ZoneId zone = ZoneId.systemDefault(); // TODO use users locale
		LocalDateTime ldt = LocalDateTime.ofInstant(instant, zone);
		return DateTimeFormatter.ofPattern("dd LLL yyyy HH:mm.ss").format(ldt);
	}

	private boolean isDateTime(Literal literalValue) {
		return vocabContainer.vocXsdDatatypes().dateTime().getIRIString().equals(literalValue.getDatatype().getIRIString());
	}

	private boolean isPropertyList() {
		boolean itIs = false;
		if(value instanceof List) {
			@SuppressWarnings("rawtypes")
			List listVal = (List) value;
			if(!listVal.isEmpty()) {
				itIs = listVal.get(0) instanceof UiTriple;
			}
		}
		return itIs;		
	}
	
	/**
	 * 
	 * @return True, if this is a String or Literal.
	 */
	public boolean isStringOrLiteral() {
		Boolean isString = Boolean.valueOf(this.value instanceof String);
		Boolean isLiteral = Boolean.valueOf(this.value instanceof Literal);
		boolean x = isString||isLiteral;
		return x;
	}
}
