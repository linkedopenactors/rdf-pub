rm -rf feditest-tests-fediverse
pip install feditest
git clone --branch v0.3 --recurse-submodules https://github.com/fediverse-devnet/feditest-tests-fediverse.git
cd feditest-tests-fediverse
feditest run --testplan ../webfinger-server-imp-vs-saas-any.json --html quickstart-results.html
xdg-open quickstart-results.html
cd ..