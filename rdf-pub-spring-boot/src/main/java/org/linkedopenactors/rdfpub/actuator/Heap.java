package org.linkedopenactors.rdfpub.actuator;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 *
 */
@Component
@Endpoint(id = "heap")
public class Heap {

    @ReadOperation
    public MemoryStats heap() {
        MemoryStats stats = new MemoryStats();
        stats.setHeapSize((Runtime.getRuntime().totalMemory()/1024/1024) + " mb");
        stats.setHeapMaxSize((Runtime.getRuntime().maxMemory()/1024/1024) + " mb");
        stats.setHeapFreeSize((Runtime.getRuntime().freeMemory()/1024/1024) + " mb");
        return stats;
    }

    @Data
    public class MemoryStats {
    	private String heapSize;
    	private String heapMaxSize;
    	private String heapFreeSize;
    }
}
