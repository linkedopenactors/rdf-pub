package org.linkedopenactors.rdfpub.config.init;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.linkedopenactors.rdfpub.app.RdfPubMDC;
import org.linkedopenactors.rdfpub.app.service.actor.CmdCreateInstance;
import org.linkedopenactors.rdfpub.app.service.actor.UcCreateInstanceApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class InitRdfPubDatabase {

	@Autowired
	private UcCreateInstanceApplication ucCreateInstanceApplication;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private PublicActorStore publicActorsStore;
	
	@PostConstruct	
    public void init() {
		RdfPubMDC.put("InitRdfPubDb");
		try {
			initInternal();
		} finally {
			RdfPubMDC.remove();
		}
	}
	
	private void initInternal() {
		
		log.debug("-> InitRdfPubDatabase");

		RdfPubObjectId instanceActorSubject = activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME);
		
		Optional<Actor> instance = publicActorsStore.findBySubject(instanceActorSubject)
			.map(ActivityPubObject::asConvertable)
			.map(ActivityPubObjectConvertable::asActor);
		
		if(instance.isEmpty()) {
			ucCreateInstanceApplication.perform(new CmdCreateInstance());
		}
		log.debug("<- InitRdfPubDatabase");		
	}
}
