package org.linkedopenactors.rdfpub.config.properties;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.rdf.api.RDF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Setter;


@Setter
@EqualsAndHashCode
@Component
@Validated
@ConfigurationProperties(prefix = "instance", ignoreUnknownFields = false)
class InstancePropertiesSpring implements org.linkedopenactors.rdfpub.domain.InstanceProperties {

	@Autowired
	private RDF rdf;
	
	@NotBlank 
	private String preferedUsername;
	
	@NotBlank 
	private String version;
	
	@NotBlank 
	@Value("${git.commit.id.abbrev:unknown}")
	private String commitId;
	
	@NotBlank	 
	private String domain;
	
	@NotBlank	 
	private String internalUrnString;
	
	@NotBlank 
	private String identifier;

//	@Autowired
//	private ResourceFactory resourceFactory;
	
	public String getPreferedUsername() {
		return preferedUsername;
	}

	public String getMavenVersion() {
		return version;
	}

	public String getCommitId() {
		return commitId;
	}

	public String getInstanceDomainString() {
		return domain.endsWith("/") ? domain.substring(0, domain.length()-1) : domain;
	}

	public String getIdentifier() {
		return identifier;
	}

	public URL getInstanceDomain() {
		try {
			return new URI(getInstanceDomainString()).toURL();
		} catch (MalformedURLException | URISyntaxException e) {
			throw new IllegalStateException(getInstanceDomainString() + " is not a valid URL.", e);
		}
	}

	@Override
	public String getInternaleIriPrefix() {
		String baseIriString = internalUrnString;
		if(!baseIriString.endsWith(":")) {
			baseIriString = baseIriString.endsWith("/") ? baseIriString : baseIriString + "/";
		}
		return baseIriString;
	}

	@Override
	public String getExternalIriPrefix() {
		return getInstanceDomainString();
	}

	@Override
	public String toString() {
		return "InstancePropertiesSpring [preferedUsername=" + preferedUsername + ", version=" + version + ", commitId="
				+ commitId + ", domain=" + domain + ", internalUrnString=" + internalUrnString + ", identifier="
				+ identifier + "]";
	}
}
