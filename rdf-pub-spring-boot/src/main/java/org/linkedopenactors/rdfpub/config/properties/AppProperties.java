package org.linkedopenactors.rdfpub.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
@Component
@Validated
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = false)
public class AppProperties {

	@NotNull
	private Boolean unittest;
	
	@NotBlank
	private String rdfRepositoryHome;
}
