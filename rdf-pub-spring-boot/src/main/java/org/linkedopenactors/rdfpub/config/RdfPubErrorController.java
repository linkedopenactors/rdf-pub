package org.linkedopenactors.rdfpub.config;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class RdfPubErrorController implements ErrorController {

	@RequestMapping("/error")
	@ResponseBody
	public String handleError(HttpServletRequest request) {
		HttpStatus status = getStatus(request);
		String errorMessage = (String) request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
		return "{\n" + "    \"reason\" : \"" + status.getReasonPhrase() + "\",\n" + "    \"code\" : \"" + status.value()
				+ "\",\n" + "    \"message\" : \"" + errorMessage + " \"\n" + "} ";

	}

	protected HttpStatus getStatus(HttpServletRequest request) {
		Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		if (statusCode == null) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		try {
			return HttpStatus.valueOf(statusCode);
		} catch (Exception ex) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}
}