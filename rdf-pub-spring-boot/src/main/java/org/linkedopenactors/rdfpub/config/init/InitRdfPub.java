package org.linkedopenactors.rdfpub.config.init;

import java.util.List;

import org.linkedopenactors.rdfpub.adapter.http.FredysSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.security.oauth2.client.web.server.ServerOAuth2AuthorizedClientRepository;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * 
 */
@Configuration
@Slf4j
public class InitRdfPub {

//	@Bean
//	WebClient webClient(WebClient.Builder builder) {
//	    return builder
//	    		.filters(exchangeFilterFunctions -> {
//	    			exchangeFilterFunctions.add(logRequest());
//	    			exchangeFilterFunctions.add(logResponse());
//	    			})
//	    		// To activate redirect 3xx
////	    		.clientConnector(new ReactorClientHttpConnector(HttpClient.create().followRedirect(true)))
//	    		.build();
//	}
	
	/**
	 * @param clientRegistrations
	 * @param authorizedClients
	 * @return WebClient 
	 */
	@Bean
	@Profile("!test")
	public WebClient webClient(ReactiveClientRegistrationRepository clientRegistrations,
			ServerOAuth2AuthorizedClientRepository authorizedClients) {
		ServerOAuth2AuthorizedClientExchangeFilterFunction oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(
				clientRegistrations, authorizedClients);
		oauth.setDefaultOAuth2AuthorizedClient(true);
		WebClient wc = WebClient.builder()
				.filters(exchangeFilterFunctions -> {
					exchangeFilterFunctions.add(logRequest());
					exchangeFilterFunctions.add(logResponse());
				})				
				.filter(oauth)
				.build();
		return wc;
	}

//	@Bean
//	public WebClient webClient(ReactiveClientRegistrationRepository clientRegistrations,
//			ServerOAuth2AuthorizedClientRepository authorizedClients) {
//		ServerOAuth2AuthorizedClientExchangeFilterFunction oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(
//				clientRegistrations, authorizedClients);
//		oauth.setDefaultOAuth2AuthorizedClient(true);
//		WebClient wc = WebClient.builder().filters(exchangeFilterFunctions -> {
//			exchangeFilterFunctions.add(logRequest());
//			exchangeFilterFunctions.add(logResponse());
//		})
//		.filter(oauth)
//		// To activate redirect 3xx
//		//	.clientConnector(new ReactorClientHttpConnector(HttpClient.create().followRedirect(true)))
//
//		.filter(oauth).build();
//		return wc;
//	}

	@Bean
	FredysSignature fredysSignature() {
		return new FredysSignature();
	}
	
	private ExchangeFilterFunction logResponse() {
		 return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
		        if (log.isDebugEnabled()) {
			    	StackTraceElement myCaller = Thread.currentThread().getStackTrace()[2];
			    	Logger logger = LoggerFactory.getLogger(myCaller.getClassName());
		            StringBuilder sb = new StringBuilder("Response: ");
		            if(logger.isTraceEnabled()) {
		            	sb.append("\n");
			            clientResponse
			              .headers()
			              .asHttpHeaders()
			              .forEach((name, values) -> sb.append("\n\theader: " + name + " - " + values));
			            sb.append("\n\t");
		            }
			        sb.append("status: " + clientResponse.statusCode());
			        logger.debug(sb.toString());
		        }
		        return Mono.just(clientResponse);
		    });
		}

	private ExchangeFilterFunction logRequest() {
		 return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
		        if (log.isDebugEnabled()) {
			    	StackTraceElement myCaller = Thread.currentThread().getStackTrace()[2];
			    	Logger logger = LoggerFactory.getLogger(myCaller.getClassName());
			    	StringBuilder sb = new StringBuilder("Request: \n");
		            sb.append(clientRequest.method().toString() + " " + clientRequest.url().getPath() + " HTTP/1.1"); 
		            sb.append("\nhost: " + clientRequest.url().getHost());
		            clientRequest
		              .headers()
		              .forEach((name, values) -> sb.append("\n" + name + ": " + toString(values)));
		            sb.append("body: \n" + clientRequest.body().toString());
		            logger.debug(sb.toString());
		        }
		        return Mono.just(clientRequest);
		    });	}

	private String toString(List<String> values) {
		if(values.size()==1) {
			return values.getFirst().toString();
		}
		return values.toString();
	}
}
