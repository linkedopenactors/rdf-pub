package org.linkedopenactors.rdfpub.config;

import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Definition of some common beans. 
 * @author <a href="http://hauschel.de">SofwareEngineering Hauschel</a>
 */
@Configuration
@EnableScheduling
public class ApConfig {

	public static final String PROFILE_UNIT_TEST = "unittest";
	
	@Bean
	public HttpExchangeRepository htttpTraceRepository()
	{
	  return new InMemoryHttpExchangeRepository();
	}
}
