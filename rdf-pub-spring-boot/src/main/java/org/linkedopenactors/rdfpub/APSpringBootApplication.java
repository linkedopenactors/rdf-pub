package org.linkedopenactors.rdfpub;

import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@SpringBootApplication(scanBasePackages = {
		"org.linkedopenactors.rdfpub.controller",
		"org.linkedopenactors.rdfpub.app",
		"org.linkedopenactors.rdfpub.filter",
		"org.linkedopenactors.rdfpub.logging",
		"org.linkedopenactors.rdfpub.config",
		"org.eclipse.rdf4j.http.server.readonly.sparql",
		"org.linkedopenactors.rdfpub.store.rdf4j.vocab",
		"org.linkedopenactors.rdfpub.store.rdf4j",
		"org.linkedopenactors.rdfpub.domain",
		"org.linkedopenactors.rdfpub.domain.commonsrdf",
		"org.linkedopenactors.rdfpub.adapter.driving",
		"org.linkedopenactors.rdfpub.adapter.driving.nodeinfo",
		"org.linkedopenactors.rdfpub.adapter.driven",
		"org.linkedopenactors.rdfpub.adapter.driven.ext",
		"org.linkedopenactors.rdfpub.adapter.http",
		"org.linkedopenactors.rdfpub.config.upgrade",
		"org.rdfpub.vocabulary"
})
public class APSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(APSpringBootApplication.class, args);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propsConfig 
          = new PropertySourcesPlaceholderConfigurer();
        propsConfig.setLocation(new ClassPathResource("git.properties"));
        propsConfig.setIgnoreResourceNotFound(true);
        propsConfig.setIgnoreUnresolvablePlaceholders(true);
        return propsConfig;
    }
    
    @Bean
    public RDF rdf() {
    	return new RDF4J();
    }
}
