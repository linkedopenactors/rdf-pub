package org.linkedopenactors.rdfpub.logging;

import java.io.IOException;
import java.util.Optional;

import org.linkedopenactors.rdfpub.adapter.driving.UserAgentExtractor;
import org.linkedopenactors.rdfpub.app.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 */
@Component
@Order(3)
public class RequestLogFilter implements jakarta.servlet.Filter {

	private final static Logger LOG = LoggerFactory.getLogger(RequestLogFilter.class);

	public RequestLogFilter() {
		LOG.info("Initializing RequestLogFilter");
	}
	
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		LOG.info("Initializing filter :{}", this);
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		if(!isContent(req)) {
//		boolean inboxCall = req.getRequestURI().endsWith("inbox");
//		if(!inboxCall &&  req.getRequestURI().startsWith("/"+RdfPubObjectId.PREFIX_TOKEN) ) {
			String user = Optional.ofNullable(req.getRemoteUser()).map(u->" user: " + u).orElse("");
			String msg = "("+req.getMethod()+") " + req.getRequestURI() + user;
			LOG.info("-> " + msg);
			LOG.info("\t " + new UserAgentExtractor().extractUserAgent(req).map(UserAgent::toString).orElse("no User-Agent"));
		}
		chain.doFilter(request, response);
//		if(!inboxCall &&  req.getRequestURI().startsWith("/"+RdfPubObjectId.PREFIX_TOKEN) ) {
		if(!isContent(req)) {
			int status = ((HttpServletResponse) response).getStatus();
			LOG.info("<- (" + status + ") " + req.getRequestURI());
		}
	}
	
	private boolean isContent(HttpServletRequest request) {
		return request.getRequestURI().startsWith("/content");
	}
	
	@Override
	public void destroy() {		
		LOG.warn("Destructing filter :{}", this);
	}
}
