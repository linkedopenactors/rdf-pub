package org.linkedopenactors.rdfpub.scheduler.tempdir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TempFileManagerProperties {
	private String customTempDirName;
	private String prefix;
	private String postix;
	private long maxAgeInSeconds;

}
