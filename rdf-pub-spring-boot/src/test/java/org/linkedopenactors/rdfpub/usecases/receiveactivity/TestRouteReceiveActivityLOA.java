package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;

import com.nimbusds.jose.util.StandardCharset;

import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.renderer.SimpleTextRenderer;

@SpringBootTest(classes = {TestingWebApplication.class}, properties = "app.rdfRepositoryHome=target/store/unittest/TestRouteReceiveActivityLOA")
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles("test")
@Disabled
public class TestRouteReceiveActivityLOA {	

	private static EtmMonitor monitor;
	
	@Autowired
	private TestHelperV2 testHelperV2;

	@BeforeEach
	public void setup() {
		BasicEtmConfigurator.configure();
		monitor = EtmManager.getEtmMonitor();
		monitor.start();
	}

	@AfterEach
	public void tearDown() {
		monitor.render(new SimpleTextRenderer());
		monitor.stop();
	}

	@Test
	public void testSuccess() throws Exception {
		String userId = Integer.toString(new Random().ints(1111, 9999).findFirst().getAsInt());
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");
		
		String loa = IOUtils.toString(new ClassPathResource("loa.ttl").getInputStream(), StandardCharset.UTF_8);
		String identifier = UUID.randomUUID().toString();
		loa = loa.replace("9d317daca74246d4be41b1a37e30ee2a", identifier);
		StringReader sr = new StringReader(loa);
		Model model = Rio.parse(sr, RDFFormat.TURTLE);
		
		StringWriter sw = new StringWriter();
		Rio.write(model, sw, RDFFormat.JSONLD);
		
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(actor.getOutbox().toString())
				.with(jwt)
				.content(sw.toString())
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);
	
		assertNotNull(activityLocation);
		mvcResult =  testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model activityModel = Rio.parse(sr,  RDFFormat.JSONLD);
		
		IRI objectIri = Models.getPropertyIRI(activityModel, iri(activityLocation), Values.iri(VocAs.object)).orElseThrow();
		mvcResult =  testHelperV2.getMockMvc().perform(get(objectIri.stringValue())
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model objectModel = Rio.parse(sr,  RDFFormat.JSONLD);
		
		String identifierOfObject = Models.getPropertyLiteral(objectModel, objectIri, Values.iri(VocSchema.identifier)).orElseThrow().stringValue();
		assertEquals(identifier, identifierOfObject);
		// TODO check if everything was saved correctly. hard work!
		// maybe the loa.ttl is not the best id and we should do it with Models
	}
}
