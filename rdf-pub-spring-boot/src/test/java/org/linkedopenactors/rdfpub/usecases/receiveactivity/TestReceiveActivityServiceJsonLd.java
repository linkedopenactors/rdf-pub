package org.linkedopenactors.rdfpub.usecases.receiveactivity;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = {TestingWebApplication.class}, properties = "app.rdfRepositoryHome=target/store/unittest/TestReceiveActivityServiceJsonLd")
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
public class TestReceiveActivityServiceJsonLd {	
	
	@Autowired
	private TestHelperV2 testHelperV2;

	@Test
	public void testSuccess() throws Exception {

		String userId = UUID.randomUUID().toString();
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");

		StringReader sr = new StringReader(sampleMessage(UUID.randomUUID().toString(), actor.getSubject().toString()));
		Model activity = Rio.parse(sr, RDFFormat.JSONLD);
		
		StringWriter sw = new StringWriter();
		Rio.write(activity, sw, RDFFormat.JSONLD);
		
		MvcResult mvcResult;
		try {
			String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
			mvcResult = testHelperV2.getMockMvc().perform(post(url)
					.with(jwt)
					.content(sw.toString())
				    .contentType("application/ld+json")
					.header(HttpHeaders.ACCEPT, "application/ld+json"))				
					.andExpect(status().isCreated())
					.andReturn();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);

		mvcResult =  testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model reReadModel = Rio.parse(sr,  RDFFormat.JSONLD);
		
		
		Set<IRI> propertyIRIs = Models.getPropertyIRIs(reReadModel, iri(activityLocation), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Create)));
//		assertEquals(AS.Create,  reReadModel.filter(Values.iri(activityLocation), RDF.TYPE, null).stream().findFirst().get().getObject());
		IRI object = iri(reReadModel.filter(Values.iri(activityLocation), Values.iri(VocAs.object), null).stream().findFirst().get().getObject().stringValue());

		mvcResult =  testHelperV2.getMockMvc().perform(get(object.stringValue())
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model reReadObjectModel = Rio.parse(sr,  RDFFormat.JSONLD);

		Set<String> typesAsString = reReadObjectModel.filter(object, RDF.TYPE, null).stream()
			.map(Statement::getObject)
			.filter(Value::isResource)
			.map(Value::stringValue)
			.collect(Collectors.toSet());
		
		assertTrue(typesAsString.contains(Values.iri(VocAs.Note).stringValue()));
	}

	private String sampleMessage(String identifier, String actorId) {
		return "{\n"
				+ "   \"@context\":[\n"
				+ "      \"https://schema.org/docs/jsonldcontext.json\",\n"
				+ "      \"https://www.w3.org/ns/activitystreams\"\n"
				+ "   ],\n"
				+ "   \"type\":\"Create\",\n"
				+ "   \"version\":1,\n"
				+ "   \"identifier\":\"" + identifier + "\",\n"
				+ "   \"id\":\"https://social.example/alyssa/posts/a29a6843-9feb-4c74-a7f7-081b9c9201d3\",\n"
				+ "   \"to\":[\n"
				+ "      \"https://example.org/ben/\"\n"
				+ "   ],\n"
				+ "   \"actor\":\"" + actorId + "\",\n"
				+ "   \"object\":{\n"
				+ "      \"type\":\"Note\",\n"
				+ "      \"id\":\"https://social.example/alyssa/posts/49e2d03d-b53a-4c4c-a95c-94a6abf45a19\",\n"
				+ "      \"version\":1,\n"
				+ "      \"identifier\":\"" + identifier + "XXX" + "\",\n"
				+ "      \"attributedTo\":\"https://social.example/alyssa/\",\n"
				+ "      \"to\":[\n"
				+ "         \"https://chatty.example/ben/\"\n"
				+ "      ],\n"
				+ "      \"content\":\"Say, did you finish reading that book I lent you?\"\n"
				+ "   }\n"
				+ "}";
	}
}
