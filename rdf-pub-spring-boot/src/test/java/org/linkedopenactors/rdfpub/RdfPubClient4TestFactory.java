package org.linkedopenactors.rdfpub;

import org.linkedopenactors.rdfpub.client2024.RdfPubClient;
import org.linkedopenactors.rdfpub.client2024.RdfPubClientDefault;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;

public class RdfPubClient4TestFactory {
	
	private TestHelperV2 testHelper;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private String serverBaseUrlParam;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;


	public RdfPubClient4TestFactory(TestHelperV2 testHelper, 
			ActivityPubObjectFactory activityPubObjectFactory,
			String serverBaseUrlParam,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.testHelper = testHelper;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.serverBaseUrlParam = serverBaseUrlParam;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}


	public RdfPubClient getRdfPubClient(String userId, String preferredUsername) {
		return new RdfPubClientDefault(testHelper.getHttpAdapter2024(userId, preferredUsername), activityPubObjectFactory,
				serverBaseUrlParam,
				activityPubObjectIdBuilder);
	}
	
	public RdfPubClient getRdfPubClient4Anonymous() {
		return new RdfPubClientDefault(testHelper.getHttpAdapter2024(null, null), activityPubObjectFactory,
				serverBaseUrlParam,
				activityPubObjectIdBuilder);
	}

}
