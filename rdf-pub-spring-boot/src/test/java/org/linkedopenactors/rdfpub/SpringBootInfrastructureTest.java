package org.linkedopenactors.rdfpub;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT,
	classes = TestingWebApplication.class)
@ActiveProfiles({"signatureTest", "test"})
public class SpringBootInfrastructureTest {
	
	@Value("${instance.domain}")
	private String instanceDomain;
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void swaggerUi() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/content/swagger-ui/index.html",
				String.class)).contains("<title>Swagger UI</title>");
	}

    @Test
    public void shouldReturnApiDocs() {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/content/v3/api-docs",
				String.class)).contains("{\"openapi\":\"3.0.1\",\"info\":{\"title\":\"OpenAPI definition\",\"version\":\"v0\"}");
    }
    
    @Test
    public void actuator() {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/actuator",
				String.class)).contains("{\"self\":{\"href\":\"http://localhost:" + port + "/actuator\",\"templated\":false}");
    }

    @Test
    public void mavenSite() {
		assertThat(this.restTemplate.getForEntity("http://localhost:" + port + "/content/generated/rdf-pub/index.html",
				String.class).getStatusCode().equals(HttpStatus.OK));
    }

    @Test
    public void javadoc() {
		assertThat(this.restTemplate.getForEntity("http://localhost:" + port + "/content/generated/rdf-pub/apidocs/index.html",
				String.class).getStatusCode().equals(HttpStatus.OK));
    }
    
    @Test
    public void documentation() {
		assertThat(this.restTemplate.getForEntity("http://localhost:" + port + "/content/generated/generated-docs/index.html",
				String.class).getStatusCode().equals(HttpStatus.OK));
    }

    @Test
    public void jetm() {
		assertThat(this.restTemplate.getForEntity("http://localhost:" + port + "/jetm/print",
				String.class).getStatusCode().equals(HttpStatus.OK));
    }
}