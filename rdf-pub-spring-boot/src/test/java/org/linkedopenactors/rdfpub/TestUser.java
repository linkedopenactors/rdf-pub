package org.linkedopenactors.rdfpub;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestUser {
	private String userId;
	private String preferredUserName;
}

