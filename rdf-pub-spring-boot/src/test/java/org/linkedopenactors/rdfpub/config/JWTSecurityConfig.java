package org.linkedopenactors.rdfpub.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Profile("test")
@Configuration
public class JWTSecurityConfig {
		
	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
        http.csrf(AbstractHttpConfigurer::disable)// TODO csrf !!
        	.authorizeHttpRequests(authz -> authz
        				.requestMatchers("/asPublic/dump", "/actuator/env").hasRole("admin") // TODO validate that this is only accessible by admins
        				.requestMatchers("/admin/**").hasRole("superadmin")
        				.requestMatchers(        			
							"/",
							"/favicon.ico",
							"/content/**",
							"/actuator/**",
							"/api-doc",
							"/v3/api-docs/**",
							"/swagger-resources/**",
							"/content/swagger-ui/**",
							"/content/swagger-ui/index.html**",
							"/webjars/**",
							"/asPublic/sparql",
							"/.well-known/webfinger/",
							"/.well-known/webfinger*",
							"/.well-known/rdfpub",
							"/.well-known/nodeinfo",
							"/nodeinfo/2.1",
							"/error",
							"/actors/persons"
							,"/*/outbox"
							,"/*/outbox/*"
							,"/*/inbox"
							,"/resource/**"
							,"/resource/instanceActor/*"
		                    ,"/.well-known/oauth-authorization-server"
		                    ,"/oauth/*"
							).permitAll()
            .anyRequest()
            .authenticated()
            );
        return http.build();
    }
}
