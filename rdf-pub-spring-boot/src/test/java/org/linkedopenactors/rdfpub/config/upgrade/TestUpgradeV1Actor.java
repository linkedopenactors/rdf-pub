package org.linkedopenactors.rdfpub.config.upgrade;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestActorProfile")
public class TestUpgradeV1Actor {

	@Autowired
	private UpgradeActors upgradeActors; 
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private TestHelperV2 testHelperV2;

	private Actor actorMax;

	@BeforeEach
	public void setup() throws Exception {
		URI issuer = URI.create("http://example.com");
		String maxUserId = "0816_" + RandomStringUtils.random(10, true, false);
		String maxPreferredUsername = "max_" + RandomStringUtils.random(10, true, false);
		
		Actor actorMaxExtern = testHelperV2.createActor(maxUserId, maxPreferredUsername, issuer.toString());
		
		String asTurtle = actorMaxExtern.toString(RdfFormat.TURTLE, true);
		ActivityPubObject ao = activityPubObjectFactory.create(RdfFormat.TURTLE, asTurtle, 
				actorMaxExtern.getSubject().getIRIString());
		actorMax = ao.asConvertable().asActor();
	}
	
	@Test
	@Disabled // cannot be tested after createActor is adjusted ;-(
	public void test() {

		String outbox = VocAs.outbox.replace(VocAs.NS, "");
		assertEquals(actorMax.getOutbox().getIRIString(), actorMax.getSubject().getIRIString() + "/" + outbox);
		String inbox = VocAs.inbox.replace(VocAs.NS, "");
		assertEquals(actorMax.getInbox().getIRIString(), actorMax.getSubject().getIRIString() + "/" + inbox);
		String liked = VocAs.liked.replace(VocAs.NS, "");
		
		assertEquals(actorMax.getPendingFollowers().getIRIString(), actorMax.getSubject().getIRIString() + "/" + VocPending.pendingFollowers);
		assertEquals(actorMax.getPendingFollowing().map(RdfPubBlankNodeOrIRI::getIRIString).orElseThrow(), actorMax.getSubject().getIRIString() + "/" + VocPending.pendingFollowing);
		Actor upgraded = upgradeActors.upgradeV1Actor(actorMax);
		
		String newPrefix = actorMax.getSubject() + "/col/";
		assertEquals(newPrefix + outbox, upgraded.getOutbox().getIRIString());
		assertEquals(newPrefix + inbox, upgraded.getInbox().getIRIString());
		assertEquals(newPrefix + liked, upgraded.getLiked().getIRIString());
		assertEquals(newPrefix + VocPending.pendingFollowers, upgraded.getPendingFollowers().getIRIString());
		assertEquals(newPrefix + VocPending.pendingFollowing, upgraded.getPendingFollowing().map(RdfPubBlankNodeOrIRI::getIRIString).orElseThrow());
	}
}
