package org.linkedopenactors.rdfpub;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.uc.TestHelperV2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication(scanBasePackages = {
		"org.linkedopenactors.rdfpub.app",
		"org.linkedopenactors.rdfpub.config",
        "org.linkedopenactors.rdfpub.config.properties",
		"org.linkedopenactors.rdfpub.config.security",
		"org.eclipse.rdf4j.http.server.readonly.sparql",
		"org.linkedopenactors.rdfpub.store",
		"org.linkedopenactors.rdfpub.store.rdf4j",
		"org.linkedopenactors.rdfpub.domain",
		"org.linkedopenactors.rdfpub.domain.commonsrdf",
		"org.linkedopenactors.rdfpub.adapter.driving",
		"org.linkedopenactors.rdfpub.adapter.driven",
		"org.linkedopenactors.rdfpub.adapter.http",
		"org.linkedopenactors.rdfpub.usecases.receiveactivity",
		"org.rdfpub.vocabulary"
//		,
//		"org.linkedopenactors.rdfpub.app.uc.c2s",
//		"org.linkedopenactors.rdfpub.app.uc.s2s"
})
@EnableAutoConfiguration
@Slf4j
public class TestingWebApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TestingWebApplication.class, args);
	}
	
	@Bean
	WebClient webClient(WebClient.Builder builder) {
	    return builder
	    		.build();
	}
	
	@Bean
	public TestHelperV2 testhelper(WebApplicationContext context, ActivityPubObjectFactory activityPubObjectFactory//, ObjectFactory objectFactory,
//			ToActivityPubObject stringToActivityPubObject
//			,org.linkedopenactors.rdfpub.domain.ToActivityPubObject toActivityPubObject
			) {
		return new TestHelperV2(context, activityPubObjectFactory);//, objectFactory, stringToActivityPubObject);//, toActivityPubObject);
	}
	
	@Bean
	public RDF rdf() {
		return new org.apache.commons.rdfrdf4j.RDF4J();
	}
	
	
	@Bean 
	public RequestResponseLoggingFilter requestResponseLoggingFilter()  {
		return new RequestResponseLoggingFilter();
	}
	
	@Bean 
	public RdfPubClient4TestFactory rdfPubClient4TestFactory(TestHelperV2 testHelper, 
			ActivityPubObjectFactory activityPubObjectFactory,
			@Value("${instance.domain}") String serverBaseUrlParam,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		return new RdfPubClient4TestFactory(testHelper, 
				activityPubObjectFactory,
				serverBaseUrlParam,
				activityPubObjectIdBuilder);
				
	}
}
