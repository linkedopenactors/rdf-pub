package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;

public class ActivityPubObjectIdBuilderFactory4Test {

	public static ActivityPubObjectIdBuilder create(RDF rdf, PrefixMapper prefixMapper) {
		VocabContainerDefault vocabContainer = new VocabContainerDefault(rdf);
		return new ActivityPubObjectIdBuilderDefault(vocabContainer, rdf, prefixMapper);
	}
}
