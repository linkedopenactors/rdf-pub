package org.linkedopenactors.rdfpub.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.scheduler.tempdir.TempFileManager;

class TestTempFileManager {

	@Test
	void test() throws IOException, InterruptedException {
		TempFileManager tfm = new TempFileManager("tmp" + System.currentTimeMillis(), "pre", ".ttl", 1);
		for (int i = 0; i < 5; i++) {
			tfm.createTempFile();
		}
		Thread.sleep(1000*2);
		assertEquals(5,tfm.getTempFiles().size());
		for (int i = 0; i < 3; i++) {
			tfm.createTempFile();
		}
		tfm.cleanup();
		assertEquals(3,tfm.getTempFiles().size());
		Thread.sleep(1000*3);
		tfm.cleanup();
		assertEquals(0,tfm.getTempFiles().size());
	}
}
