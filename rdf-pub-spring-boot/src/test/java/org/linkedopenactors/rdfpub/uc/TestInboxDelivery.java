package org.linkedopenactors.rdfpub.uc;

import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

/**
 * This test is not testing rdf-pub functionality, but it's showing how to save/read Lists. 
 */
@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestInboxDelivery")
class TestInboxDelivery {

	@Autowired
	private TestHelperV2 testHelperV2;

	@Test
	void test() throws Exception {
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");

		ActivityPubObjectIdBuilder activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);

		
		String userIdSender = UUID.randomUUID().toString();
		String preferredUsernameSender = "senderPreferredUsername_" + RandomStringUtils.random(10, true, false);
		JwtRequestPostProcessor jwtSender = testHelperV2.jwt(userIdSender, preferredUsernameSender, "http://example.com");
		Actor sender = testHelperV2.createActor(userIdSender, preferredUsernameSender, "http://example.com");
		RdfPubObjectIdActor rdfPubObjectIdActor = sender.getSubject().asActor().orElseThrow();

		RdfPubObjectIdActivity activityId = activityPubObjectIdBuilder.createActivity(rdfPubObjectIdActor);
		
		String userIdReceiver = UUID.randomUUID().toString();
		String preferredUsernameReceiver = "receiverPreferredUsernameId" + RandomStringUtils.random(10, true, false);
		JwtRequestPostProcessor jwtReceiver = testHelperV2.jwt(userIdReceiver, preferredUsernameReceiver, "http://example.com");
		Actor receiver = testHelperV2.createActor(userIdReceiver, preferredUsernameReceiver, "http://example.com");

		RdfPubBlankNodeOrIRI asObjectRdfPubIRI = activityPubObjectIdBuilder.createResource(rdfPubObjectIdActor).asResource().orElseThrow();
		IRI asObjectIri = Values.iri(asObjectRdfPubIRI.getIRIString());
		Literal nameLiteral = literal("SampleObject");
		String receiverSubjectString = receiver.getSubject().toString();
		Model initialActivity = new ModelBuilder()
				.subject(activityId.getExternalSubject().getIRIString())
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), asObjectIri)
					.add(Values.iri(VocAs.to), iri(receiverSubjectString))
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(asObjectIri)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocAs.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();

		// Send initial Activity to senders outbox 
		StringWriter sw = new StringWriter();
		Rio.write(initialActivity, sw, RDFFormat.TURTLE);
		
		String string = sender.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(string)
				.with(jwtSender)
				.content(sw.toString())
			    .contentType("text/turtle")
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isCreated())
				.andReturn();
	
		// ReRead the Activity
		String activityLocation = Optional.ofNullable(mvcResult.getResponse().getHeader(HttpHeaders.LOCATION)).orElseThrow();		
		mvcResult = testHelperV2.getMockMvc().perform(get(activityLocation)
				.with(jwtSender)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();

		// Extract the object ID of the activity
		Model activity = Rio.parse(new StringReader(mvcResult.getResponse().getContentAsString()), RDFFormat.TURTLE);

		IRI objectIRI = Models.getPropertyIRI(activity, iri(activityLocation), Values.iri(VocAs.object)).orElseThrow();
		
		// Read the object to be sure, it is saved.
		mvcResult = testHelperV2.getMockMvc().perform(get(objectIRI.toString())
				.with(jwtSender)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();

		
		// read the senders outbox
		String outboxUrl = sender.getOutbox().asCollection().orElseThrow().getExternalBaseSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(outboxUrl)
				.with(jwtSender)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();
		String sendersOutbox = mvcResult.getResponse().getContentAsString();
		Model sendersOutboxModel = Rio.parse(new StringReader(sendersOutbox), RDFFormat.TURTLE);
		// extract the statements of the initial activity in the senders outbox
		Model filteredOutbox = sendersOutboxModel.filter(iri(activityLocation), null, null);
		
		// Test, that the activity with type create is available
		Set<IRI> propertyIRIs = Models.getPropertyIRIs(filteredOutbox, iri(activityLocation), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Create)), "propertyIRIs: " + propertyIRIs);
		
		
		// Read the receivers profile
		String receiverSubject = receiver.getSubject().asActor().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(receiverSubject)
				.with(jwtReceiver)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();

		// read the receivers inbox
		String inboxUrl = receiver.getInbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		mvcResult = testHelperV2.getMockMvc().perform(get(inboxUrl)
				.with(jwtReceiver)
				.header(HttpHeaders.ACCEPT, "text/turtle"))
				.andExpect(status().isOk())
				.andReturn();
		
		String contentAsString = mvcResult.getResponse().getContentAsString();
		Model inbox = Rio.parse(new StringReader(contentAsString), RDFFormat.TURTLE);

		// extract the statements of the initial activity in the receivers inbox
		Model filtered = inbox.filter(iri(activityLocation), null, null);
		
		// Test, that the activity with type create is available
		propertyIRIs = Models.getPropertyIRIs(filtered, iri(activityLocation), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Create)));
	}
}
