package org.linkedopenactors.rdfpub.uc;

import java.io.StringWriter;
import java.math.BigInteger;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import lombok.Data;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestActorProfile")
class TestObjectCreation {

	@Value("${app.rdfRepositoryHome}")
	private String rdfRepositoryHome;
	
	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;

	@Autowired
	private VocabContainer vocabContainer;
	
	@Data
	public class ActorId{
		public String id = "";
		public String sameAs = "";
		
		public ActorId(Actor actor) {
			this.id = actor.getSubject().toString();
			this.sameAs = actor.getSameAs().toString();
		}
	}
	
	@Test
	void testUpdateActorsProfile() throws Exception {

		String actorSubject = "http://example.com/someActor";
//		String updatedOauthAuthorizationEndpoint = "https://example.com/Oauth2";
		String activitySubject = "http://example.com/someActivity";
		IRI endpointsIri = Values.iri("http://example.com/endpoints");
		Model activity = new ModelBuilder()
				.subject(activitySubject)
					.add(RDF.TYPE, Values.iri(VocAs.Update))
					.add(Values.iri(VocAs.object), Values.iri(actorSubject))
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
					.add(Values.iri(VocAs.to), Values.iri(VocAs.Public))
				.build();

		Model updateActor = new ModelBuilder()
				.subject(Values.iri(actorSubject))
					.add(RDF.TYPE, vocabContainer.vocAs().actor())
					.add(Values.iri(VocAs.endpoints), endpointsIri)
					.add(Values.iri(VocAs.name), "updatedName")
				.subject(endpointsIri)
					.add(RDF.TYPE, vocabContainer.vocAs().endpoints())
//					.add(Values.iri(org.linkedopenactors.rdfpub.domain.AS.oauthAuthorizationEndpoint().toString()), Values.iri(updatedOauthAuthorizationEndpoint))					
				.build();

		activity.addAll(updateActor);
		
		StringWriter sw2 = new StringWriter();
		Rio.write(activity, sw2, RDFFormat.TURTLE);

		
		org.linkedopenactors.rdfpub.domain.ActivityPubObject activityPubObject = activityPubObjectFactory.create(sw2.toString());
//		ActivityPubObject activityPubObject = stringToActivityPubObject.convert("text/turtle", sw2.toString());
		
		org.linkedopenactors.rdfpub.domain.Activity activityObject = activityPubObject.asConvertable().asActivity();
		org.linkedopenactors.rdfpub.domain.ActivityPubObject obj = activityObject.resolveObject().stream().findFirst().orElseThrow();
		org.linkedopenactors.rdfpub.domain.Actor actorObj = obj.asConvertable().asActor();
		/*org.linkedopenactors.rdfpub.domain.Endpoints endpoints = */actorObj.resolveEndpoints().orElseThrow();
	}
}
