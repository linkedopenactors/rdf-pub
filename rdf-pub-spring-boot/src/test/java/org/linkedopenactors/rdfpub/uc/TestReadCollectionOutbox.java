package org.linkedopenactors.rdfpub.uc;

import static org.eclipse.rdf4j.model.util.Values.literal;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.TestingWebApplication;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocSchema;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = TestingWebApplication.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@ActiveProfiles({"signatureTest", "test"})
@TestPropertySource(properties = "app.rdfRepositoryHome=target/store/unittest/TestReadCollectionOutbox")
class TestReadCollectionOutbox {

	@Autowired
	private TestHelperV2 testHelperV2;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	@BeforeEach
	public void setup() throws Exception {
	
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");
	
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(new RDF4J(), prefixMapper);
	}
	
	@Test
	void testWithCreateActivity() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String preferredUsername = RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");
		
		IRI objectId = Values.iri(activityPubObjectIdBuilder.createResource(actor.getSubject()).getExternalSubject().getIRIString());
		IRI activityId = Values.iri(activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow()).getExternalSubject().getIRIString());

		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject(activityId)
					.add(RDF.TYPE, Values.iri(VocAs.Create))
					.add(Values.iri(VocAs.object), objectId)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.subject(objectId)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocSchema.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
				
		StringWriter sw = new StringWriter();
		Rio.write(activity, sw, RDFFormat.JSONLD);
		
		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(jwt)
				.content(sw.toString())
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);
		
		String urlOutbox = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		mvcResult =  testHelperV2.getMockMvc().perform(get(urlOutbox)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		StringReader sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model outbox = Rio.parse(sr,  RDFFormat.JSONLD);
		
		
		Set<IRI> propertyIRIs = Models.getPropertyIRIs(outbox, Values.iri(activityLocation), RDF.TYPE);
		assertTrue(propertyIRIs.contains(Values.iri(VocAs.Create)));
		Optional<IRI> objectIriOptional = Models.getPropertyIRI(outbox, Values.iri(activityLocation), Values.iri(VocAs.object));
		assertTrue(objectIriOptional.isPresent());
	}
	
	@Test
	void testSendingObjectWithoutActivity() throws Exception {
		
		String userId = UUID.randomUUID().toString();
		String preferredUsername = "dummyMax_" + RandomStringUtils.random(10, true, false);
		Actor actor = testHelperV2.createActor(userId, preferredUsername, "http://example.com");
		
		JwtRequestPostProcessor jwt = testHelperV2.jwt(userId, preferredUsername, "http://example.com");
	
		IRI asObjectIri = Values.iri(activityPubObjectIdBuilder.createResource(actor.getSubject()).getExternalSubject().getIRIString());

		Literal nameLiteral = literal("Max Mustermann");
		Model activity = new ModelBuilder()
				.subject(asObjectIri)
					.add(RDF.TYPE, Values.iri(VocAs.Object))
					.add(Values.iri(VocSchema.name), nameLiteral)
					.add(Values.iri(VocSchema.version), BigInteger.valueOf(1L))
					.add(Values.iri(VocSchema.identifier), UUID.randomUUID())
				.build();
		
		StringWriter sw = new StringWriter();
		Rio.write(activity, sw, RDFFormat.JSONLD);

		String url = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		MvcResult mvcResult = testHelperV2.getMockMvc().perform(post(url)
				.with(jwt)
				.content(sw.toString())
			    .contentType("application/ld+json")
				.header(HttpHeaders.ACCEPT, "application/ld+json"))				
				.andExpect(status().isCreated())
				.andReturn();

		String activityLocation = mvcResult.getResponse().getHeader(HttpHeaders.LOCATION);
		assertNotNull(activityLocation);
		
		String urlOutbox = actor.getOutbox().asCollection().orElseThrow().getExternalSubject().getIRIString();
		mvcResult =  testHelperV2.getMockMvc().perform(get(urlOutbox)
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "application/ld+json"))
				.andExpect(status().isOk())
//				.andDo(print())
				.andReturn();

		StringReader sr = new StringReader(mvcResult.getResponse().getContentAsString());
		Model outbox = Rio.parse(sr,  RDFFormat.JSONLD);
		
		Optional<IRI> objectIriOptional = Models.getPropertyIRI(outbox, Values.iri(activityLocation), Values.iri(VocAs.object));
		assertTrue(objectIriOptional.isPresent());
	}
}
