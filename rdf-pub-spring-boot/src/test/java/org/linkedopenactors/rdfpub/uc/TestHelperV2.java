package org.linkedopenactors.rdfpub.uc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Optional;

import org.linkedopenactors.rdfpub.client2024.HttpAdapterMvcMock;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.core.oidc.StandardClaimNames;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestHelperV2 {

	
	private WebApplicationContext context;

	private MockMvc mockMvc;

	private ActivityPubObjectFactory activityPubObjectFactory;
	
	public TestHelperV2(WebApplicationContext context, ActivityPubObjectFactory activityPubObjectFactory) {
		this.context = context;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.context)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
	}
	
	public org.linkedopenactors.rdfpub.client2024.HttpAdapter getHttpAdapter2024(String userId, String preferredUsername) {
		return new HttpAdapterMvcMock(this.mockMvc, userId, preferredUsername);
	}
	
	public Actor createActor(String userId, String preferredUsername, String issuer) throws Exception {
		JwtRequestPostProcessor jwt = jwt(userId, preferredUsername, issuer);
		
		MvcResult mvcResultActor = this.getMockMvc().perform(get("/api/whoami")
				.with(jwt)
				.header(HttpHeaders.ACCEPT, "text/turtle"))				
				.andExpect(status().isOk())
				.andReturn();		
		return toActor(mvcResultActor).orElseThrow();
	}	

	public Optional<Actor> toActor(MvcResult mvcResultActor) throws UnsupportedEncodingException, IOException {
		String turtleString = mvcResultActor.getResponse().getContentAsString();
		String rootHeader = mvcResultActor.getResponse().getHeader("root");
		Optional<Actor> actor = Optional.ofNullable(rootHeader)
				.map(root->activityPubObjectFactory.create(RdfFormat.TURTLE, turtleString, root))
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActor);
			
		if(actor.isPresent()) {
			log.debug("->rootHeader:" + rootHeader);
			log.debug("->actor:" + actor.get());
			log.debug("->mvcResultActor:" + mvcResultActor.getResponse().getContentAsString());
			String subjectString = actor.get().getSubject().asActor().orElseThrow().getExternalBaseSubject().toString();
			if(!rootHeader.startsWith(subjectString)) {
				throw new RuntimeException();
			}
			return actor;
		} else {
			throw new IllegalStateException("no root");			
		}
	}

	public Optional<ActivityPubObject> toActivityPubObject(MvcResult mvcResultActor) throws UnsupportedEncodingException, IOException {
		return toActivityPubObjectInternal(mvcResultActor);
	}

	public Optional<ActivityPubObject> toActivityPubObject(String subject, MvcResult mvcResultActor) throws UnsupportedEncodingException, IOException {
		return toActivityPubObjectInternal(subject, mvcResultActor);
	}

	public Optional<Activity> toActivity(String activityLocation, MvcResult mvcResult) throws UnsupportedEncodingException, IOException {
		return toActivityPubObjectInternal(activityLocation, mvcResult)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asActivity);
	}

	private Optional<ActivityPubObject> toActivityPubObjectInternal(String activityLocation, MvcResult mvcResult) throws UnsupportedEncodingException, IOException {
		String contentAsString = mvcResult.getResponse().getContentAsString();
		return Optional.ofNullable(activityPubObjectFactory.create(RdfFormat.JSONLD, contentAsString));
	}

	public Optional<Instance> toInstance(MvcResult mvcResultActor) throws UnsupportedEncodingException, IOException {
		return toActivityPubObjectInternal(mvcResultActor)
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asInstance);
	}

	private Optional<ActivityPubObject> toActivityPubObjectInternal(MvcResult mvcResultActor) throws UnsupportedEncodingException, IOException {
		String contentAsString = mvcResultActor.getResponse().getContentAsString();
		return Optional.ofNullable(activityPubObjectFactory.create(RdfFormat.JSONLD, contentAsString));
	}

	public MockMvc getMockMvc() {
		return mockMvc;
	}

	public JwtRequestPostProcessor jwt(String userId, String preferredUsername, String issuer) {
		return SecurityMockMvcRequestPostProcessors.jwt().jwt(j -> {					
			j.claim(StandardClaimNames.SUB, userId);
			j.claim(StandardClaimNames.PREFERRED_USERNAME, preferredUsername);
			j.claim("iss", issuer);
		});
	}
}
