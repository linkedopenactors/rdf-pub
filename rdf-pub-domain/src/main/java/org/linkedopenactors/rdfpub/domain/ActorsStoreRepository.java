package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;

public interface ActorsStoreRepository {
	Optional<ActorsStore> find(RdfPubObjectIdActor actorSubject);
	Optional<ActorsStore> find(Actor actor);
	
	/**
	 * also as:public is a receiver
	 * @param receiverId
	 * @return
	 */
	Optional<ReceiverStore> findReceiverStore(RdfPubBlankNodeOrIRI receiverId);
	
	PendingFollowerCache getPendingFollowerCache(RdfPubIRI owner, String loggableStoreName);
	PendingFollowerCache getPendingFollowingCache(RdfPubIRI owner, String loggableStoreName);
}
