package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.RDFTerm;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

public interface PublicActorStore extends ReceiverStore {
	
	OrderedCollectionPage getPersons(Integer pageSize, Integer startIndex);
	OrderedCollectionPage getPublicCollection(Integer startIndex, Integer pageSize);
	OrderedCollectionPage getEmptyCollection(Integer startIndex, Integer pageSize);
		
	/**
	 * Same as {@link #getActorsCollection()} but the subjects are resolved to objects.
	 * @return {@link Set} of {@link Actor}s
	 */
	Set<Actor> getActors();
	
	Set<RdfPubIRI> getPersonsCollection();
	Set<RdfPubIRI> getApplicationsCollection();
	Set<RdfPubIRI> getGroupsCollection();
	Set<RdfPubIRI> getOrganizationsCollection();
	Set<RdfPubIRI> getServicesCollection();
	Set<RdfPubIRI> getActorsCollection();
	
	void addToPersons(RdfPubIRI actorId);
	void addToApplications(RdfPubIRI actorId);
	
	Instance getInstanceActor();
	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject);
	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject, int deep);
	Optional<ActivityPubObject> findByTriple(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object);
	Set<ActivityPubObject> findByPredicateAndObject(RdfPubIRI predicate, RDFTerm object);
	
	void dump();
}
