package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

public interface ActorId //extends ActivityPubObjectId 
{
	/**
	 * @return The preferred username of the actor. this username has to be unique per rdf-pub instance.
	 */
	Optional<String> getPreferredUsername();
}
