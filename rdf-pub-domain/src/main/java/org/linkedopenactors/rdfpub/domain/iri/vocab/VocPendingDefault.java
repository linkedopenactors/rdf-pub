package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocPending;

public class VocPendingDefault implements VocPending {
	
	private RDF rdf;

	public VocPendingDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI pendingFollowing() {
		return new RdfPubIRIDefault(rdf.createIRI(pendingFollowing));
	}	

	public RdfPubIRI pendingFollowers() {
		return new RdfPubIRIDefault(rdf.createIRI(pendingFollowers));
	}
}
