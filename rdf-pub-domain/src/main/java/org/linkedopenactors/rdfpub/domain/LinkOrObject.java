package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.Graph;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.springframework.util.MimeType;

import com.neovisionaries.i18n.LanguageCode;

public interface LinkOrObject {
	
	Graph asGraph();
	
	Graph asExternalGraph();
	
	/**
	 * The id of the {@link ActivityPubObject}. Because an {@link ActivityPubObject}
	 * is a RDF Object, the id is called subject. See
	 * https://www.w3.org/TR/rdf12-concepts/#data-model
	 * 
	 * @return The id/subject of the {@link ActivityPubObject}. 
	 */
	RdfPubBlankNodeOrIRI getSubject();

	/**
	 * The RDF Type
	 * See Also: https://www.w3.org/TR/rdf12-schema/#ch_type
	 * 
	 * @param type
	 */
	void setType(Set<RdfPubIRI> type);
	
	/**
	 * The RDF Type
	 * See Also: https://www.w3.org/TR/rdf12-schema/#ch_type
	 * 
	 * @param type
	 */
	void addType(RdfPubBlankNodeOrIRI type);

	/**
	 * The RDF Type
	 * See Also: https://www.w3.org/TR/rdf12-schema/#ch_type
	 * 
	 * @return The type(s)
	 */
	Set<RdfPubIRI> getTypes();

	/**
	 * A simple, human-readable, plain-text name for the object. HTML markup MUST
	 * NOT be included. The name MAY be expressed using multiple language-tagged
	 * values.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
	 * 
	 * @param languageCode
	 * @return The name
	 */
	Optional<String> getName(LanguageCode languageCode);
	
	/**
	 * Same as {@link ActivityPubObject#getName()} but without or with default language.
	 * @return name
	 */
	Optional<String> getName();

	/**
	 * A simple, human-readable, plain-text name for the object. HTML markup MUST
	 * NOT be included. The name MAY be expressed using multiple language-tagged
	 * values.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
	 * 
	 * @param languageCode
	 * @param name
	 */
	void setName(LanguageCode languageCode, String name);
	
	/**
	 * Same as {@link ActivityPubObject#setName(String)} with default language.
	 * @param name
	 */
	void setName(String name);
	
 	/**
	 * When used on a Link, identifies the MIME media type of the referenced
	 * resource. When used on an Object, identifies the MIME media type of the value
	 * of the content property. If not specified, the content property is assumed to
	 * contain text/html content.
	 * @return {@link MimeType}
	 */
	 Optional<MimeType> getMediaType();
	 
	 /**
	  * @param isolated If true, only the references of the current subject are resolved. otherwhise all references of the underlaying graph.
	  * @return All iris, that are an object value and so potentially a reference to an object. 
	  */
	 Set<RdfPubBlankNodeOrIRI> getReferences(boolean isolated);
	 
	 Set<RdfPubBlankNodeOrIRI> getReferences(boolean isolated, boolean externalized);
}
