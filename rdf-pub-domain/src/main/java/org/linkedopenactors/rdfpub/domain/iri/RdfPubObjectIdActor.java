package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;
import java.util.UUID;

import org.linkedopenactors.rdfpub.domain.Actor;


public interface RdfPubObjectIdActor extends RdfPubObjectId {

	/**
	 * @return The {@link UUID} which uniquely identifies an {@link Actor}.
	 */
	String getIdentifier();

	Optional<UUID> getRevision();
	
	boolean isRevision();
}
