package org.linkedopenactors.rdfpub.domain.iri;

public interface RdfPubIRIFactory {
	RdfPubIRI createRdfPubIriIri(String iriAsString);
	RdfPubIRI asPublic();
}
