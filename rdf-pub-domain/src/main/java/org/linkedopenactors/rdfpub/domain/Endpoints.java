package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

public interface Endpoints extends ActivityPubObject {
	void setOauthAuthorizationEndpoint(RdfPubBlankNodeOrIRI oauthAuthorizationEndpoint);
	void setOauthTokenEndpoint(RdfPubBlankNodeOrIRI oauthTokenEndpoint);
	Optional<RdfPubBlankNodeOrIRI> getOauthAuthorizationEndpoint();
	Optional<RdfPubBlankNodeOrIRI> getOauthTokenEndpoint();
}
