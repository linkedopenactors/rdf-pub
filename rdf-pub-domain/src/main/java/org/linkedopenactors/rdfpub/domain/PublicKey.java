package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;


public interface PublicKey extends ActivityPubObject {
	void setOwner(RdfPubBlankNodeOrIRI owner);
	void setPublicKeyPem(String publicKey);
	Optional<java.security.PublicKey> getPublicKeyPem();
//	void setPublicKey(String publicKey);
}
