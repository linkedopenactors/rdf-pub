package org.linkedopenactors.rdfpub.domain.security;

public interface SignatureVerifier {
	VerificationResult verify();
}