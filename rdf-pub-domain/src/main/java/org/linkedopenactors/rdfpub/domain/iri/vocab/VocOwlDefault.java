package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocOwl;

public class VocOwlDefault implements VocOwl {

	private RDF rdf;

	public VocOwlDefault(RDF rdf) {
		this.rdf = rdf;
	}

	public RdfPubIRI sameAs() {
		return new RdfPubIRIDefault(rdf.createIRI(sameAs));
	}
}
