package org.linkedopenactors.rdfpub.domain;

import java.net.URL;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocRdfPub;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

public class InstanceDefault extends ActorDefault implements Instance {

	public InstanceDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}
	
	@Override
	public URL getDomain() {
		return null;//instanceProperties.getInstanceDomain();
	}

	@Override
	public RdfPubObjectIdCollection getCollectionInbox() {
		return getIri(vocLDP().inbox()).orElseThrow().asCollection().orElseThrow();
	}

	@Override
	public RdfPubObjectIdCollection getCollectionOutbox() {
		return getIri(vocAs().outbox()).orElseThrow().asCollection().orElseThrow();
	}

	@Override
	public RdfPubObjectIdCollection getCollectionPublic() {
		return getIri(vocAs().Public()).orElseThrow().asCollection().orElseThrow();
	}

	@Override
	public String getAppVersion() {
		return "n/a";//instanceProperties.getMavenVersion();
	}

	@Override
	public Long getProfileVersion() {
		return 0L;
	}

	@Override
	public RdfPubBlankNodeOrIRI getPublicSparqlEndpoint() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setType(RdfPubBlankNodeOrIRI id) {
		set(vocRdf().type(), id);
		
	}

	@Override
	public void setInboxCollection(RdfPubBlankNodeOrIRI inboxCollection) {
		set(vocLDP().inbox(), inboxCollection);		
	}

	@Override
	public void setOutboxCollection(RdfPubBlankNodeOrIRI outboxCollection) {
		set(vocAs().outbox(), outboxCollection);
	}

	@Override
	public void setPublicCollection(RdfPubBlankNodeOrIRI publicCollection) {
		set(vocAs().Public(), publicCollection);
	}

	@Override
	public void setInstanceVersion(String mavenVersion) {
		set(vocRdfPub().version(), rdf.createLiteral(mavenVersion));		
	}

	@Override
	public void setCommitId(String commitId) {
		set(vocRdfPub().commitId(), rdf.createLiteral(commitId));		
	}

	@Override
	public void setPublicSparqlEndpoint(RdfPubBlankNodeOrIRI sparqlPublic) {
		// TODO Auto-generated method stub
		
	}
}
