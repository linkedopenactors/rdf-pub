package org.linkedopenactors.rdfpub.domain.iri;

public interface VocProv {
	public static final String NS = "http://www.w3.org/ns/prov#";
	
	public static final String Entity = NS + "Entity";
	public static final String Activity = NS + "Activity";
	public static final String wasAssociatedWith = NS + "wasAssociatedWith";
	public static final String wasAttributedTo = NS + "wasAttributedTo";
	public static final String wasGeneratedBy = NS + "wasGeneratedBy";
	public static final String wasRevisionOf = NS + "wasRevisionOf";
	
	RdfPubIRI Entity();

	RdfPubIRI Activity();
	
	RdfPubIRI wasAssociatedWith();
	
	RdfPubIRI wasAttributedTo();
	
	RdfPubIRI wasGeneratedBy();
	
	RdfPubIRI wasRevisionOf();
}
