package org.linkedopenactors.rdfpub.domain.security;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerificationResult {
	private boolean succeeded;
	private String failureMessage;
	
	public static final VerificationResult ok() {
		return new VerificationResult(true, null);
	}
	
	public static final VerificationResult failed(String msg) {
		return new VerificationResult(false, msg);
	}

	public boolean succeeded() {
		return succeeded;
	}

}
