package org.linkedopenactors.rdfpub.domain;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class NativePublicKey {

	public static void main(String[] args) {
		String test2 = "-----BEGIN PUBLIC KEY-----\n"
				+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtygsnxmaapJoQo/lkHZl\n"
				+ "Qu9YuDYZI5sHzFvJhcB+uPG/Gjo5OkHbfPdiH4ZQnoLj3QfrlO21MQ+BNwwJmxMw\n"
				+ "83KHgdf/co04Wo3Un5PKbMC98v0XOaWW+MuDTqA5QGt1MqT753n7VOJGZ0mbERjj\n"
				+ "cBZ08djZFfwHTutjR3NSOx/GIGXlu9IeIa7UtJE9PfEknHcWMuFU6kPRdU/CMiMZ\n"
				+ "agL8OGDySJ+Onag/iXrVCj8x+a84/TeTB05k+/8FI4Pgw4KcqZ4iQRzqlbN22lPi\n"
				+ "KYTDwBVcx7vFwgj9H9vGeO1aq8GojQQoQOCuUU2sMQRNj5+JCD0n9558NsiV9zl/\n"
				+ "GQIDAQAB\n"
				+ "-----END PUBLIC KEY-----";
				/*RSAPublicKey rsa = */readX509PublicKey(test2);
	}
		 
	 public static RSAPublicKey readX509PublicKey(String key)  {

		    try {
				String publicKeyPEM = key
				  .replace("-----BEGIN PUBLIC KEY-----", "")
				  .replaceAll(System.lineSeparator(), "")
				  .replace("-----END PUBLIC KEY-----", "");

				byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
				return (RSAPublicKey) keyFactory.generatePublic(keySpec);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
				throw new IllegalStateException(e);
			}
		}	 
}
