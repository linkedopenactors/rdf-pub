package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.BlankNode;

public class RdfPubBlankNodeDefault {

    private final BlankNode blankNode;

    public RdfPubBlankNodeDefault(final BlankNode blankNodeParam) {
		blankNode = blankNodeParam;
    }

	@Override
	public int hashCode() {
		return blankNode.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return blankNode.equals(obj);
	}

	@Override
	public String toString() {
		return blankNode.toString();
	}
}
