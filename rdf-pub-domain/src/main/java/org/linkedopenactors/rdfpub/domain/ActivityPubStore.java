package org.linkedopenactors.rdfpub.domain;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdCollection;

public interface ActivityPubStore {

	void add(ActivityPubObject ao1, String logMsg);
	void add(Set<Triple> newTriples, String logMsg);
	void update(ActivityPubObject ao1, String logMsg);
	
	void addCollectionItem(RdfPubIRI collection, RdfPubIRI item, String logMsg);
	void removeCollectionItem(RdfPubIRI collection, RdfPubIRI item, String logMsg);
	
	List<RdfPubIRI> getCollection(RdfPubIRI collection);
	boolean existsCollection(RdfPubIRI collection);
	OrderedCollectionPage getCollection(Collection<RdfPubIRI> irisToResolve, RdfPubIRI namedGraphToUse, Integer pageSize, Integer startIndex, Integer deep);
	void createCollection(RdfPubIRI collection, String humanReadableName);

	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject);
	
	/**
	 * Searches for the {@link ActivityPubObject} with the passed subject. 
	 * @param subject
	 * @param deep up to which hierarchy level should object references be resolved ?
	 * @return found {@link ActivityPubObject}.
	 */
	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI subject, int deep);
	
	/**
	 * Searches for the {@link ActivityPubObject} that matches the passed triple.
	 * @param subject
	 * @param predicate
	 * @param object
	 * @return found {@link ActivityPubObject}.
	 */
	Optional<ActivityPubObject> findByTriple(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object);
	
	/**
	 * Searches for subjects that matches the passed triple.
	 * @param subject
	 * @param predicate
	 * @param object
	 * @return Subjects that matches the passed triple.
	 */
	Set<RdfPubIRI> findIris(RdfPubBlankNodeOrIRI subject, RdfPubIRI predicate, RDFTerm object);
	
	/**
	 * Finds all subjects that match the passed predicate/object.
	 * @param predicate
	 * @param object
	 * @return Set of {@link ActivityPubObject}s that contains the predicate/object. 
	 * NOTE the {@link ActivityPubObject}s have only one predicate!    
	 */
	Set<ActivityPubObject> findByPredicateAndObject(RdfPubIRI predicate, RDFTerm object);
	
	/**
	 * Find object with the passed subjects in the actors store.
	 * @param subjectsParam Subjects to search for.
	 * @return Found {@link ActivityPubObject}s 
	 */
	Set<ActivityPubObject> findAll(Set<RdfPubIRI> subjectsParam);
	
	/**
	 * Find object with the passed subjects in the actors store and resolve dependencies up to the passed depth.
	 * @param subjectsParam Subjects to search for.
	 * @param deep up to which hierarchy level should object references be resolved ?
	 * @return Found {@link ActivityPubObject}s 
	 */
	Set<ActivityPubObject> findAll(Set<RdfPubIRI> subjectsParam, int deep);

	Optional<ActivityPubObject> findLatestRevision(RdfPubIRI pointerSubject);
	
	/**
	 * ATTENTION!! This reads the whole grap of the actor !
		 * @param subject
	 * @return The found {@link ActivityPubObject}
	 */
	Optional<ActivityPubObject> findWholeGraph(RdfPubIRI subject);

	RdfPubIRI getOwner();
	
	/**
	 * Gets all triples of the passed graph.
	 * @param graphName
	 * @return All triples of the passed graph.
	 */
	Graph dump(RdfPubIRI graphName);
	
	/**
	 * Dumps the whole store (including all graphs) to System.out
	 */
	void dump();
	
	/**
	 * @return {@link Set} of all known graphNames.
	 */
	Set<BlankNodeOrIRI> getGraphNames();

	/**
	 * @return number of all triples in the store.
	 */
	long countTriples();
	
	/**
	 * @param graphName
	 * @return number of all triples in the passed graph.
	 */
	long countTriples(BlankNodeOrIRI graphName);
	
	/**
	 * Removes the object with the passed subject from the this store. 
	 * @param objectToDelete
	 * @param logMsg
	 */
	void remove(RdfPubBlankNodeOrIRI objectToDelete, String logMsg);
	
	/**
	 * Removes the objects with the passed subjects from the this store.
	 * @param objectsToDelete
	 * @param logMsg
	 */
	void remove(Set<RdfPubIRI> objectsToDelete, String logMsg);
	String queryTuple(String query);
}