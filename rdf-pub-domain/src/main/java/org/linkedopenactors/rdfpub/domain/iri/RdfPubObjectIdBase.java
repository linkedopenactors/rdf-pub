package org.linkedopenactors.rdfpub.domain.iri;

import java.util.UUID;

import org.apache.commons.rdf.api.RDF;

abstract class RdfPubObjectIdBase implements RdfPubObjectId {
	
	private static final String ACTOR_PREFIX = "/";
	protected RDF rdf;
	protected String internalUrnString;
	protected String instanceDomainString;
	
	protected String actorIdentifier;
	protected String instanceActorIdentifier;
	protected RdfPubIRIFactory rdfPubIRIFactory;
		
	
	public RdfPubObjectIdBase(RdfPubObjectIdBase id) {
		this(id.rdfPubIRIFactory, id.rdf, id.internalUrnString, id.instanceDomainString, id.instanceActorIdentifier, id.actorIdentifier);
	}
	
	@Override
	public boolean isBlankNode() {
		// TODO !!! das geht besser, oder ?
		try {
			rdf.createIRI(getIRIString());
			return false;
		} catch (IllegalArgumentException e) {
			rdf.createBlankNode(getIRIString());
			return true;
		}
	}

	/**
	 * @param vocabContainerParam
	 * @param rdf
	 * @param internalUrnString The internal instance independent urn prefix.
	 * @param instanceDomainString The URL which routes to this instance.
	 * @param instanceActorIdentifier
	 * @param actorIdentifier The unique id of the actor. That is not the subject, but a part of the subject!
	 */
	public RdfPubObjectIdBase(RdfPubIRIFactory vocabContainerParam, RDF rdf, String internalUrnString, String instanceDomainString,
			String instanceActorIdentifier, String actorIdentifier) {
		this.rdfPubIRIFactory = vocabContainerParam;
		this.rdf = rdf;
		this.internalUrnString = internalUrnString;
		this.instanceDomainString = instanceDomainString;
		this.instanceActorIdentifier = instanceActorIdentifier;
		this.actorIdentifier = actorIdentifier;
		
		// TODO is this still relevant?
		if(!instanceActorIdentifier.equals(actorIdentifier)) {
			try {
				UUID.fromString(actorIdentifier);
			} catch (Exception e) {
				throw new IllegalArgumentException("internal actorIdentifier is no UUID: " + actorIdentifier, e);
			}
		}
	}

	protected RdfPubObjectIdActor getBaseActor() {
		return new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnString, instanceDomainString, instanceActorIdentifier, actorIdentifier);			
	}
	
	public void setIdentifier(String actorIdentifier) {
		this.actorIdentifier = actorIdentifier;
	}
	
	@Override
	public String getIdentifier() {
		return actorIdentifier;
	}
	
	@Override
	public String getIRIString() {
//		return resolveExternal(getSubject()).getIRIString();
		// TODO ok, to use external here ??
		return getSubject().getIRIString();
	}

	@Override
	public String ntriplesString() {
		return getSubject().ntriplesString();
	}

	protected String getBaseSubjectString() {
		return internalUrnString + PREFIX_TOKEN + ACTOR_PREFIX + actorIdentifier.toString();
	}
	
	@Override
	public RdfPubIRI getExternalSubject() {
		return resolveExternal(getSubject());
	}

	@Override
	public RdfPubBlankNodeOrIRI getExternalBaseSubject() {
		return resolveExternal(getBaseSubject());
	}

	protected RdfPubIRI resolveExternal(RdfPubBlankNodeOrIRI internalIriParam) {
		String internalAsString = internalIriParam.toString();
		String replace = resolveExternal(internalAsString);
		return rdfPubIRIFactory.createRdfPubIriIri(replace);
	}

	private String resolveExternal(String internalAsString) {
		instanceDomainString = instanceDomainString.endsWith("/") ? instanceDomainString : instanceDomainString + "/";
		String result = internalAsString.replace(internalUrnString, instanceDomainString);
		return result;
	}

	@Override
	public String getPath() {
		String result = getIRIString().replace(internalUrnString, "");
		return result;
	}

	@Override
	public String toString() {
		return getIRIString();
	}

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof RdfPubObjectId)) {
            return false;
        }
        final RdfPubObjectId other = (RdfPubObjectId) obj;
        boolean equals = getIRIString().equals(other.getIRIString());
		return equals;
    }

    @Override
    public int hashCode() {
        return getIRIString().hashCode();
    }
}
