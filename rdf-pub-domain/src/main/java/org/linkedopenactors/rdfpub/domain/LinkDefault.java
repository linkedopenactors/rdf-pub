package org.linkedopenactors.rdfpub.domain;

import java.net.URI;
import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import com.neovisionaries.i18n.LanguageCode;

public class LinkDefault extends LinkOrObjectDefault implements Link {

	public LinkDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}

	@Override
	public URI getHref() {
		return getIri(vocAs().href())
				.map(RdfPubBlankNodeOrIRI::getIRIString)
				.map(URI::create)
				.orElseThrow();
	}

	@Override
	public void setHref(URI href) {
		setString(vocAs().href(), href.toString());		
	}

	@Override
	public Set<String> getRel() {
		return getStrings(vocAs().rel());
	}

	@Override
	public Optional<LanguageCode> getHreflang() {
		return getString(vocAs().hreflang()).map(LanguageCode::valueOf);
	}

	@Override
	public Optional<Long> getHeight() {
		return getLong(vocAs().height());
	}

	@Override
	public Optional<Long> getWidth() {
		return getLong(vocAs().width());
	}

	@Override
	public Set<RdfPubBlankNodeOrIRI> getPreview() {
		return getIris(vocAs().preview());
	}
}
