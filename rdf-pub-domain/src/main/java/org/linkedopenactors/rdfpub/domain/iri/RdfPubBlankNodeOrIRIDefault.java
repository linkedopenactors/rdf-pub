package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.BlankNode;
import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;

class RdfPubBlankNodeOrIRIDefault implements RdfPubBlankNodeOrIRI, BlankNodeOrIRI {
	
    private final BlankNodeOrIRI iri;

    public RdfPubBlankNodeOrIRIDefault(final IRI iriParam) {
    	this.iri = iriParam;
    }

    public RdfPubBlankNodeOrIRIDefault(RDF rdf, final String iriParam) {
		this.iri = rdf.createIRI(iriParam);
    }
    
//    public RdfPubIRIDefault(final String iri) {
//        // NOTE: We don't keep the URI as it uses outdated RFC2396 and will get
//        // some IDNs wrong
//        this.iri = iri;
//
//        // should throw IllegalArgumentException on most illegal IRIs
//        if(!isBlankNode()) { // TODO somehow we need to handle blanknodes in it's own type!
//        	URI.create(iri);
//        }
//    }

	public boolean isBlankNode() {
		return iri instanceof BlankNode;
	}

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof IRI)) {
            return false;
        }
        final IRI other = (IRI) obj;
        boolean equals = getIRIString().equals(other.getIRIString());
		return equals;
    }

    @Override
    public String getIRIString() {    	
    	if(iri instanceof IRI) {
    		return ((IRI)iri).getIRIString();
    	} else {
    		return iri.toString();
    	}
    }

    @Override
    public int hashCode() {
        return iri.hashCode();
    }

    @Override
    public String ntriplesString() {
        return "<" + getIRIString() + ">";
    }

    @Override
    public String toString() {
        return getIRIString();
    }
}
