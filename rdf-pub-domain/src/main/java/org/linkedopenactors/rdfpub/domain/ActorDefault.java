package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.Literal;
import org.apache.commons.rdf.api.Triple;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActorDefault extends ActivityPubObjectDefault implements Actor {

	public ActorDefault(VocabContainer vocabContainer, DomainObjectInitialazationData data) {
		super(vocabContainer, data);
	}

	@Override
	public RdfPubBlankNodeOrIRI getOutbox() {
		Set<RdfPubBlankNodeOrIRI> iris = getIris(vocAs().outbox());
		return iris.stream().findFirst().orElseThrow();
	}

	@Override
	public void setOutbox(RdfPubBlankNodeOrIRI outboxIri) {
		set(vocAs().outbox(), Set.of(outboxIri));
	}

	@Override
	public RdfPubBlankNodeOrIRI getInbox() {
		Optional<RdfPubBlankNodeOrIRI> iriOpt = getIris(vocLDP().inbox()).stream().findFirst();
		if(iriOpt.isEmpty()) {
			log.error("unable to determine inbox of: \n" + this.toString() + " subject: " + this.getSubject());
			throw new IllegalStateException("unable to determine inbox");
		}		
		return iriOpt.orElseThrow();
	}
	
	@Override
	public void setInbox(RdfPubBlankNodeOrIRI inboxIri) {
		set(vocLDP().inbox(), Set.of(inboxIri));		
	}
	
	@Override
	public RdfPubBlankNodeOrIRI getPendingFollowers() {
		return getIris(vocPending().pendingFollowers()).stream().findFirst().orElseThrow();
	}

	@Override
	public void setPendingFollowers(RdfPubBlankNodeOrIRI pendingFollowersIri) {
		set(vocPending().pendingFollowers(), Set.of(pendingFollowersIri));
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getPendingFollowing() {
		return getIris(vocPending().pendingFollowing()).stream().findFirst();
	}

	@Override
	public void setPendingFollowing(RdfPubBlankNodeOrIRI pendingFollowingIri) {
		set(vocPending().pendingFollowing(), Set.of(pendingFollowingIri));
	}

	@Override
	public RdfPubBlankNodeOrIRI getFollowing() {
		return getIris(vocAs().following()).stream().findFirst().orElseThrow();
	}

	@Override
	public void setFollowing(RdfPubBlankNodeOrIRI followingIri) {
		set(vocAs().following(), Set.of(followingIri));
	}

	@Override
	public RdfPubBlankNodeOrIRI getFollowers() {
		return getIris(vocAs().followers()).stream().findFirst().orElseThrow();
	}

	@Override
	public void setFollowers(RdfPubBlankNodeOrIRI followersIri) {
		set(vocAs().followers(), Set.of(followersIri));
	}

	@Override
	public RdfPubBlankNodeOrIRI getLiked() {
		return getIris(vocAs().liked()).stream().findFirst().orElseThrow();
	}

	@Override
	public void setLiked(RdfPubBlankNodeOrIRI likedIri) {
		set(vocAs().liked(), Set.of(likedIri));
	}

	@Override
	public Optional<String> getOauth2IssuerUserId() {
		return asGraph().stream(getSubjectToSearchGraph(), vocRdfPub().oauth2IssuerUserId(), null).findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm);
	}

	@Override
	public void setOauth2IssuerUserId(String userId) {
		set(vocRdfPub().oauth2IssuerUserId(), rdf.createLiteral(userId));		
	}

	@Override
	public Optional<String> getOauth2Issuer() {
		return asGraph().stream(getSubjectToSearchGraph(), vocRdfPub().oauth2Issuer(), null).findFirst()
				.map(Triple::getObject)
				.map(Literal.class::cast)
				.map(Literal::getLexicalForm);
	}

	@Override
	public void setOauth2Issuer(String issuer) {
		set(vocRdfPub().oauth2Issuer(), rdf.createLiteral(issuer));		
	}

	@Override
	public void setPreferredUsername(String preferredUsername) {
		set(vocAs().preferredUsername(), rdf.createLiteral(preferredUsername));
	}

	@Override
	public Optional<String> getPreferredUsername() {
		return getString(vocAs().preferredUsername());
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getEndpoints() {
		return getIri(vocAs().endpoints());
	}

	@Override
	public void setEndpoints(Endpoints endpoints) {		
		set(vocAs().endpoints(), endpoints.getSubject());		
		getEndpoints().ifPresent(this::remove);
		add(endpoints.asGraph());
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getPublicKey() {
		return getIri(vocSecurity().publicKey());
	}

	@Override
	public Optional<RdfPubBlankNodeOrIRI> getPublicKey(boolean resolveIris) {
		Optional<RdfPubBlankNodeOrIRI> externalScheme = getPublicKey()
				.flatMap(RdfPubBlankNodeOrIRI::asRdfPubObjectId)
				.map(o->o.getExternalSubject());
		return externalScheme;
	}

	@Override
	public Optional<PublicKey> resolvePublicKey() {
		return getPublicKey()
				.flatMap(i->asConvertable().resolve(i))
				.map(ActivityPubObject::asConvertable)
				.map(ActivityPubObjectConvertable::asPublicKey);
	}
	
	@Override
	public Optional<Endpoints> resolveEndpoints() {
		return getEndpoints()
			.flatMap(i->asConvertable().resolve(i))
			.map(ActivityPubObject::asConvertable)
			.map(ActivityPubObjectConvertable::asEndpoints);
	}

	@Override
	public void setPublicKey(PublicKey publicKey) {
		set(vocSecurity().publicKey(), publicKey.getSubject());		
		getPublicKey().ifPresent(this::remove);
		add(publicKey.asGraph());
	}
}
