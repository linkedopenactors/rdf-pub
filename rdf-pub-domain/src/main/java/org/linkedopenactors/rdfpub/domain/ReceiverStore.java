package org.linkedopenactors.rdfpub.domain;

import java.util.List;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

/**
 * The {@link ActorsStore} from the perspective of incoming S2S activities.
 */
public interface ReceiverStore {
	RdfPubBlankNodeOrIRI addToInbox(Activity activity, String logMsg);
	void addFollower(RdfPubIRI follower);
	List<RdfPubIRI> getFollowers();
	List<RdfPubIRI> getFollowings();
	Optional<ActivityPubObject> findBySubject(RdfPubBlankNodeOrIRI rdfPubIRI);
	void removeFollower(RdfPubIRI follower);
}
