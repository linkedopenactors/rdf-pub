package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;

public interface ActivityPubObjectConvertable {

	Activity asActivity();

	Actor asActor();
	
	Tombstone asTombstone();
	
	/**
	 * An {@link ActivityPubObject} wrappes a graph and is identified by it's
	 * subject. This resolve method uses the same underlaying graph, but switches
	 * the subject. This is meaningfull, if the {@link ActivityPubObject} is an
	 * {@link Activity} and you want to resolve the object of the {@link Activity}.
	 * 
	 * @param subject The subject of the {@link ActivityPubObject} to be resolved. 
	 * @return The resolved {@link ActivityPubObject}. TODO make this Optional and return empty in case there are no triples for the subkject !!!
	 */
	Optional<ActivityPubObject> resolve(RdfPubBlankNodeOrIRI subject);
//	
//	Note asNote();
//
//	Endpoints asEndpoints();
//	
//	Instance asInstance();

	/**
	 * Removes all Triples from the underlaying graph that are no Triples of {@link ActivityPubObject#getSubject()}. 
	 * @return An {@link ActivityPubObject} that's graph only contain itÄs own triples.
	 */
	ActivityPubObject isolated();

	ActivityPubObject asRevison();

	PublicKey asPublicKey();
	
	Endpoints asEndpoints();

	Instance asInstance();

	OrderedCollectionPage asOrderedCollectionPage();

	Link asLink();
	
//	String asJsonLdString();

//	PublicKey asPublicKey();

}