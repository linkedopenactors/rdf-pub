package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocLDP;

public class VocLDPDefault implements VocLDP {

	private RDF rdf;

	public VocLDPDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI inbox() {
		return new RdfPubIRIDefault(rdf.createIRI(inbox));
	}
}
