package org.linkedopenactors.rdfpub.domain.iri;

public interface VocSecurity {
	public static final String NS = "https://w3id.org/security#";
	public static final String owner = NS + "owner";
	public static final String publicKeyPem = NS + "publicKeyPem";
	public static final String publicKey = NS + "publicKey";
	
	RdfPubIRI owner();

	RdfPubIRI publicKeyPem();

	RdfPubIRI publicKey();
}
