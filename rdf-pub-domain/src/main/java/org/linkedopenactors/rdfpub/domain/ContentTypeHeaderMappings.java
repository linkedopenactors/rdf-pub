package org.linkedopenactors.rdfpub.domain;

import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;

public interface ContentTypeHeaderMappings {
	RdfFormat determineRdfFormat(String requestedAcceptHeader);
}
