package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.springframework.stereotype.Component;

@Component
class ActivityPubObjectIdBuilderDefault implements ActivityPubObjectIdBuilder {
	
	private RDF rdf;
	private String internalUrnStringWithEndingSlash;
	private String instanceDomainString;
	private RdfPubIRIFactory rdfPubIRIFactory;
	private RdfPubIRI asPublic;
	
	/**
	 * @param rdfPubIRIFactoryParam
	 * @param asPublic
	 * @param rdf
	 * @param prefixMapper 
	 */
	public ActivityPubObjectIdBuilderDefault(RdfPubIRIFactory rdfPubIRIFactoryParam, RDF rdf, PrefixMapper prefixMapper) {
		this.rdfPubIRIFactory = rdfPubIRIFactoryParam;
		this.asPublic = rdfPubIRIFactoryParam.asPublic();
		this.rdf = rdf;		
		this.internalUrnStringWithEndingSlash = Optional.ofNullable(prefixMapper.getInternaleIriPrefix()).orElseThrow();		
		this.instanceDomainString = prefixMapper.getExternalIriPrefix();
		if(!this.internalUrnStringWithEndingSlash.endsWith(":")) {
			throw new IllegalArgumentException("internalUrnString has to end with ':', but is: " + internalUrnStringWithEndingSlash);
		}		
	}

	@Override
	public RdfPubObjectIdActor createActor() {
		return new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, UUID.randomUUID().toString());
	}

	@Override
	public RdfPubObjectIdActor createActor(RdfPubBlankNodeOrIRI iri) {
		return (RdfPubObjectIdActor)createFromUrl(iri);
	}

	@Override
	public RdfPubObjectIdActor createActor(String actorIdentifier) {
		return new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorIdentifier);
	}

	@Override
	public RdfPubObjectIdActor createActor(RdfPubObjectIdActor actor) {
		RdfPubObjectIdActorDefault res = new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actor.getIdentifier());
		res.setRevision(UUID.randomUUID());
		return res;
	}

	@Override
	public RdfPubObjectIdActor createActorRevision(String actorIdentifier, UUID actorRevision) {
		RdfPubObjectIdActorDefault id = new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorIdentifier);
		id.setRevision(actorRevision);
		return id;
	}

	@Override
	public RdfPubObjectIdResource createResource(RdfPubBlankNodeOrIRI iri) {
		if(iri.isActor()) {
			return createResource(iri.asActor().orElseThrow());
		}
		return (RdfPubObjectIdResource)createFromUrl(iri);
	}

	@Override
	public RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier) {
		return new RdfPubObjectIdResourceDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorIdentifier.getIdentifier());
	}
	
	@Override
	public RdfPubObjectIdResource createResource(RdfPubObjectIdResource resource) {
		RdfPubObjectIdResourceDefault res = new RdfPubObjectIdResourceDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, resource.getIdentifier());
		res.setResourceIdentifier(resource.getResourceIdentifier());
		res.setRevision(UUID.randomUUID());
		return res;
	}

	@Override
	public RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier, UUID resourceIdentifier) {
		return new RdfPubObjectIdResourceDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorIdentifier.getIdentifier(), resourceIdentifier);
	}
	
	@Override
	public RdfPubObjectIdResource createResource(RdfPubObjectIdActor actorIdentifier, UUID resourceIdentifier, UUID resourceRevision) {
		RdfPubObjectIdResourceDefault res = new RdfPubObjectIdResourceDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorIdentifier.getIdentifier());
		res.setRevision(resourceRevision);
		return res;
	}
	
	@Override
	public RdfPubObjectIdActivity createActivity(RdfPubBlankNodeOrIRI actorId) {
		return createActivity(actorId.asActor().orElseThrow());
	}

	@Override
	public RdfPubObjectIdActivity createActivity(RdfPubObjectId actorId) {
		return new RdfPubObjectIdActivityDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actorId.getIdentifier(), UUID.randomUUID());
	}

	@Override
	public RdfPubObjectIdActivity createActivity(RdfPubObjectIdActivity activity) {
		RdfPubObjectIdActivityDefault res = new RdfPubObjectIdActivityDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, activity.getIdentifier(), activity.getActivityIdentifier());
		res.setRevision(UUID.randomUUID());
		return res;
	}

	@Override
	public RdfPubIRI createFromUrl(String subject) {
		if(isInternal(subject)) {
			if(subject.contains(RdfPubObjectId.PREFIX_TOKEN)||subject.equals(asPublic.getIRIString())) {
				return createRdfPubId(subject);
			} else {
				if(subject.startsWith(instanceDomainString)) {
					subject = subject.replace(instanceDomainString, "");
				} else if(subject.startsWith(internalUrnStringWithEndingSlash)) {
					subject = subject.replace(internalUrnStringWithEndingSlash, instanceDomainString + "/");
				}				
				return rdfPubIRIFactory.createRdfPubIriIri(subject);
			}
		} else {			
			return rdfPubIRIFactory.createRdfPubIriIri(subject);
		}
	}

	private boolean isInternal(String subject) {
		return subject.startsWith(instanceDomainString) 
				|| subject.startsWith(internalUrnStringWithEndingSlash)
				|| subject.equals(asPublic.getIRIString());
	}

	@Override
	public RdfPubBlankNodeOrIRI createFromUrl(RdfPubBlankNodeOrIRI subject) {
		return createFromUrl(subject.getIRIString());
	}

	@Override
	public RdfPubBlankNodeOrIRI create(RdfPubBlankNodeOrIRI subject) {
		return createFromUrl(subject);
	}
	
	@Override
	public RdfPubIRI create(String path) {
		if(path.startsWith("http") || path.startsWith("urn:")) {
			throw new IllegalStateException("This is only for generating RdfPubIRI for this instance. Please pass onle the path without scheme/domain. forbidden: " + path);
		}
		path = path.startsWith("/") ? path : "/" + path;
//		return rdfPubIRIFactory.createRdfPubIriIri(instanceDomainString + path);
		return new RdfPubIRIDefault(rdf.createIRI(instanceDomainString + path));
	}

	@Override
	public RdfPubObjectIdCollection createCollection(RdfPubObjectId actor, String collectionId) {
		return new RdfPubObjectIdCollectionDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, actor.getIdentifier(), collectionId);
	}
	
	
	@Override
	public RdfPubObjectIdCollection createPublicCollection() {
		return new RdfPubObjectIdPublicCollection(rdfPubIRIFactory, asPublic, rdf, internalUrnStringWithEndingSlash, instanceDomainString, Instance.INSTANCE_ACTOR_NAME, Instance.INSTANCE_ACTOR_NAME);
	}

	@Override
	public RdfPubObjectIdCollection createCollection(RdfPubBlankNodeOrIRI actorId, String collectionId) {
		return createCollection(actorId.asActor().orElseThrow(), collectionId);
	}

//	@Override
//	public RdfPubIriIri createOauthAuthorizationEndpoint(RdfPubIRI actorId) {
//		endpoints.setOauthAuthorizationEndpoint(activityPubObjectIdBuilder.create("/oauth/oauthAuthorizationEndpoint")); // TODO hardcoded url !!!
//		return null;
//	}
//
//	@Override
//	public RdfPubIriIri createOauthTokenEndpoint(RdfPubIRI actorId) {
//		createResource(actorId)
//		endpoints.setOauthTokenEndpoint(activityPubObjectIdBuilder.create("/oauth/oauthTokenEndpoint")); // TODO hardcoded url !!!
//		return null;
//	}

	private String extractActorIdentifier(String url) {
		if(url.startsWith(instanceDomainString)) {
			String domain = instanceDomainString.endsWith("/") ? instanceDomainString : instanceDomainString + "/"; 
			url = url.replace(domain, internalUrnStringWithEndingSlash);
		}
		if(!url.startsWith(internalUrnStringWithEndingSlash)) {
			throw new IllegalArgumentException("url is not an internal url: " + url);
		}
		List<String> pathTokens = toPathTokens(url.replace(internalUrnStringWithEndingSlash, ""));
		return pathTokens.get(1);
	}
	
	private RdfPubObjectId createRdfPubId(String url) {
		if(url.startsWith(instanceDomainString)) {
			String domain = instanceDomainString.endsWith("/") ? instanceDomainString : instanceDomainString + "/"; 
			url = url.replace(domain, internalUrnStringWithEndingSlash);
		}
		if(!url.startsWith(internalUrnStringWithEndingSlash) && !isCollection(url)) {
			throw new IllegalArgumentException("url is not an internal url: " + url);
		}
		if(isCollection(url)) {
			return parseCollection(url);
		}
		if(isResource(url)) {
			return parseResource(url);
		}
		if(isActivity(url)) {
			return parseActivity(url);
		}
		if(isCache(url)) {
			return parseCache(url);
		}
		return parseActor(url);
	}
	
	private RdfPubObjectIdResource parseResource(String url) {
		List<String> pathTokens = getTokensOfRightPart(url, RdfPubObjectId.RESOURCE_TOKEN);
		UUID fromString;
		String first = pathTokens.getFirst();
		try {			
			fromString = UUID.fromString(first);
		} catch (Exception e) {
			throw new IllegalArgumentException("unable to parse UUID: " + first, e);
		}
		RdfPubObjectIdResourceDefault res = new RdfPubObjectIdResourceDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash,
				instanceDomainString, Instance.INSTANCE_ACTOR_NAME, extractActorIdentifier(url),
				fromString);
		if(pathTokens.size()==2) {
			res.setRevision(UUID.fromString(pathTokens.get(1)));
		}
		return res;
	}

	private RdfPubObjectIdCollection parseCollection(String url) {
		RdfPubObjectIdCollection res;
		if(asPublic.getIRIString().equals(url)) {
			res = createRdfPubObjectIdPublicCollection();
		} else  {
			List<String> pathTokens = getTokensOfRightPart(url, RdfPubObjectId.COLLECTION_TOKEN);
			res = new RdfPubObjectIdCollectionDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash,
					instanceDomainString, Instance.INSTANCE_ACTOR_NAME, extractActorIdentifier(url),
					pathTokens.getFirst());
		}
		return res;
	}

	public RdfPubObjectIdCollection createRdfPubObjectIdPublicCollection() {
		return new RdfPubObjectIdPublicCollection(rdfPubIRIFactory, asPublic, rdf, internalUrnStringWithEndingSlash, instanceDomainString,
				Instance.INSTANCE_ACTOR_NAME, Instance.INSTANCE_ACTOR_NAME);
	}

	private RdfPubObjectIdActivity parseActivity(String url) {
		List<String> pathTokens = getTokensOfRightPart(url, RdfPubObjectId.ACTIVITY_TOKEN);
		RdfPubObjectIdActivityDefault res = new RdfPubObjectIdActivityDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash,
				instanceDomainString, Instance.INSTANCE_ACTOR_NAME, extractActorIdentifier(url),
				UUID.fromString(pathTokens.getFirst()));
		return res;
	}

	private RdfPubObjectId parseCache(String url) {
		List<String> pathTokens = getTokensOfRightPart(url, RdfPubObjectId.CACHE_TOKEN);
		if(pathTokens.size()!=1) {
			throw new IllegalArgumentException(url);
		}
		return new RdfPubObjectIdCacheDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash,
				instanceDomainString, Instance.INSTANCE_ACTOR_NAME, extractActorIdentifier(url), pathTokens.getFirst());
	}

	private RdfPubObjectIdActor parseActor(String url) {
		List<String> pathTokens = getTokensOfRightPart(url, RdfPubObjectId.PREFIX_TOKEN);
		RdfPubObjectIdActorDefault res = new RdfPubObjectIdActorDefault(rdfPubIRIFactory, rdf, internalUrnStringWithEndingSlash,
				instanceDomainString, Instance.INSTANCE_ACTOR_NAME, extractActorIdentifier(url));
		if(pathTokens.size()>1) {
			res.setRevision(UUID.fromString(pathTokens.get(1)));
		}
		return res;
	}

	private List<String> getTokensOfRightPart(String url, String recognitionToken) {
		String str = recognitionToken+"/";
		String rightPart = url.substring(url.lastIndexOf(str)+str.length());
		return toPathTokens(rightPart);
	}
	
	private boolean isResource(String url) {
		return url.contains("/"+RdfPubObjectId.RESOURCE_TOKEN+"/");
	}

	private boolean isCache(String url) {
		return url.contains("/"+RdfPubObjectId.CACHE_TOKEN+"/");
	}

	private boolean isCollection(String url) {
		return asPublic.getIRIString().equals(url) 
				|| url.contains("/"+RdfPubObjectId.COLLECTION_TOKEN+"/");
	}
	
	private boolean isActivity(String url) {
		return url.contains("/"+RdfPubObjectId.ACTIVITY_TOKEN+"/");
	}

	private List<String> toPathTokens(String subject) {
		
		if(subject.startsWith(internalUrnStringWithEndingSlash)) {
			subject = subject.substring(internalUrnStringWithEndingSlash.length());
		}
		List<String> tTest = Collections.list(new StringTokenizer(subject, "/")).stream()
			      .map(token -> (String) token)
			      .collect(Collectors.toList());;
		return tTest;
	}

	@Override
	public RdfPubObjectIdActor instanceActorSubject() {
		return createFromUrl(internalUrnStringWithEndingSlash + "resource/" + Instance.INSTANCE_ACTOR_NAME).asActor().orElseThrow();
	}

	@Override
	public RdfPubBlankNodeOrIRI createDummy() {
		return new RdfPubIRIDefault(rdf.createIRI("irn:dummy"));
	}

	@Override
	public RdfPubIRI createPendingFollowerCacheId(RdfPubIRI recipientActorId) {
		RdfPubBlankNodeOrIRI cacheId = recipientActorId.asActor().map(RdfPubObjectIdActor::getBaseSubject).orElse(recipientActorId);
		String cacheIdString = cacheId.getIRIString() + "/cache/pendingfollowercache";
		return createFromUrl(cacheIdString);
	}

	@Override
	public RdfPubIRI createPendingFollowingCacheId(RdfPubIRI recipientActorId) {
		RdfPubBlankNodeOrIRI cacheId = recipientActorId.asActor().map(RdfPubObjectIdActor::getBaseSubject).orElse(recipientActorId);
		String cacheIdString = cacheId.getIRIString() + "/cache/pendingfollowingcache";
		return createFromUrl(cacheIdString);
	}
}
