package org.linkedopenactors.rdfpub.domain;

import java.util.Set;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;


public interface OrderedCollectionPage extends ActivityPubObject {
	RdfPubBlankNodeOrIRI partOf();
	long totalItems();
	void setPartOf(RdfPubBlankNodeOrIRI iri);
	void setTotalItems(int size);
	void setItems(Set<RdfPubBlankNodeOrIRI> collect);
	void addObject(ActivityPubObject object);
	Set<RdfPubBlankNodeOrIRI> getItems();
}
