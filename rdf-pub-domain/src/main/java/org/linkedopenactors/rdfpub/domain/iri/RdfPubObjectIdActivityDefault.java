package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.RDF;

class RdfPubObjectIdActivityDefault extends RdfPubObjectIdBase implements RdfPubObjectIdActivity {

	private UUID activityIdentifier;
	private UUID revision;
	
	public RdfPubObjectIdActivityDefault(RdfPubObjectIdActivityDefault id) {
		super(id.rdfPubIRIFactory, id.rdf, id.internalUrnString, id.instanceDomainString, id.instanceActorIdentifier, id.actorIdentifier);
		this.activityIdentifier = id.getActivityIdentifier();
	}
	
	public RdfPubObjectIdActivityDefault(RdfPubIRIFactory vocabContainerParam, RDF rdf, String baseIriString, String instanceDomainString, String instanceActorIdentifier, String actorIdentifier, UUID activityIdentifier) {
		super(vocabContainerParam, rdf, baseIriString, instanceDomainString, instanceActorIdentifier, actorIdentifier);
		this.activityIdentifier = activityIdentifier;
	}

	@Override
	public RdfPubBlankNodeOrIRI getSubject() {
		return getBaseSubject();
	}

	@Override
	public RdfPubIRI getBaseSubject() {
		return rdfPubIRIFactory.createRdfPubIriIri(super.getBaseSubjectString() + "/" + ACTIVITY_TOKEN + "/" + activityIdentifier);
	}

	@Override
	public UUID getActivityIdentifier() {
		return activityIdentifier;
	}
	
	public void setRevision(UUID revision) {
		this.revision = revision;
	}
	
	public Optional<UUID> getRevision() {
		return Optional.ofNullable(revision);
	}
}
