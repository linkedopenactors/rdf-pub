package org.linkedopenactors.rdfpub.domain.iri;

public interface VocLDP {
	public static final String NS = "http://www.w3.org/ns/ldp#";
	public final static String inbox = NS + "inbox";

	RdfPubIRI inbox();
}
