package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocRdfPub;

public class VocRdfPubDefault implements VocRdfPub {

	private RDF rdf;

	public VocRdfPubDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	/**
	 * rdf-pub commitId
	 * <p>
	 * {@code http://rdf-pub.org#commitId}
	 * <p>
	 * The git commitId from which this instance was built.
	 * @see <a href="http://rdf-pub.org#commitId">commitId</a>
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #commitId} property.
	 */
	public RdfPubIRI commitId() {
		return new RdfPubIRIDefault(rdf.createIRI(commitId));
	}

	/**
	 * The unique userId, that a user has in it's {@link #oauth2Issuer}.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #oauth2Issuer} property.
	 */
	public RdfPubIRI oauth2IssuerUserId() {
		return new RdfPubIRIDefault(rdf.createIRI(oauth2IssuerUserId));
	}
	
	/**
	 * The issue host e.g. 'login.m4h.network'.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #oauth2Issuer} property.
	 */
	public RdfPubIRI oauth2Issuer() {
		return new RdfPubIRIDefault(rdf.createIRI(oauth2Issuer));
	}

	/**
	 * The software version of rdf-pub. Normaly matches to the maven version.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #version} property.
	 */
	public RdfPubIRI version() {
		return new RdfPubIRIDefault(rdf.createIRI(version));
	}

	/**
	 * The version of an object. Assuming an actor has version 5, the FEP-4ccd is
	 * now implemented in rdf-pub, which means that an actor also receives a
	 * pendingFollowers and pendingFollowing collection. Then the actor must be
	 * updated to version 6, which is extended by the 2 collections.
	 * Actor is just a sample, applies to all types of objects.
	 * @return The {@link RdfPubBlankNodeOrIRI} of the {@link #activityPubObjectVersion} property.
	 */
	public RdfPubIRI activityPubObjectVersion() {
		return new RdfPubIRIDefault(rdf.createIRI(activityPubObjectVersion));
	}
	
	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the personsCollection.
	 */
	public RdfPubIRI personsCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(personsCollection));
	}

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the applicationsCollection.
	 */
	public RdfPubIRI applicationsCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(applicationsCollection));
	}

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the groupsCollection.
	 */
	public RdfPubIRI groupsCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(groupsCollection));
	}

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the organisationsCollection.
	 */
	public RdfPubIRI organizationsCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(organizationsCollection));
	}

	/**
	 * For administrative purposes it is very helpful to manage all stakeholder groups in collections. 
	 * @return The {@link RdfPubBlankNodeOrIRI} of the servicesCollection.
	 */
	public RdfPubIRI servicesCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(servicesCollection));
	}
}
