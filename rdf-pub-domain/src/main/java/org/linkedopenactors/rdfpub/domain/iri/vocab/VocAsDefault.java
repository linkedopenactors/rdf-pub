package org.linkedopenactors.rdfpub.domain.iri.vocab;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRIDefault;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;

public class VocAsDefault implements VocAs {
	
	private RDF rdf;

	public VocAsDefault(RDF rdf) {
		this.rdf = rdf;
	}
	
	public RdfPubIRI Object() {
		return new RdfPubIRIDefault(rdf.createIRI(Object));
	}

	public RdfPubIRI Link() {
		return new RdfPubIRIDefault(rdf.createIRI(Link));
	}

	public RdfPubIRI Activity() {
		return new RdfPubIRIDefault(rdf.createIRI(Activity));
	}

	public RdfPubIRI IntransitiveActivity() {
		return new RdfPubIRIDefault(rdf.createIRI(IntransitiveActivity));
	}

	public RdfPubIRI Collection() {
		return new RdfPubIRIDefault(rdf.createIRI(Collection));
	}

	public RdfPubIRI OrderedCollection() {
		return new RdfPubIRIDefault(rdf.createIRI(OrderedCollection));
	}

	public RdfPubIRI CollectionPage() {
		return new RdfPubIRIDefault(rdf.createIRI(CollectionPage));
	}

	public RdfPubIRI OrderedCollectionPage() {
		return new RdfPubIRIDefault(rdf.createIRI(OrderedCollectionPage));
	}

	public RdfPubIRI Accept() {
		return new RdfPubIRIDefault(rdf.createIRI(Accept));
	}

	public RdfPubIRI TentativeAccept() {
		return new RdfPubIRIDefault(rdf.createIRI(TentativeAccept));
	}

	public RdfPubIRI Add() {
		return new RdfPubIRIDefault(rdf.createIRI(Add));
	}

	public RdfPubIRI Arrive() {
		return new RdfPubIRIDefault(rdf.createIRI(Arrive));
	}

	public RdfPubIRI Create() {
		return new RdfPubIRIDefault(rdf.createIRI(Create));
	}

	public RdfPubIRI Delete() {
		return new RdfPubIRIDefault(rdf.createIRI(Delete));
	}

	public RdfPubIRI Follow() {
		return new RdfPubIRIDefault(rdf.createIRI(Follow));
	}

	public RdfPubIRI Ignore() {
		return new RdfPubIRIDefault(rdf.createIRI(Ignore));
	}

	public RdfPubIRI Join() {
		return new RdfPubIRIDefault(rdf.createIRI(Join));
	}

	public RdfPubIRI Leave() {
		return new RdfPubIRIDefault(rdf.createIRI(Leave));
	}

	public RdfPubIRI Like() {
		return new RdfPubIRIDefault(rdf.createIRI(Like));
	}

	public RdfPubIRI Offer() {
		return new RdfPubIRIDefault(rdf.createIRI(Offer));
	}

	public RdfPubIRI Invite() {
		return new RdfPubIRIDefault(rdf.createIRI(Invite));
	}

	public RdfPubIRI Reject() {
		return new RdfPubIRIDefault(rdf.createIRI(Reject));
	}

	public RdfPubIRI TentativeReject() {
		return new RdfPubIRIDefault(rdf.createIRI(TentativeReject));
	}

	public RdfPubIRI Remove() {
		return new RdfPubIRIDefault(rdf.createIRI(Remove));
	}

	public RdfPubIRI Undo() {
		return new RdfPubIRIDefault(rdf.createIRI(Undo));
	}

	public RdfPubIRI Update() {
		return new RdfPubIRIDefault(rdf.createIRI(Update));
	}

	public RdfPubIRI View() {
		return new RdfPubIRIDefault(rdf.createIRI(View));
	}

	public RdfPubIRI Listen() {
		return new RdfPubIRIDefault(rdf.createIRI(Listen));
	}

	public RdfPubIRI Read() {
		return new RdfPubIRIDefault(rdf.createIRI(Read));
	}

	public RdfPubIRI Move() {
		return new RdfPubIRIDefault(rdf.createIRI(Move));
	}

	public RdfPubIRI Travel() {
		return new RdfPubIRIDefault(rdf.createIRI(Travel));
	}

	public RdfPubIRI Announce() {
		return new RdfPubIRIDefault(rdf.createIRI(Announce));
	}

	public RdfPubIRI Block() {
		return new RdfPubIRIDefault(rdf.createIRI(Block));
	}

	public RdfPubIRI Flag() {
		return new RdfPubIRIDefault(rdf.createIRI(Flag));
	}

	public RdfPubIRI Dislike() {
		return new RdfPubIRIDefault(rdf.createIRI(Dislike));
	}

	public RdfPubIRI Question() {
		return new RdfPubIRIDefault(rdf.createIRI(Question));
	}

	public RdfPubIRI Application() {
		return new RdfPubIRIDefault(rdf.createIRI(Application));
	}

	public RdfPubIRI Group() {
		return new RdfPubIRIDefault(rdf.createIRI(Group));
	}

	public RdfPubIRI Organization() {
		return new RdfPubIRIDefault(rdf.createIRI(Organization));
	}

	public RdfPubIRI Person() {
		return new RdfPubIRIDefault(rdf.createIRI(Person));
	}

	public RdfPubIRI Service() {
		return new RdfPubIRIDefault(rdf.createIRI(Service));
	}

	public RdfPubIRI Relationship() {
		return new RdfPubIRIDefault(rdf.createIRI(Relationship));
	}

	public RdfPubIRI Article() {
		return new RdfPubIRIDefault(rdf.createIRI(Article));
	}

	public RdfPubIRI Document() {
		return new RdfPubIRIDefault(rdf.createIRI(Document));
	}

	public RdfPubIRI Audio() {
		return new RdfPubIRIDefault(rdf.createIRI(Audio));
	}

	public RdfPubIRI Image() {
		return new RdfPubIRIDefault(rdf.createIRI(Image));
	}

	public RdfPubIRI Video() {
		return new RdfPubIRIDefault(rdf.createIRI(Video));
	}

	public RdfPubIRI Note() {
		return new RdfPubIRIDefault(rdf.createIRI(Note));
	}

	public RdfPubIRI Page() {
		return new RdfPubIRIDefault(rdf.createIRI(Page));
	}

	public RdfPubIRI Event() {
		return new RdfPubIRIDefault(rdf.createIRI(Event));
	}

	public RdfPubIRI Place() {
		return new RdfPubIRIDefault(rdf.createIRI(Place));
	}

	public RdfPubIRI Mention() {
		return new RdfPubIRIDefault(rdf.createIRI(Mention));
	}

	public RdfPubIRI Profile() {
		return new RdfPubIRIDefault(rdf.createIRI(Profile));
	}

	public RdfPubIRI Tombstone() {
		return new RdfPubIRIDefault(rdf.createIRI(Tombstone));
	}

	public RdfPubIRI actor() {
		return new RdfPubIRIDefault(rdf.createIRI(actor));
	}

	public RdfPubIRI attachment() {
		return new RdfPubIRIDefault(rdf.createIRI(attachment));
	}

	public RdfPubIRI attributedTo() {
		return new RdfPubIRIDefault(rdf.createIRI(attributedTo));
	}

	public RdfPubIRI audience() {
		return new RdfPubIRIDefault(rdf.createIRI(audience));
	}

	public RdfPubIRI bcc() {
		return new RdfPubIRIDefault(rdf.createIRI(bcc));
	}

	public RdfPubIRI bto() {
		return new RdfPubIRIDefault(rdf.createIRI(bto));
	}

	public RdfPubIRI cc() {
		return new RdfPubIRIDefault(rdf.createIRI(cc));
	}

	public RdfPubIRI context() {
		return new RdfPubIRIDefault(rdf.createIRI(context));
	}

	public RdfPubIRI current() {
		return new RdfPubIRIDefault(rdf.createIRI(current));
	}

	public RdfPubIRI first() {
		return new RdfPubIRIDefault(rdf.createIRI(first));
	}

	public RdfPubIRI generator() {
		return new RdfPubIRIDefault(rdf.createIRI(generator));
	}

	public RdfPubIRI icon() {
		return new RdfPubIRIDefault(rdf.createIRI(icon));
	}

	public RdfPubIRI image() {
		return new RdfPubIRIDefault(rdf.createIRI(image));
	}

	public RdfPubIRI inReplyTo() {
		return new RdfPubIRIDefault(rdf.createIRI(inReplyTo));
	}

	public RdfPubIRI instrument() {
		return new RdfPubIRIDefault(rdf.createIRI(instrument));
	}

	public RdfPubIRI last() {
		return new RdfPubIRIDefault(rdf.createIRI(last));
	}

	public RdfPubIRI location() {
		return new RdfPubIRIDefault(rdf.createIRI(location));
	}

	public RdfPubIRI items() {
		return new RdfPubIRIDefault(rdf.createIRI(items));
	}

	public RdfPubIRI oneOf() {
		return new RdfPubIRIDefault(rdf.createIRI(oneOf));
	}

	public RdfPubIRI anyOf() {
		return new RdfPubIRIDefault(rdf.createIRI(anyOf));
	}

	public RdfPubIRI closed() {
		return new RdfPubIRIDefault(rdf.createIRI(closed));
	}

	public RdfPubIRI origin() {
		return new RdfPubIRIDefault(rdf.createIRI(origin));
	}

	public RdfPubIRI next() {
		return new RdfPubIRIDefault(rdf.createIRI(next));
	}

	public RdfPubIRI object() {
		return new RdfPubIRIDefault(rdf.createIRI(object));
	}

	public RdfPubIRI prev() {
		return new RdfPubIRIDefault(rdf.createIRI(prev));
	}

	public RdfPubIRI preview() {
		return new RdfPubIRIDefault(rdf.createIRI(preview));
	}

	public RdfPubIRI result() {
		return new RdfPubIRIDefault(rdf.createIRI(result));
	}

	public RdfPubIRI replies() {
		return new RdfPubIRIDefault(rdf.createIRI(replies));
	}

	public RdfPubIRI tag() {
		return new RdfPubIRIDefault(rdf.createIRI(tag));
	}

	public RdfPubIRI target() {
		return new RdfPubIRIDefault(rdf.createIRI(target));
	}

	public RdfPubIRI to() {
		return new RdfPubIRIDefault(rdf.createIRI(to));
	}

	public RdfPubIRI url() {
		return new RdfPubIRIDefault(rdf.createIRI(url));
	}

	public RdfPubIRI accuracy() {
		return new RdfPubIRIDefault(rdf.createIRI(accuracy));
	}

	public RdfPubIRI altitude() {
		return new RdfPubIRIDefault(rdf.createIRI(altitude));
	}

	public RdfPubIRI content() {
		return new RdfPubIRIDefault(rdf.createIRI(content));
	}

	public RdfPubIRI name() {
		return new RdfPubIRIDefault(rdf.createIRI(name));
	}

	public RdfPubIRI duration() {
		return new RdfPubIRIDefault(rdf.createIRI(duration));
	}

	public RdfPubIRI height() {
		return new RdfPubIRIDefault(rdf.createIRI(height));
	}

	public RdfPubIRI href() {
		return new RdfPubIRIDefault(rdf.createIRI(href));
	}

	public RdfPubIRI hreflang() {
		return new RdfPubIRIDefault(rdf.createIRI(hreflang));
	}

	public RdfPubIRI partOf() {
		return new RdfPubIRIDefault(rdf.createIRI(partOf));
	}

	public RdfPubIRI latitude() {
		return new RdfPubIRIDefault(rdf.createIRI(latitude));
	}

	public RdfPubIRI longitude() {
		return new RdfPubIRIDefault(rdf.createIRI(longitude));
	}

	public RdfPubIRI mediaType() {
		return new RdfPubIRIDefault(rdf.createIRI(mediaType));
	}

	public RdfPubIRI endTime() {
		return new RdfPubIRIDefault(rdf.createIRI(endTime));
	}

	public RdfPubIRI published() {
		return new RdfPubIRIDefault(rdf.createIRI(published));
	}

	public RdfPubIRI startTime() {
		return new RdfPubIRIDefault(rdf.createIRI(startTime));
	}

	public RdfPubIRI radius() {
		return new RdfPubIRIDefault(rdf.createIRI(radius));
	}

	public RdfPubIRI rel() {
		return new RdfPubIRIDefault(rdf.createIRI(rel));
	}

	public RdfPubIRI startIndex() {
		return new RdfPubIRIDefault(rdf.createIRI(startIndex));
	}

	public RdfPubIRI summary() {
		return new RdfPubIRIDefault(rdf.createIRI(summary));
	}

	public RdfPubIRI totalItems() {
		return new RdfPubIRIDefault(rdf.createIRI(totalItems));
	}

	public RdfPubIRI units() {
		return new RdfPubIRIDefault(rdf.createIRI(units));
	}

	public RdfPubIRI updated() {
		return new RdfPubIRIDefault(rdf.createIRI(updated));
	}

	public RdfPubIRI width() {
		return new RdfPubIRIDefault(rdf.createIRI(width));
	}

	public RdfPubIRI subject() {
		return new RdfPubIRIDefault(rdf.createIRI(subject));
	}

	public RdfPubIRI relationship() {
		return new RdfPubIRIDefault(rdf.createIRI(relationship));
	}

	public RdfPubIRI describes() {
		return new RdfPubIRIDefault(rdf.createIRI(describes));
	}

	public RdfPubIRI formerType() {
		return new RdfPubIRIDefault(rdf.createIRI(formerType));
	}

	public RdfPubIRI deleted() {
		return new RdfPubIRIDefault(rdf.createIRI(deleted));
	}

	public RdfPubIRI outbox() {
		return new RdfPubIRIDefault(rdf.createIRI(outbox));
	}

	public RdfPubIRI inbox() {
		return new RdfPubIRIDefault(rdf.createIRI(inbox));
	}

	public RdfPubIRI following() {
		return new RdfPubIRIDefault(rdf.createIRI(following));
	}

	public RdfPubIRI followers() {
		return new RdfPubIRIDefault(rdf.createIRI(followers));
	}

	public RdfPubIRI liked() {
		return new RdfPubIRIDefault(rdf.createIRI(liked));
	}

	public RdfPubIRI oauthAuthorizationEndpoint() {
		return new RdfPubIRIDefault(rdf.createIRI(oauthAuthorizationEndpoint));
	}

	public RdfPubIRI oauthTokenEndpoint() {
		return new RdfPubIRIDefault(rdf.createIRI(oauthTokenEndpoint));
	}

	public RdfPubIRI preferredUsername() {
		return new RdfPubIRIDefault(rdf.createIRI(preferredUsername));
	}

	public RdfPubIRI endpoints() {
		return new RdfPubIRIDefault(rdf.createIRI(endpoints));
	}

	public RdfPubIRI Public() {
		return new RdfPubIRIDefault(rdf.createIRI(Public));
	}

	@Override
	public String withoutNameSpace(RdfPubIRI iri) {
		return iri.getIRIString().replace(NS, "");
	}
}
