package org.linkedopenactors.rdfpub.domain.iri;

public interface VocAs {

	public static final String PREFIX = "as";
	public static final String NS = "https://www.w3.org/ns/activitystreams#";
	public static final String LDP_NS = "http://www.w3.org/ns/ldp#";
	
	public final static String Object = NS + "Object";
	public final static String Link = NS + "Link";
	public final static String Activity = NS + "Activity";
	public final static String IntransitiveActivity = NS + "IntransitiveActivity";
	public final static String Collection = NS + "Collection";
	public final static String OrderedCollection = NS + "OrderedCollection";
	public final static String CollectionPage = NS + "CollectionPage";
	public final static String OrderedCollectionPage = NS + "OrderedCollectionPage";
	public final static String Accept = NS + "Accept";
	public final static String TentativeAccept = NS + "TentativeAccept";
	public final static String Add = NS + "Add";
	public final static String Arrive = NS + "Arrive";
	public final static String Create = NS + "Create";
	public final static String Delete = NS + "Delete";
	public final static String Follow = NS + "Follow";
	public final static String Ignore = NS + "Ignore";
	public final static String Join = NS + "Join";
	public final static String Leave = NS + "Leave";
	public final static String Like = NS + "Like";
	public final static String Offer = NS + "Offer";
	public final static String Invite = NS + "Invite";
	public final static String Reject = NS + "Reject";
	public final static String TentativeReject = NS + "TentativeReject";
	public final static String Remove = NS + "Remove";
	public final static String Undo = NS + "Undo";
	public final static String Update = NS + "Update";
	public final static String View = NS + "View";
	public final static String Listen = NS + "Listen";
	public final static String Read = NS + "Read";
	public final static String Move = NS + "Move";
	public final static String Travel = NS + "Travel";
	public final static String Announce = NS + "Announce";
	public final static String Block = NS + "Block";
	public final static String Flag = NS + "Flag";
	public final static String Dislike = NS + "Dislike";
	public final static String Question = NS + "Question";
	public final static String Application = NS + "Application";
	public final static String Group = NS + "Group";
	public final static String Organization = NS + "Organization";
	public final static String Person = NS + "Person";
	public final static String Service = NS + "Service";
	public final static String Relationship = NS + "Relationship";
	public final static String Article = NS + "Article";
	public final static String Document = NS + "Document";
	public final static String Audio = NS + "Audio";
	public final static String Image = NS + "Image";
	public final static String Video = NS + "Video";
	public final static String Note = NS + "Note";
	public final static String Page = NS + "Page";
	public final static String Event = NS + "Event";
	public final static String endpoints = NS + "endpoints";
	public final static String Place = NS + "Place";
	public final static String Mention = NS + "Mention";
	public final static String Profile = NS + "Profile";
	public final static String Tombstone = NS + "Tombstone";
	public final static String actor = NS + "actor";
	public final static String attachment = NS + "attachment";
	public final static String attributedTo = NS + "attributedTo";
	public final static String audience = NS + "audience";
	public final static String bcc = NS + "bcc";
	public final static String bto = NS + "bto";
	public final static String cc = NS + "cc";
	public final static String context = NS + "context";
	public final static String current = NS + "current";
	public final static String first = NS + "first";
	public final static String generator = NS + "generator";
	public final static String icon = NS + "icon";
	public final static String image = NS + "image";
	public final static String inReplyTo = NS + "inReplyTo";
	public final static String instrument = NS + "instrument";
	public final static String last = NS + "last";
	public final static String location = NS + "location";
	public final static String items = NS + "items";
	public final static String oneOf = NS + "oneOf";
	public final static String anyOf = NS + "anyOf";
	public final static String closed = NS + "closed";
	public final static String origin = NS + "origin";
	public final static String next = NS + "next";
	public final static String object = NS + "object";
	public final static String prev = NS + "prev";
	public final static String preview = NS + "preview";
	public final static String result = NS + "result";
	public final static String replies = NS + "replies";
	public final static String tag = NS + "tag";
	public final static String target = NS + "target";
	public final static String to = NS + "to";
	public final static String url = NS + "url";
	public final static String accuracy = NS + "accuracy";
	public final static String altitude = NS + "altitude";
	public final static String content = NS + "content";
	public final static String name = NS + "name";
	public final static String duration = NS + "duration";
	public final static String height = NS + "height";
	public final static String href = NS + "href";
	public final static String hreflang = NS + "hreflang";
	public final static String partOf = NS + "partOf";
	public final static String latitude = NS + "latitude";
	public final static String longitude = NS + "longitude";
	public final static String mediaType = NS + "mediaType";
	public final static String endTime = NS + "endTime";
	public final static String published = NS + "published";
	public final static String Public = NS + "Public";
	public final static String preferredUsername = NS + "preferredUsername";
	public final static String startTime = NS + "startTime";
	public final static String radius = NS + "radius";
	public final static String rel = NS + "rel";
	public final static String startIndex = NS + "startIndex";
	public final static String summary = NS + "summary";
	public final static String totalItems = NS + "totalItems";
	public final static String units = NS + "units";
	public final static String updated = NS + "updated";
	public final static String width = NS + "width";
	public final static String subject = NS + "subject";
	public final static String relationship = NS + "relationship";
	public final static String describes = NS + "describes";
	public final static String formerType = NS + "formerType";
	public final static String deleted = NS + "deleted";
	public final static String outbox = NS + "outbox";
	public final static String inbox = LDP_NS + "inbox";
	public final static String following = NS + "following";
	public final static String followers = NS + "followers";
	public final static String liked = NS + "liked";
	public final static String oauthAuthorizationEndpoint = NS + "oauthAuthorizationEndpoint";
	public final static String oauthTokenEndpoint = NS + "oauthTokenEndpoint";

	RdfPubIRI Object();

	RdfPubIRI Link();

	RdfPubIRI Activity();

	RdfPubIRI IntransitiveActivity();

	RdfPubIRI Collection();

	RdfPubIRI OrderedCollection();

	RdfPubIRI CollectionPage();

	RdfPubIRI OrderedCollectionPage();

	RdfPubIRI Accept();

	RdfPubIRI TentativeAccept();

	RdfPubIRI Add();

	RdfPubIRI Arrive();

	RdfPubIRI Create();

	RdfPubIRI Delete();

	RdfPubIRI Follow();

	RdfPubIRI Ignore();

	RdfPubIRI Join();

	RdfPubIRI Leave();

	RdfPubIRI Like();

	RdfPubIRI Offer();

	RdfPubIRI Invite();

	RdfPubIRI Reject();

	RdfPubIRI TentativeReject();

	RdfPubIRI Remove();

	RdfPubIRI Undo();

	RdfPubIRI Update();

	RdfPubIRI View();

	RdfPubIRI Listen();

	RdfPubIRI Read();

	RdfPubIRI Move();

	RdfPubIRI Travel();

	RdfPubIRI Announce();

	RdfPubIRI Block();

	RdfPubIRI Flag();

	RdfPubIRI Dislike();

	RdfPubIRI Question();

	RdfPubIRI Application();

	RdfPubIRI Group();

	RdfPubIRI Organization();

	RdfPubIRI Person();

	RdfPubIRI Service();

	RdfPubIRI Relationship();

	RdfPubIRI Article();

	RdfPubIRI Document();

	RdfPubIRI Audio();

	RdfPubIRI Image();

	RdfPubIRI Video();

	RdfPubIRI Note();

	RdfPubIRI Page();

	RdfPubIRI Event();

	RdfPubIRI Place();

	RdfPubIRI Mention();

	RdfPubIRI Profile();

	RdfPubIRI Tombstone();

	RdfPubIRI actor();

	RdfPubIRI attachment();

	RdfPubIRI attributedTo();

	RdfPubIRI audience();

	RdfPubIRI bcc();

	RdfPubIRI bto();

	RdfPubIRI cc();

	RdfPubIRI context();

	RdfPubIRI current();

	RdfPubIRI first();

	RdfPubIRI generator();

	RdfPubIRI icon();

	RdfPubIRI image();

	RdfPubIRI inReplyTo();

	RdfPubIRI instrument();

	RdfPubIRI last();

	RdfPubIRI location();

	RdfPubIRI items();

	RdfPubIRI oneOf();

	RdfPubIRI anyOf();

	RdfPubIRI closed();

	RdfPubIRI origin();

	RdfPubIRI next();

	RdfPubIRI object();

	RdfPubIRI prev();

	RdfPubIRI preview();

	RdfPubIRI result();

	RdfPubIRI replies();

	RdfPubIRI tag();

	RdfPubIRI target();

	RdfPubIRI to();

	RdfPubIRI url();

	RdfPubIRI accuracy();

	RdfPubIRI altitude();

	RdfPubIRI content();

	RdfPubIRI name();

	RdfPubIRI duration();

	RdfPubIRI height();

	RdfPubIRI href();

	RdfPubIRI hreflang();

	RdfPubIRI partOf();

	RdfPubIRI latitude();

	RdfPubIRI longitude();

	RdfPubIRI mediaType();

	RdfPubIRI endTime();

	RdfPubIRI published();

	RdfPubIRI startTime();

	RdfPubIRI radius();

	RdfPubIRI rel();

	RdfPubIRI startIndex();

	RdfPubIRI summary();

	RdfPubIRI totalItems();

	RdfPubIRI units();

	RdfPubIRI updated();

	RdfPubIRI width();

	RdfPubIRI subject();

	RdfPubIRI relationship();

	RdfPubIRI describes();

	RdfPubIRI formerType();

	RdfPubIRI deleted();

	RdfPubIRI outbox();

	RdfPubIRI inbox();

	RdfPubIRI following();

	RdfPubIRI followers();

	RdfPubIRI liked();

	RdfPubIRI oauthAuthorizationEndpoint();

	RdfPubIRI oauthTokenEndpoint();

	RdfPubIRI preferredUsername();

	RdfPubIRI endpoints();

	RdfPubIRI Public();
	
	String withoutNameSpace(RdfPubIRI iri);
}
