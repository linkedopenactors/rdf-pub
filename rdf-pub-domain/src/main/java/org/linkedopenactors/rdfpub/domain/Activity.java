package org.linkedopenactors.rdfpub.domain;

import java.util.Optional;
import java.util.Set;

import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;



/**
 * An Activity is a subtype of {@link ActivityPubObject} that describes some
 * form of action that may happen, is currently happening, or has already
 * happened. The Activity type itself serves as an abstract base type for all
 * types of activities. It is important to note that the Activity type itself
 * does not carry any specific semantics about the kind of action being taken
 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-activity
 */
public interface Activity extends ActivityPubObject {
	/**
	 * Describes an object of any kind. The Object type serves as the base type for
	 * most of the other kinds of objects defined in the Activity Vocabulary,
	 * including other Core types such as Activity, IntransitiveActivity, Collection
	 * and OrderedCollection.
	 * 
	 * @return The object of this {@link Activity}
	 */
	java.util.Set<RdfPubBlankNodeOrIRI> getObject();

	/**
	 * Describes an {@link ActivityPubObject} of any kind.
	 * @param objects
	 */
	void setObject(java.util.Set<RdfPubBlankNodeOrIRI> objects);
	
	/**
	 * Describes an {@link ActivityPubObject} of any kind.
	 * @param activityPubObject
	 */
	void addObject(ActivityPubObject activityPubObject);

	/**
	 * Describes an {@link ActivityPubObject} of any kind.
	 * @param activityPubObject
	 */
	void setObjects(Set<ActivityPubObject> activityPubObject);

	/**
	 * Describes an {@link ActivityPubObject} of any kind.
	 * @param object
	 */
	void addObject(RdfPubBlankNodeOrIRI object);

	/**
	 * Describes an {@link ActivityPubObject} of any kind.
	 * @return The object(s)
	 */
	Set<ActivityPubObject> resolveObject();
	
	/**
	 * Returns a {@link Set} of all objects that are hidden in the underlaying
	 * graph. An {@link ActivityPubObject} is a wrapper arround a graph. it points
	 * to only one subject in this graph. This method detect all objects in the
	 * graph expect the activity itself.
	 * 
	 * @return A {@link Set} of all objects that are hidden in the underlaying
	 * graph
	 */
	Set<ActivityPubObject> resolveAllOfGraph();

	/**
	 * Describes one or more entities that either performed or are expected to
	 * perform the activity. Any single activity can have multiple actors.
	 * 
	 * @return The actor id(s) of this {@link Activity}.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-actor
	 */
	java.util.Set<RdfPubIRI> getActor();

//	/**
//	 * Abstraction of the type of this activity, for convenience. 
//	 * @return Abstraction of the activity type. 
//	 */
//	AT activityType();

//	/**
//	 * @param activityType
//	 * @return True, if the activity has the passed type.
//	 */
//	boolean is(AT activityType);

	/**
	 * Describes one or more entities that either performed or are expected to
	 * perform the activity. Any single activity can have multiple actors.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-actor
	 * 
	 * @param actor
	 */
	void addActor(RdfPubBlankNodeOrIRI actor);
	
	/**
	 * Describes one or more entities that either performed or are expected to
	 * perform the activity. Any single activity can have multiple actors.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-actor
	 * 
	 * @param actor
	 */
	void setActor(Set<RdfPubBlankNodeOrIRI> actor);

	/**
	 * Describes the indirect object, or target, of the activity. The precise
	 * meaning of the target is largely dependent on the type of action being
	 * described but will often be the object of the English preposition "to". For
	 * instance, in the activity "John added a movie to his wishlist", the target of
	 * the activity is John's wishlist. An activity can have more than one target.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-target
	 * 
	 * @param target
	 */
	void setTarget(RdfPubBlankNodeOrIRI target);
	
	/**
	 * Describes the indirect object, or target, of the activity. The precise
	 * meaning of the target is largely dependent on the type of action being
	 * described but will often be the object of the English preposition "to". For
	 * instance, in the activity "John added a movie to his wishlist", the target of
	 * the activity is John's wishlist. An activity can have more than one target.
	 * See Also: https://www.w3.org/TR/activitystreams-vocabulary/#dfn-target
	 * 
	 * @return The target's id.
	 */
	Optional<RdfPubBlankNodeOrIRI> getTarget();
	
	void setWasAssociatedWith(RdfPubBlankNodeOrIRI actor);
	Optional<RdfPubBlankNodeOrIRI> getWasAssociatedWith();
	void removeWasAssociatedWith();
	
	/**
	 * Ensures that audience, cc, bcc, to, bto are identical in activity and objects
	 */
	void unify();

	void replaceSubjectsWithInternals();
	void replaceSubjects(RdfPubObjectIdActor actorsBaseSubject, boolean includingObjects);
	void replaceSubjects(RdfPubObjectIdActor actorsBaseSubject);

	void replaceObject(ActivityPubObject existing);
}
