package org.linkedopenactors.rdfpub.domain.iri;

public interface VocRdf {
	public static final String NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String type = NS + "type";
	public final static String Bag = NS + "Bag";
	public final static String Property = NS + "Property";
	
	RdfPubIRI type();

	RdfPubIRI Bag();

	RdfPubIRI Property();
}
