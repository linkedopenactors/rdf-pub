package org.linkedopenactors.rdfpub.domain.iri;

public interface VocXsdDatatypes {
//	public static final String NS = "https://www.w3.org/TR/xmlschema11-2/#";
	public static final String NS = "http://www.w3.org/2001/XMLSchema#";
	public final static String nonNegativeInteger = NS + "nonNegativeInteger";
	public final static String string = NS + "string";
	public final static String decimal = NS + "decimal";
	public final static String integer = NS + "integer";
	public final static String long_ = NS + "long";
	public final static String float_type = NS + "float";
	public final static String boolean_type = NS + "boolean";
	public final static String date = NS + "date";
	public final static String dateTime = NS + "dateTime";
	public final static String time = NS + "time";
	public final static String duration = NS + "duration";
	
	RdfPubIRI nonNegativeInteger();

	RdfPubIRI string();

	RdfPubIRI decimal();

	RdfPubIRI integer();

	RdfPubIRI long_();

	RdfPubIRI floatType();

	RdfPubIRI booleanType();

	RdfPubIRI date();
	
	RdfPubIRI dateTime();
		
	RdfPubIRI time();
	
	RdfPubIRI duration();
}
