package org.linkedopenactors.rdfpub.domain.iri;

import org.apache.commons.rdf.api.IRI;

public interface RdfPubIRI extends RdfPubBlankNodeOrIRI, IRI  {

}
