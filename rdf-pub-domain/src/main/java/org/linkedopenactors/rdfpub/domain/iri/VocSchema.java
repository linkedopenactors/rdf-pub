package org.linkedopenactors.rdfpub.domain.iri;

public interface VocSchema {
	public static final String PREFIX = "schema";
	public static final String NS = "http://schema.org/";
	public final static String version = NS + "version";
	public final static String identifier = NS + "identifier";
	public final static String name = NS + "name";
	
	RdfPubIRI version();
}
