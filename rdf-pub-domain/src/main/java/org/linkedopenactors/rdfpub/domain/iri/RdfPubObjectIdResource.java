package org.linkedopenactors.rdfpub.domain.iri;

import java.util.Optional;
import java.util.UUID;


public interface RdfPubObjectIdResource extends RdfPubObjectId {

	UUID getResourceIdentifier();

	Optional<UUID> getRevision();
	
	boolean isRevision();
}
