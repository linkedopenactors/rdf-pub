package org.linkedopenactors.rdfpub.domain;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.rdf4j.RDF4J;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.commonsrdf.GraphToStringConverter;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocXsdDatatypes;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.mockito.Mockito;

@Disabled
class TestPartialUpdate {

	private ActivityPubObjectFactory activityPubObjectFactory;
	
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	private VocabContainer vocabContainer;
	
	@BeforeEach
	public void setup() {
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn("http://localhost:8081");
		when(prefixMapper.getInternaleIriPrefix()).thenReturn("urn:rdfpub:");
		
		vocabContainer = Mockito.mock(VocabContainer.class);
		VocXsdDatatypes vocXsdDatatypes = Mockito.mock(VocXsdDatatypes.class);
		VocAs vocAs = Mockito.mock(VocAs.class);
		VocRdf vocRdf = Mockito.mock(VocRdf.class);
				
		when(vocAs.Object()).thenReturn(getIri("https://www.w3.org/ns/activitystreams#Objecft"));
		when(vocAs.Audio()).thenReturn(getIri("https://www.w3.org/ns/activitystreams#Audio"));
		when(vocAs.Announce()).thenReturn(getIri("https://www.w3.org/ns/activitystreams#Announce"));
		when(vocAs.Collection()).thenReturn(getIri("https://www.w3.org/ns/activitystreams#Collection"));
		 
		when(vocRdf.type()).thenReturn(getIri(vocRdf.NS + vocRdf.type));
		
		when(vocXsdDatatypes.long_()).thenReturn(getIri(VocXsdDatatypes.NS + VocXsdDatatypes.long_));
		
		when(vocabContainer.vocXsdDatatypes()).thenReturn(vocXsdDatatypes);
		when(vocabContainer.vocAs()).thenReturn(vocAs);
		when(vocabContainer.vocRdf()).thenReturn(vocRdf);
		
		when(vocabContainer.createRdfPubIriIri(any())).thenReturn(getIri("https://mocked/" + UUID.randomUUID().toString()));
//		when(vocabContainer.createRdfPubIriIri(any(c)).thenReturn(getIri("https://mocked/" + UUID.randomUUID().toString()));
		
		
		RDF rdf = new RDF4J();
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(vocabContainer, rdf, prefixMapper);
				
		StringToGraphConverter stringToGraphConverter = Mockito.mock(StringToGraphConverter.class);
		GraphToStringConverter graphToStringConverter = Mockito.mock(GraphToStringConverter.class);
		
//		AS as = Mockito.mock(AS.class);
//		when(as.Object()).thenReturn(rdf.createIRI(AS.Object));
//		when(as.Audio()).thenReturn(rdf.createIRI(AS.Audio));
//		when(as.Announce()).thenReturn(rdf.createIRI(AS.Announce));
//		when(as.Collection()).thenReturn(rdf.createIRI(AS.Collection));
		
//		vocabularies = Mockito.mock(Vocabularies.class);
//		when(vocabularies.getActivityStreams()).thenReturn(as);
		
		activityPubObjectFactory = new ActivityPubObjectFactory(vocabContainer,  rdf, stringToGraphConverter, graphToStringConverter,
				activityPubObjectIdBuilder, prefixMapper);
	}
	
	@Test
	void test() {
		RdfPubBlankNodeOrIRI objectSubject = activityPubObjectIdBuilder.createActor();
		ActivityPubObject existing = activityPubObjectFactory.create(objectSubject);
		
		existing.addType(vocabContainer.vocAs().Object());
		existing.addType(vocabContainer.vocAs().Audio());
		
		ActivityPubObject newOne = activityPubObjectFactory.create(objectSubject);
		newOne.addType(vocabContainer.vocAs().Collection());
		newOne.addType(vocabContainer.vocAs().Announce());
				
		
		System.out.println(newOne);
		
		existing.partialUpdate(newOne);
		
		List<RdfPubBlankNodeOrIRI> types = existing.getTypes().stream().collect(Collectors.toList());
		Assertions.assertEquals(2, types.size());
		Assertions.assertEquals(vocabContainer.vocAs().Announce(), types.get(0));
		Assertions.assertEquals(vocabContainer.vocAs().Collection(), types.get(1));
	}
	
	private RdfPubIRI asPublic() {
		return getIri("https://www.w3.org/ns/activitystreams#Public");
	}
	
	private RdfPubIRI getIri(String iri) {
		return new RdfPubIRI() {
			
			@Override
			public String ntriplesString() {
				return "<" + getIRIString() + ">";
			}
			
			@Override
			public boolean isBlankNode() {
				return false;
			}
			
			@Override
			public String getIRIString() {
				return iri;
			}
		};
	}
}
