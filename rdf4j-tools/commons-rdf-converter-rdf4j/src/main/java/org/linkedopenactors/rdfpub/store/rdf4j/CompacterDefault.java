package org.linkedopenactors.rdfpub.store.rdf4j;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.JSONLDSettings;
import org.springframework.stereotype.Component;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

@Component
class CompacterDefault {
	
	private WriterConfig getWriterConfig() {
		WriterConfig config = new WriterConfig();
		config.set(JSONLDSettings.HIERARCHICAL_VIEW, true);
		config.set(JSONLDSettings.COMPACT_ARRAYS, true);
		config.set(JSONLDSettings.OPTIMIZE, true);
//		config.set(JSONLDSettings.USE_RDF_TYPE, true);
		return config;
	}
	
	public String compact(Model asModel, String input) {
		String context = getContext(asModel);
		String compacted = compact(context, input, true);
		return compacted;
	}

	private String getContext(Model asModel) {
		Set<String> namespaces = asModel.stream()
			.map(Statement::getPredicate)
			.map(IRI::getNamespace)
			.collect(Collectors.toSet());
		
		List<String> ctx = new ArrayList<>();
		ctx.add("\"https://www.w3.org/ns/activitystreams\"");
		ctx.add("\"https://rdf-pub.org/schema/rdfpub01.jsonld\"");
		
		if(namespaces.contains("https://w3id.org/security#")) {
			ctx.add("\"https://w3id.org/security/v1\"");
		}
		if(namespaces.contains("https://schema.org/")) {
			ctx.add("\"https://schema.org/docs/jsonldcontext\"");
		}
		if(namespaces.contains("https://purl.archive.org/socialweb/pending#")) {
			ctx.add("\"https://purl.archive.org/socialweb/pending\"");
		}

//		if(namespaces.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#")) {
//			
//		}
		
//		if(namespaces.contains("https://purl.archive.org/socialweb/pending#")) {
//			
//		}
		
//		if(namespaces.contains("http://www.w3.org/ns/prov#")) {
//			ctx.add("\"https://www.w3.org/submissions/prov-jsonld/context\"");			
//		}
		
//		if(namespaces.contains("http://www.w3.org/2002/07/owl#")) {
//			
//		}
//		
//		if(namespaces.contains("http://www.w3.org/ns/ldp#")) {
//			
//		}
		
//		if(namespaces.contains("https://schema.rdf-pub.org/")) {
//			ctx.add("\"https://schema.rdf-pub.org\"");				
//		}
				
		// TODO thats not really nice! And there are a lot of missing contexts!
		// Lets create a propoerty file with the mappings! So it's extendable. 
		
		String cctx = ctx.stream().collect(Collectors.joining(","));
		cctx = "{\"@context\": [" + cctx + "]}";
		return cctx;
	}
	
	private String compact(String context, String jsonldString, boolean pretty) {
		Model input;
		try {
			input = Rio.parse(new StringReader(jsonldString), RDFFormat.JSONLD);
		} catch (Exception e) {
			throw new IllegalStateException("cannot parse jsonld: " + jsonldString, e);
		}		
		WriterConfig config = getWriterConfig();
		StringWriter stringWriter = new StringWriter();
		
		Rio.write(input, stringWriter, RDFFormat.JSONLD, config);
		String string = stringWriter.toString();		
		return compactInternal(context, string, pretty); 
	}
	
	
	private String compactInternal(String context, String input, boolean pretty) {
		try {
			JsonLdOptions opts = new JsonLdOptions();
			opts.setCompactArrays(true);
//		opts.setOmitDefault(Boolean.TRUE);
			opts.setProcessingMode(JsonLdOptions.JSON_LD_1_1);

			Object frame = JsonUtils.fromString(context);
			Map<String, Object> res = JsonLdProcessor.compact(JsonUtils.fromString(input), frame, opts);

			StringWriter writer = new StringWriter();
			if(pretty) {
				JsonUtils.writePrettyPrint(writer, res);
			} else {
				JsonUtils.write(writer, res);
			}
			return writer.toString();
		} catch (Exception e) {
			throw new IllegalStateException("unable to compact json-ld", e);
		}
	}

//	private String getContext() {
//		return "{\n" + "  \"@context\": [\n" + "  	\"https://schema.org/docs/jsonldcontext.json\",\n"
////				+ "  	\"https://rdf-pub.org/schema/rdf-pub-context.json\",\n"
//				// TODO thats a problem, because i seperated website and technical stuff
//				+ "  	\"https://w3id.org/security/v1\",\n"				
////				+ "  	\"https://www.w3.org/submissions/prov-jsonld/context.jsonld\",\n"
//				+ "  	\"https://www.w3.org/ns/activitystreams\"\n"
//				+ "  ]\n" + "}";
//	}
}
