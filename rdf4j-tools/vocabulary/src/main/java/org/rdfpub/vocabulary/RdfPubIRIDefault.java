package org.rdfpub.vocabulary;

//import org.apache.commons.rdf.api.BlankNode;
//import org.apache.commons.rdf.api.BlankNodeOrIRI;
//import org.apache.commons.rdf.api.IRI;
//import org.apache.commons.rdf.api.RDF;
//import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
//
//class RdfPubIRIDefault implements RdfPubIRI, BlankNodeOrIRI {
//	
//    private final BlankNodeOrIRI iri;
//
//    public RdfPubIRIDefault(final IRI iriParam) {
//    	this.iri = iriParam;
//    }
//
//    public RdfPubIRIDefault(RDF rdf, final String iriParam) {
//		this.iri = rdf.createIRI(iriParam);
//    }
//
//	public boolean isBlankNode() {
//		return iri instanceof BlankNode;
//	}
//
//    @Override
//    public boolean equals(final Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null || !(obj instanceof IRI)) {
//            return false;
//        }
//        final IRI other = (IRI) obj;
//        boolean equals = getIRIString().equals(other.getIRIString());
//		return equals;
//    }
//
//    @Override
//    public String getIRIString() {    	
//    	if(iri instanceof IRI) {
//    		return ((IRI)iri).getIRIString();
//    	} else {
//    		return iri.toString();
//    	}
//    }
//
//    @Override
//    public int hashCode() {
//        return iri.hashCode();
//    }
//
//    @Override
//    public String ntriplesString() {
//        return "<" + getIRIString() + ">";
//    }
//
//    @Override
//    public String toString() {
//        return getIRIString();
//    }
//}
