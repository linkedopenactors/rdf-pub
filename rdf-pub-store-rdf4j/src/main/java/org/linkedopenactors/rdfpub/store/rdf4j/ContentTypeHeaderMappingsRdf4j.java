package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.linkedopenactors.rdfpub.domain.ContentTypeHeaderMappings;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.springframework.stereotype.Component;

@Component
public class ContentTypeHeaderMappingsRdf4j implements ContentTypeHeaderMappings {
	
	static final Map<String, RDFFormat> contentTypeHeaderMapping = new HashMap<>();

	static {
		toContentTypeHeaderMapping(RDFFormat.TURTLE);
		toContentTypeHeaderMapping(RDFFormat.RDFXML);
		toContentTypeHeaderMapping(RDFFormat.NTRIPLES);
		toContentTypeHeaderMapping(RDFFormat.TURTLESTAR);
		toContentTypeHeaderMapping(RDFFormat.N3);
		toContentTypeHeaderMapping(RDFFormat.JSONLD);
		toContentTypeHeaderMapping(RDFFormat.NQUADS);
		toContentTypeHeaderMapping(RDFFormat.RDFJSON);
	}

	@Override
	public RdfFormat determineRdfFormat(String requestedAcceptHeader) {
		return RdfFormat.valueOf(rdf4jFormatAsString(determineRDFFormat(requestedAcceptHeader)));
	}

	private RDFFormat determineRDFFormat(String requestedAcceptHeader) {
		if(requestedAcceptHeader==null) {
			return RDFFormat.JSONLD;
		} else {
			RDFFormat rdfFormat = Stream.of(requestedAcceptHeader.split(";", -1))
					.map(this::get)
					.filter(Objects::nonNull)
					.findAny()
					.orElse(RDFFormat.JSONLD);
			return rdfFormat;
		}
	}
	
	
	private String rdf4jFormatAsString(RDFFormat rdfFormat) {
		switch (rdfFormat.getName()) {
		case "Turtle": return "TURTLE"; 
		case "N-Triples": return "NTRIPLES";
		case "Turtle-star": return "TURTLESTAR";
		case "N3": return "N3";
		case "TriX": return "TRIX";
		case "TriG": return "TRIG";
		case "TriG-star": return "TRIGSTAR";
		case "BinaryRDF": return "BINARY";
		case "N-Quads": return "NQUADS";
		case "JSON-LD": return "JSONLD";
		case "NDJSON-LD": return "NDJSONLD";
		case "RDF/JSON": return "RDFJSON";
		case "RDFa": return "RDFA";
		case "HDT": return "HDT";
		default:
			throw new IllegalArgumentException("Unexpected value: " + rdfFormat.getName() + " use one of: " + RdfFormat.values());
		}
	}
	
	private static void toContentTypeHeaderMapping(RDFFormat rdfFormat) {
		rdfFormat.getMIMETypes().forEach(mt->contentTypeHeaderMapping.put(mt, rdfFormat));
	}

//	public static String getSupportedMimeTypes() {
//		return contentTypeHeaderMapping.values().stream()
//				.map(RDFFormat::getMIMETypes)
//				.flatMap(Collection::stream)
//				.collect(Collectors.toList())
//				.stream()
//				.collect(Collectors.joining(","));
//	}
//
	private RDFFormat get(String acceptHeaderValue) {
		return contentTypeHeaderMapping.get(acceptHeaderValue);
	}
}
