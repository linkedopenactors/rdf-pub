package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHandler {

	private RDF rdf;

	public AbstractHandler(RDF rdf) {
		this.rdf = rdf;
	}
	
	/**
	 * Remove all subjects of the passed Collection, that are not persistent.
	 * @param objectSubjects
	 * @return Only subjects, that are persistent. 
	 */
	protected Set<IRI> filterExisting( Actor owner, Set<RdfPubIRI> objectSubjects, ActivityPubStore activityPubStore) {
		return objectSubjects.stream()
				.filter(subject->activityPubStore.findBySubject(subject).isPresent())
				.collect(Collectors.toSet());
	}

	protected IRI createIRI(String iri) {
		return rdf.createIRI(iri);
	}
}
