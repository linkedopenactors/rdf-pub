package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Dataset;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Quad;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.apache.commons.rdfrdf4j.RDF4JBlankNodeOrIRI;
import org.apache.commons.rdfrdf4j.RDF4JDataset;
import org.apache.commons.rdfrdf4j.RDF4JGraphLike;
import org.apache.commons.rdfrdf4j.RDF4JIRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFContainers;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.resultio.sparqljson.SPARQLResultsJSONWriter;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocOwl;
import org.linkedopenactors.rdfpub.domain.iri.VocRdf;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.Store;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class StoreDefault implements Store {
	static final org.slf4j.Logger graphlog = org.slf4j.LoggerFactory.getLogger(StoreDefault.class.getName() + "_graphlog");
	static final org.slf4j.Logger graphlogStore = org.slf4j.LoggerFactory.getLogger(StoreDefault.class.getName() + "_graphlog_store");
	private Dataset dataset;
	private RDF4J rdf;
	private String loggableStoreName;
	private VocabContainer vocabContainer;
	
	
	public StoreDefault(VocabContainer vocabContainer, RDF4J rdf, RDF4JDataset dataset, String loggableStoreName) {
		this.vocabContainer = vocabContainer;
		this.rdf = rdf;
		this.dataset = dataset;
		this.loggableStoreName = Optional.ofNullable(loggableStoreName).orElseThrow(() -> new IllegalStateException("loggableStoreName mandatory!"));
	}

	@Override
	public void add(BlankNodeOrIRI graphName, Graph graph, String logMsg) {
		Optional.ofNullable(logMsg).ifPresent(log::debug);
		if(log.isDebugEnabled()) {
			log.debug("add(storeOwner:"+loggableStoreName+", graphName: "+graphName+", graph)");
		}
			
		// TODO problem with that loop!!! See PR https://github.com/apache/commons-rdf/pull/205
		synchronized (dataset) {
			graph.stream().forEach(triple->{
				dataset.add(graphName, triple.getSubject(), triple.getPredicate(), triple.getObject());
			});
		}
	}

	@Override
	public void add(BlankNodeOrIRI graphName, Set<Triple> newTriples, String logMsg) {
		Optional.ofNullable(logMsg).ifPresent(log::debug);
		log.debug("add(owner:"+loggableStoreName+", newTriples)");
		if(log.isTraceEnabled()) {
			log.trace("newTriples: "+newTriples);
		}
		// TODO problem with that loop!!! See PR https://github.com/apache/commons-rdf/pull/205
		synchronized (dataset) {
			newTriples.stream().forEach(triple->{
				dataset.add(graphName, triple.getSubject(), triple.getPredicate(), triple.getObject());
			});
		}
	}

	@Override
	public void remove(BlankNodeOrIRI graphName, Set<BlankNodeOrIRI> subjects, String logMsg) {
		Optional.ofNullable(logMsg).ifPresent(log::debug);
		synchronized (dataset) {
			subjects.stream().forEach(subject->{
				dataset.remove(Optional.of(graphName), subject, null, null);
			});
		}
	}

	/**
	 * Info. the managment of the internal revisons while saving is done in {@link AbstractHandler}. 
	 */
	@Override
	public Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject) {
		return find(graphName, subject, 0);
	}
	
	@Override
	public Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject, int deep) {
		if(deep>1) {
			throw new UnsupportedOperationException("deep is currently only possible for 1.");
		}
		log.trace("->owner:"+loggableStoreName+", subject:"+subject+", deep:"+deep+")");
		Optional<Graph> graphOpt = find(graphName, subject, null, null)
				.flatMap(graph->checkIfItsABag(graphName, subject, graph));
		if(deep==1) {
			
			StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
			for (int i = 0; i < 5; i++) {				
				log.trace("comming from: " + stackTrace[i].getClassName() + "#" + stackTrace[i].getMethodName() + "("+stackTrace[i].getLineNumber()+")");
			}
			graphOpt = graphOpt.map(graph->resolveReferences(graphName, graph));
		}		
		log.trace("<- " +graphOpt.isPresent() + " find("+graphName+", "+subject+", "+deep+")");
		return graphOpt;
	}

	@Override
	public String queryTuple(String query) {
		synchronized (dataset) {
			Repository repo = ((RDF4JDataset) dataset).asRepository().orElseThrow();
			try (RepositoryConnection conn = repo.getConnection()) {
				TupleQuery tupleQuery = conn.prepareTupleQuery(query);
				StringWriter sw = new StringWriter();
				tupleQuery.evaluate(new SPARQLResultsJSONWriter(sw));
				String result = sw.toString();
				return result;
			}
		}
	}

	private Graph resolveReferences(BlankNodeOrIRI graphName, Graph graph) {
		log.trace("resolveReferences("+graphName+", graph)");
		Map<String, RDFTerm> m = graph.stream()
				.collect(Collectors.toMap(e->UUID.randomUUID().toString() + "|" + e.getPredicate().toString(), Triple::getObject));
		
		
		Map<String, IRI> filtered = m.entrySet().stream()
	                        .filter(entry->IRI.class.isInstance(entry.getValue()))
	                        .filter(entry->isInternal(entry.getValue().toString()))
	                        .collect(Collectors.toMap(
	                                e -> e.getKey(),
	                                e -> IRI.class.cast(e.getValue())
	                            ));
		filtered.forEach((k, v) -> {
			log.trace("resolving: " + k + " -> " + v );
			Optional<Graph> gf = find(graphName, v, null, null).flatMap(gr -> checkIfItsABag(graphName, v, gr));
			if(gf.isEmpty()) {
				gf = find(vocabContainer.vocAs().Public(), v, null, null).flatMap(gr -> checkIfItsABag(graphName, v, gr));
			}
			log.trace("resolving ("+v+") found: " + gf.isPresent());
			gf.ifPresent(gr -> {
				gr.stream().forEach(graph::add);
			});
		});		
		return graph;
	}
	
	private boolean isInternal(String iri) {
		return !iri.startsWith("http");
	}
	
	private Optional<Graph> checkIfItsABag(BlankNodeOrIRI graphName, BlankNodeOrIRI subject, Graph graph) {
		
		if(isBag(subject, graph)) {
			log.trace("checkIfItsABag: YES");
			Optional<Graph> latest = findLatestRevision(graphName, (IRI)subject, true);
			return latest;
		} else {
			log.trace("checkIfItsABag: NO");
		}
		return Optional.of(graph);
	}

	@Override
	public Optional<Graph> find(BlankNodeOrIRI graphName, BlankNodeOrIRI subject, IRI predicate, RDFTerm object) {
		log.debug("find(graphName: " + graphName + ", subject: " + subject + ", predicate: " + predicate + ", object: " + object + ")");
		Graph graph = rdf.createGraph();
		synchronized (dataset) {
			try(Stream<? extends Quad> stream = dataset.stream(Optional.ofNullable(graphName), subject, predicate, object)) {
				stream.forEach(quad->graph.add(quad.getSubject(), quad.getPredicate(), quad.getObject()));
			}
		}
		if(graph.size() < 1) {
			return Optional.empty();
		} else {
			return Optional.of(graph);
		}
	}

	@Override
	public Map<IRI, Graph> find(BlankNodeOrIRI graphName, IRI predicate, RDFTerm object) {
		Map<BlankNodeOrIRI, Graph> grapfByIri = find(graphName, null, predicate, object)
				.map(graph->new GraphToGraphs(rdf).convert(graph))
				.orElse(Collections.emptyMap());
		return toIriMap(grapfByIri);
	}

	private Map<IRI, Graph> toIriMap(Map<BlankNodeOrIRI, Graph> d) {
		Map<IRI, Graph> grapfByIri = new HashMap<>();		
		d.entrySet().stream()
			.filter(s->s.getKey() instanceof IRI)
			.forEach(e-> grapfByIri.put((IRI)e.getKey(), e.getValue()));
		return grapfByIri;
	}

	@Override
	public Map<IRI, Graph> findAll(BlankNodeOrIRI graphName, List<BlankNodeOrIRI> subjects) {
		Map<BlankNodeOrIRI, Graph> result = new HashMap<>();
		subjects.forEach(sub->{
			find(graphName, sub)
				.ifPresent(graph->result.put(sub, graph));
		});			
		return toIriMap(result);
	}

	@Override
	public Map<IRI, Graph> findAll(BlankNodeOrIRI graphName, List<BlankNodeOrIRI> subjects, int deep) {
		Map<BlankNodeOrIRI, Graph> result = new HashMap<>();
		subjects.forEach(sub->{
			find(graphName, sub, deep)
				.ifPresent(graph->result.put(sub, graph));
		});			
		return toIriMap(result);
	}

	private Optional<Graph> findLatestRevision(BlankNodeOrIRI graphName, IRI subject, boolean useCollectionSubject) {
		Optional<Graph> result = Optional.empty();
		List<IRI> items = getIriList(graphName, subject);		
		if(!items.isEmpty()) {
			IRI revisionSubject = items.get(items.size()-1);
			result = find(graphName, revisionSubject);
			if(useCollectionSubject) {
				result = replaceSubject(result.orElse(null), revisionSubject, subject);
				result.ifPresent(r->r.add(subject, rdf.createIRI(VocOwl.sameAs), revisionSubject));
			}
		}
		return result;
		
	}

	@Override
	public Optional<IRI> findLatestRevisionSubject(BlankNodeOrIRI graphName, IRI collectionSubject) {
		Optional<IRI> result = Optional.empty();
		List<IRI> items = getIriList(graphName, collectionSubject);

		if(!items.isEmpty()) {
			result = Optional.ofNullable(items.get(items.size()-1));
		}
		return result;
	}

	private List<IRI> getIriList(BlankNodeOrIRI graphName, IRI subject) {
		return getCollection(graphName, subject)
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList()).stream()
					.map(IRI.class::cast)
					.collect(Collectors.toList());
	}

	@Override
	public Optional<Graph> find(BlankNodeOrIRI graphName) {
		// ATTENTION
		// the dataset is a org.apache.commons.rdfrdf4j.impl.RepositoryDatasetImpl and returns a org.apache.commons.rdfrdf4j.impl.RepositoryGraphImpl
		// which offers open connection streams, so the clinet has to take care of closing that stream.
		// I think that's too dangerous, so it's better to have a decoupled copy
		Optional<Graph> found;
		synchronized (dataset) {
			found = dataset.getGraph(graphName);
		}
		if(found.isPresent()) {
			Graph copy = rdf.createGraph();
			try(Stream<? extends Triple> stream = found.get().stream()) {
				stream.forEach(triple->copy.add(triple));
				return Optional.of(copy);
			}
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Check's, if the passed graph is a Rdf-Bag.
	 * @param subject
	 * @param graph
	 * @return True, if the passed graph's type is Rdf-Bag.
	 */
	private boolean isBag(BlankNodeOrIRI subject, Graph graph) {
		return graph.stream(subject, rdf.createIRI(VocRdf.type), (RDFTerm)null)
				.map(Triple::getObject)
				.anyMatch(term->{
					RDF4JIRI bagTerm = rdf.createIRI(VocRdf.Bag);
					return term.equals(bagTerm);
				});
	}
	
	/**
	 * The latest Object is known to the user under a pointer name. This pointer
	 * points internally to a collection of revisions of a graph, so we have to
	 * replace the internal subject with the pointer.
	 * 
	 * @param graph
	 * @param revisionSubject
	 * @param pointerSubject
	 * @return The passed graph with the pointerSubject instead of the intrenal subject.
	 */
	private Optional<Graph> replaceSubject(Graph graph, BlankNodeOrIRI revisionSubject, BlankNodeOrIRI pointerSubject) {
		return Optional.ofNullable(graph).map(g->{
			Graph result = rdf.createGraph();
			g.stream(revisionSubject, null, null)
				.forEach(t->result.add(pointerSubject, t.getPredicate(), t.getObject()));
			return result;
		});
	}
		
	public Set<BlankNodeOrIRI> getGraphNames() {
		try(Stream<BlankNodeOrIRI> graphNames = dataset.getGraphNames()) {
			return graphNames.collect(Collectors.toSet());
		}
	}

	public long countTriples() {
		long result;
		synchronized (dataset) {
			result = this.dataset.stream().count();
		}
		return result;
	}

	public long countTriples(BlankNodeOrIRI graphName) {
		long result;
		synchronized (dataset) {
			result = this.dataset.stream(Optional.ofNullable(graphName), null, null, null).count();
		}
		return result;
	}

	@Override
	public void saveCollection(BlankNodeOrIRI graphName, IRI subject, List<IRI> items, String humanReadableName, /*List<IRI> items,*/ String logMsg) {
		log.debug("saveCollection: owner:"+loggableStoreName+", collection: "+subject+", + graphName: " + graphName + ")");
				
		if(exists(graphName, subject)) {
			throw new IllegalStateException("graph '%s' already exists!".formatted(graphName));
		}
		saveCollectionInternal(graphName, subject, items, humanReadableName, logMsg);
	}
	
	private void saveCollectionInternal(BlankNodeOrIRI graphName, IRI subject, List<IRI> items, String humanReadableName, /*List<IRI> items,*/ String logMsg) {
		List<org.eclipse.rdf4j.model.IRI> rdf4jItems = items.stream()
				.map(i->Values.iri(i.toString()))
				.collect(Collectors.toList());
		
		RDF4JIRI rdf4jIri = rdf.createIRI(subject.getIRIString());
		org.eclipse.rdf4j.model.IRI collectionSubject = rdf4jIri.asValue();
		Model bag = RDFContainers.toRDF(iri("http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag"), rdf4jItems, 
				collectionSubject, 
				new TreeModel());
		bag.add(collectionSubject, Values.iri(VocAs.name), Values.literal(humanReadableName));
		
		// TODO use new method -> https://github.com/apache/commons-rdf/pull/205
		Graph bagGraph = new RDF4J().asGraph(bag);
		synchronized (dataset) {
			bagGraph.stream().forEach(t->dataset.add(graphName, t.getSubject(), t.getPredicate(), t.getObject()));
		}
	}
	
	private void removeCollectionStatements(BlankNodeOrIRI graphName, IRI subject) {
		synchronized (dataset) {
			List<? extends Quad> quads = dataset.stream(Optional.ofNullable(graphName), subject, null,  null).collect(Collectors.toList());
			// so either i don't understand the dataset correctly, or it has a bug in the
			// remove method. either way. remove via graph works
			quads.stream().forEach(e->{
				Graph g = dataset.getGraph(graphName).orElseThrow();
				g.remove(e.getSubject(), e.getPredicate(), e.getObject());
				});
		}
	}

	/**
	 * @param graphName
	 * @param subject
	 * @return True, if the passed subjects points to a none empty graph.
	 */
	private boolean exists(BlankNodeOrIRI graphName, BlankNodeOrIRI subject) {
		try(Graph graph = getGraph(graphName);){
			return graph.contains(subject, null, null);
		} catch (Exception e) {
			throw new IllegalStateException("unable to get graph", e);
		}
	}
	
	@Override
	public Optional<PersistableRdfPubCollection> getCollection(BlankNodeOrIRI graphName, IRI collectionSubject ) {		 
		try(Stream<? extends Triple> stream = getGraph(graphName).stream(collectionSubject, null, null)){
			List<Triple> triples = stream.collect(Collectors.toList());	
			Graph sub = rdf.createGraph();
			triples.forEach(sub::add);
			@SuppressWarnings({ "rawtypes", "unchecked" })
			Optional<Model> modelOpt = ((RDF4JGraphLike)sub).asModel();
			log.trace("getCollection(owner:"+loggableStoreName+",collectionSubject:"+collectionSubject+") -> found: " + triples.size());
			return modelOpt.flatMap(model->{				
				return toPersistableRdfPubCollection(model, collectionSubject);
			});
			
		} catch (Exception e) {
			throw new IllegalStateException("unable to get collection: " + collectionSubject + ", graphName: " + graphName, e);
		}
	}

	@Override
	public Set<PersistableRdfPubCollection> findCollection(BlankNodeOrIRI graphName, IRI memberParam) {
		RDF4JIRI memberPredicate = rdf.createIRI("http://www.w3.org/2000/01/rdf-schema#member");
		try(Stream<? extends Triple> stream = getGraph(graphName).stream(null, memberPredicate, memberParam)){
			return stream
					.map(Triple::getSubject)
					.filter(IRI.class::isInstance)
					.map(IRI.class::cast)
					.map(x->getCollection(graphName, x))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.collect(Collectors.toSet());
		}
	}

	/**
	 * @param graphName
	 * @return The graph to the passed graphName.
	 */
	private Graph getGraph(BlankNodeOrIRI graphName) {
		Graph g;
		synchronized (dataset) {
			g = dataset.getGraph(graphName).orElseThrow(()->new IllegalStateException("no graph with name '%s'".formatted(graphName)));
		}
		return g;
	}
	

	/**
	 * @param model
	 * @param collectionSubjectParam
	 * @return The passed {@link Value}'s converted to {@link org.eclipse.rdf4j.model.IRI}'s. 
	 */
	private Optional<PersistableRdfPubCollectionDefault> toPersistableRdfPubCollection(Model model, IRI collectionSubjectParam)  {
		IRI collectionSubject = rdf.createIRI(collectionSubjectParam.getIRIString());		
		List<Value> values = RDFContainers.toValues(org.eclipse.rdf4j.model.vocabulary.RDF.BAG, model, 
				((RDF4JBlankNodeOrIRI)collectionSubject).asValue(), 
				new ArrayList<Value>());
		String name = Models.getPropertyLiteral(model, Values.iri(collectionSubject.getIRIString()), Values.iri(VocAs.name))
				.map(l->l.getLabel())
				.orElse("unnamed collection");
				
		return Optional.of(new PersistableRdfPubCollectionDefault(name, collectionSubject, toIriList(values)));
	}

	/**
	 * @param values
	 * @return The passed {@link Value}'s converted to {@link org.eclipse.rdf4j.model.IRI}'s. 
	 */
	private List<IRI> toIriList(List<Value> values)  {
		return values.stream().map(v->rdf.createIRI(v.toString())).collect(Collectors.toList());
	}

	@Override
	public void addItem(BlankNodeOrIRI graphName, IRI collection, String humanReadableName, IRI item, String logMsg) {
		log.debug("addItem (owner:"+loggableStoreName+", collection:"+collection+", item:"+item+")");
		if(!exists(graphName, collection)) {
			log.debug("collection does not yet exists, it will be created!");
			saveCollection(graphName, collection, List.of(item), humanReadableName, logMsg);
		} else {
			if(!contains(graphName, collection, item)) {
				log.debug("collection exists, item will be added!");
				long size = size(graphName, collection);
				String propIriAsString = "http://www.w3.org/1999/02/22-rdf-syntax-ns#_%s".formatted(size+1);
		
				// TODO how can we add multiple triples/quads ? See https://github.com/apache/commons-rdf/pull/205
				synchronized (dataset) {
					dataset.add(graphName, collection, rdf.createIRI(propIriAsString), item);
					dataset.add(graphName, collection, rdf.createIRI("http://www.w3.org/2000/01/rdf-schema#member"), item);
				}
			}
		}
	}
	
	/**
	 * Checks if the item is already in the collection.
	 * @param graphName
	 * @param collectionSubject
	 * @param item
	 * @return True, if the item is already in the collection.
	 */
	private boolean contains(BlankNodeOrIRI graphName, IRI collectionSubject, IRI item) {
		return getCollection(graphName, collectionSubject)
			.orElseThrow()
			.getItems().stream()
				.map(IRI::getIRIString)
				.filter(s->s.equals(item.getIRIString()))
				.count() > 0;
	}
	
	@Override
	public void removeItem(BlankNodeOrIRI graphName, IRI collection, String humanReadableName, IRI item,
			String logMsg) {
		// TODO Thats currently not the best way to do that! Maybe we find a better way in the future, if performance plays mor role.		
		Optional<PersistableRdfPubCollection> col = getCollection(graphName, collection);
		if(col.isPresent()) {
			List<IRI> itemsBeforeRemove = col.get().getItems();
			List<IRI> itemsAfterRemove = new ArrayList<>(itemsBeforeRemove);
			for (IRI i : itemsBeforeRemove) {
				if(i.getIRIString().equals(item.getIRIString())) {
					itemsAfterRemove.remove(i);
				}
			}
			removeCollectionStatements(graphName, collection);
			saveCollectionInternal(graphName, collection, itemsAfterRemove, "", "");
			
			int size = getCollection(graphName, collection).orElseThrow().getItems().size();
			if(size != itemsAfterRemove.size()) {
				throw new IllegalStateException("todo");
			}
		}
	}

	/**
	 * Determines the number of triples contained in the collection. 
	 * @param graphName
	 * @param collectionSubject
	 * @return The number of triples contained in the collection.
	 */
	private long size(BlankNodeOrIRI graphName, BlankNodeOrIRI collectionSubject) {
		synchronized (dataset) {
			Repository repo = ((RDF4JDataset)dataset).asRepository().orElseThrow();
			try (RepositoryConnection conn = repo.getConnection()) {
				Iterable<Statement> statements = conn.getStatements(Values.iri(collectionSubject.toString()), Values.iri("http://www.w3.org/2000/01/rdf-schema#member"), null);
				return StreamSupport.stream(statements.spliterator(), false).count();
			}
		}
	}
	
	@Override
	public Optional<PersistableRdfPubCollection> subCollection(BlankNodeOrIRI graphName, String humanReadableName, IRI collection, int offset, int limit) {
		Optional<PersistableRdfPubCollection> pOpt = getWholeListWithMemoryFootprint(graphName, collection);
		if(pOpt.isPresent()) {
			List<IRI> all = toIriList(pOpt);
			int end = offset + limit;
			end = end > all.size() ? all.size() : end;  
			return Optional.of(new PersistableRdfPubCollectionDefault(humanReadableName, collection, all.subList(offset, end)));	
		}
		else  {
			return pOpt;
		}
	}

	private List<IRI> toIriList(Optional<PersistableRdfPubCollection> pOpt) {
		return pOpt.get().getItems().stream()
				.map(IRI.class::cast)
				.collect(Collectors.toList());
	}
	
	/**
	 * This was an experiment, we have to check if it makes sence. Or if we have to
	 * move it to a statistics part.
	 * 
	 * @param graphName
	 * @param collection
	 * @return All items of the collection.
	 */
	private Optional<PersistableRdfPubCollection> getWholeListWithMemoryFootprint(BlankNodeOrIRI graphName, IRI collection) {
		Optional<PersistableRdfPubCollection> persistableRdfPubCollectionOpt = getCollection(graphName, collection);
		if(persistableRdfPubCollectionOpt.isPresent()) {
			try {
				List<IRI> all = toIriList(persistableRdfPubCollectionOpt);
				List<String> ss = all.stream().map(Object::toString).collect(Collectors.toList());
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
			    ObjectOutputStream oos = new ObjectOutputStream(bos);
			    oos.writeObject(ss);
				log.debug("bos (kb): " + (bos.toByteArray().length/1024) + " - items: " + all.size());
			} catch (Exception e) {
				throw new IllegalStateException("oje", e);
			}
		}
		return persistableRdfPubCollectionOpt;
	}
	
	public void dump() {
		synchronized (dataset) {
			new StoreDump(vocabContainer).dump(this.dataset, loggableStoreName);
		}
	}
}
