package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Objects;
import java.util.Optional;

import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.ActivityPubStoreRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.springframework.stereotype.Component;

@Component
class ActivityPubStoreRepositoryDefault implements ActivityPubStoreRepository {

	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private StoreRepository storeRepository;
	private VocabContainer vocabContainer;
	
	public ActivityPubStoreRepositoryDefault(VocabContainer vocabContainer, ActivityPubObjectFactory activityPubObjectFactory,
			StoreRepository storeRepository, ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.vocabContainer = vocabContainer;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.storeRepository = storeRepository;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}

	@Override
	public ActivityPubStore createActivityPubStore(RdfPubIRI storeOwner, String preferredUsername) {
		if(Objects.isNull(storeOwner)) {
			throw new IllegalStateException("storeName is mandatory!");
		}
		Optional<ActivityPubStore> existingStoreOpt = findActivityPubStore(storeOwner, preferredUsername);
		if(existingStoreOpt.isPresent()) {
			return existingStoreOpt.get();
		}
		
		Store store = storeRepository.createStore(storeOwner, preferredUsername);
		ActivityPubStore activityPubStore = new ActivityPubStoreDefault(vocabContainer, storeOwner, store, activityPubObjectFactory, activityPubObjectIdBuilder);
		return activityPubStore;
	}

	@Override
	public Optional<ActivityPubStore> findActivityPubStore(RdfPubIRI storeOwner, String preferredUsername) {
		Optional<ActivityPubStore> existingStoreOpt = Optional.empty();
		if(existingStoreOpt.isEmpty()) {
			storeRepository.findStore(storeOwner, preferredUsername)
					.map(store->new ActivityPubStoreDefault(vocabContainer, storeOwner, store, activityPubObjectFactory, activityPubObjectIdBuilder));
		}
		return existingStoreOpt;
	}
	
	@Override
	public void shutdown() {
		storeRepository.shutdown();		
	}	
}
