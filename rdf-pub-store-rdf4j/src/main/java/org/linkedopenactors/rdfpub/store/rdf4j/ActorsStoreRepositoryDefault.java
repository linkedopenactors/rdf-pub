package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.Optional;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectConvertable;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.ActorsStoreRepository;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.ReceiverStore;
import org.linkedopenactors.rdfpub.domain.cache.PendingFollowerCache;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocAs;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.ActivityPubStoreRepository;
import org.springframework.stereotype.Component;

@Component
public class ActorsStoreRepositoryDefault implements ActorsStoreRepository {

	private RDF rdf;
	private ActivityPubStoreRepository activityPubStoreRepository;
	private org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory activityPubObjectFactory;
	private ServiceForSavingActivities activityHandler;
	private PublicActorStore publicActorStore;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;

	public ActorsStoreRepositoryDefault(VocabContainer vocabContainer, RDF rdf, ActivityPubStoreRepository activityPubStoreRepository,
			org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory activityPubObjectFactory,
			ServiceForSavingActivities activityHandler, PublicActorStore publicActorStore,
			ActivityPubObjectIdBuilder activityPubObjectIdBuilder) {
		this.vocabContainer = vocabContainer;
		this.rdf = rdf;
		this.activityPubStoreRepository = activityPubStoreRepository;
		this.activityPubObjectFactory = activityPubObjectFactory;
		this.activityHandler = activityHandler;
		this.publicActorStore = publicActorStore;
		this.activityPubObjectIdBuilder = activityPubObjectIdBuilder;
	}
	
	@Override
	public Optional<ActorsStore> find(RdfPubObjectIdActor actorSubject) {
		return publicActorStore.findBySubject(actorSubject)
			.map(ActivityPubObject::asConvertable)
			.map(ActivityPubObjectConvertable::asActor)
			.map(this::getActorsStore);
	}

	@Override
	public Optional<ActorsStore> find(Actor actor) {
		return find4Actor(actor);
	}

	@Override
	public Optional<ReceiverStore> findReceiverStore(RdfPubBlankNodeOrIRI receiverId) {
		if(receiverId.isActor()) {
			return find(receiverId.asActor().orElseThrow()).map(ReceiverStore.class::cast);
		} else if(vocabContainer.vocAs().Public().equals(receiverId)) {
		return Optional.ofNullable(publicActorStore);	
		}
		else {			
			throw new IllegalStateException("not yet implemented! receiverId: " + receiverId);
		}
	}

	@Override
	public PendingFollowerCache getPendingFollowerCache(RdfPubIRI owner, String loggableStoreName) {
		RdfPubIRI storeId = activityPubObjectIdBuilder.createPendingFollowerCacheId(owner);
		return getCache(loggableStoreName, storeId);
	}

	@Override
	public PendingFollowerCache getPendingFollowingCache(RdfPubIRI owner, String loggableStoreName) {
		RdfPubIRI storeId = activityPubObjectIdBuilder.createPendingFollowingCacheId(owner);
		return getCache(loggableStoreName, storeId);
	}

	private PendingFollowerCache getCache(String loggableStoreName, RdfPubIRI storeId) {
		Optional<ActivityPubStore> activityPubStoreOpt = activityPubStoreRepository.findActivityPubStore(storeId, loggableStoreName);
		ActivityPubStore activityPubStore;
		if(activityPubStoreOpt.isPresent()) {
			activityPubStore = activityPubStoreOpt.get(); 
		} else {
			activityPubStore = activityPubStoreRepository.createActivityPubStore(storeId, loggableStoreName);
		}
		return new PendingFollowerCacheDefault(vocabContainer, activityPubStore);
	}

	private Optional<ActorsStore> find4Actor(Actor actor) {
		RdfPubObjectIdActor storeOwner = actor.getSubject().asActor().orElseThrow();
		String loggableStoreName = actor.getPreferredUsername().orElse("unknown");
		
		// TODO caching ?!
		ActivityPubStore actorStore;
		Optional<ActivityPubStore> activityPubStoreOpt = activityPubStoreRepository.findActivityPubStore(storeOwner.getBaseSubject(), loggableStoreName);
		if(activityPubStoreOpt.isPresent()) {
			actorStore = activityPubStoreOpt.get(); 
		} else {
			actorStore = activityPubStoreRepository.createActivityPubStore(storeOwner.getBaseSubject(), loggableStoreName);
		}
		return Optional.ofNullable(new ActorsStoreDefault(rdf, actor, actorStore, activityPubObjectFactory, activityHandler));
	}
	
	private ActorsStore getActorsStore(Actor actor) {
		RdfPubObjectIdActor storeOwnerId = actor.getSubject().asActor().orElseThrow();
		String loggableStoreName = actor.getPreferredUsername().orElse("unknown");

		ActivityPubStore activityPubStore = activityPubStoreRepository.findActivityPubStore(storeOwnerId.getBaseSubject(), loggableStoreName)
				.orElse(activityPubStoreRepository.createActivityPubStore(storeOwnerId.getBaseSubject(), loggableStoreName));
		return new ActorsStoreDefault(rdf, actor, activityPubStore, activityPubObjectFactory, activityHandler);
	}
}
