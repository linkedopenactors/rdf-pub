package org.linkedopenactors.rdfpub.store.rdf4j;

import org.apache.commons.rdf.api.RDF;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Instance;
import org.linkedopenactors.rdfpub.domain.PublicActorStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PublicActorStoreFactory {

	@Autowired
	private RDF rdf;
	
	@Autowired
	private StoreRepository storeRepository;

	@Autowired
	private ActivityPubObjectFactory activityPubObjectFactory;
	
	@Autowired
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	
	@Autowired
	private ServiceForSavingActivities serviceForSavingActivities;

	@Autowired
	private VocabContainer vocabContainer;
		
	@Bean
	public PublicActorStore publicActorStore() {
		RdfPubObjectIdActor instanceId = activityPubObjectIdBuilder.createActor(Instance.INSTANCE_ACTOR_NAME);
		Store store = storeRepository.createStore(vocabContainer.vocAs().Public(), "PUBLIC_STORE");
		ActivityPubStore apStore = new ActivityPubStoreDefault(vocabContainer, vocabContainer.vocAs().Public(), store, activityPubObjectFactory, activityPubObjectIdBuilder);
		return new PublicActorStoreDefault(vocabContainer, rdf, apStore, instanceId, activityPubObjectIdBuilder, serviceForSavingActivities);
	}
}
