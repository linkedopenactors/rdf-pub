package org.linkedopenactors.rdfpub.store.rdf4j;

import java.util.List;

import org.apache.commons.rdf.api.IRI;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;

public class PersistableRdfPubCollectionDefault implements PersistableRdfPubCollection {
	private String humanReadableName;
	private IRI subject;
	private List<IRI> items;
	
	public PersistableRdfPubCollectionDefault(String humanReadableName, IRI subject, List<IRI> items) {
		this.humanReadableName = humanReadableName;
		this.subject = subject;
		this.items = items;
	}

	@Override
	public String getHumanReadableName() {
		return humanReadableName;
	}

	@Override
	public IRI getSubject() {
		return subject;
	}

	@Override
	public List<IRI> getItems() {
		return items;
	}

	@Override
	public IRI getCollectionSubject() {
		return subject;
	}

	@Override
	public String toString() {
		return "[" + humanReadableName + ", subject=" + subject + "]";
	}
}
