package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;

// This is more a sample, not a test.
@Disabled
public class TestFollowerCollection extends AbstractTest {

	@Test
	void test() {

		String one = UUID.randomUUID().toString();
		RdfPubIRI follower1 = createIRI("urn:rdfpub:resource/" + one);
		String two = UUID.randomUUID().toString();
		RdfPubIRI follower2 = createIRI("urn:rdfpub:resource/" + two);
		String three = UUID.randomUUID().toString();
		RdfPubIRI follower3 = createIRI("urn:rdfpub:resource/" + three);

		ActorsStore actorsStore = getOwnersStore();		
		assertEquals(0, actorsStore.getFollowers().size());
		
		add(actorsStore, follower1, 1);		
		add(actorsStore, follower2, 2);		
		add(actorsStore, follower3, 3);
		
		List<RdfPubIRI> followers = actorsStore.getFollowers();
		
		List<String> vals = followers.stream().map(RdfPubBlankNodeOrIRI::getIRIString).collect(Collectors.toList());
		
		assertTrue(vals.contains(follower1.getIRIString()));
		assertTrue(vals.contains(follower2.getIRIString()));
		assertTrue(vals.contains(follower3.getIRIString()));
		assertTrue(followers.get(0).getIRIString().equals(follower1.getIRIString()));
		assertTrue(followers.get(1).getIRIString().equals(follower2.getIRIString()));
		assertTrue(followers.get(2).getIRIString().equals(follower3.getIRIString()));
	}

	private void add(ActorsStore actorsStore, RdfPubIRI follower1, int expectedSize) {
		actorsStore.addFollower(follower1);
		List<RdfPubIRI> followers = actorsStore.getFollowers();
		assertEquals(expectedSize, followers.size());
	}
}
