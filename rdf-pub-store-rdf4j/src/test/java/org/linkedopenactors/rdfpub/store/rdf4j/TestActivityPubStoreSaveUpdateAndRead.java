package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActivity;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectIdActor;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestActivityPubStoreSaveUpdateAndRead {

	private ActivityPubStore activityPubStore;
	private RDF4J rdf4j;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private StringToGraphConverter stringToGraphConverter;
	private StoreRepository storeRepository;
	private RdfPubObjectId owner;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;
	
	@BeforeEach
	public void setup() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);
		
		String external = "http://localhost:8081";
		String internal = "urn:rdfpub:";

		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn(external);
		when(prefixMapper.getInternaleIriPrefix()).thenReturn(internal);
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);
		owner = activityPubObjectIdBuilder.createActor();
//		String storeOwnerName = "actor_" + UUID.randomUUID().toString();
		Store store = getStore(rdf4j, storeRepository, owner.toString(), owner.getIdentifier());
		
		activityPubObjectFactory = new ActivityPubObjectFactory(vocabContainer, rdf4j, new StringToGraphConverterDefault(rdf4j),
				new GraphToStringConverterDefault(prefixMapper), activityPubObjectIdBuilder, prefixMapper);
		
		stringToGraphConverter = new StringToGraphConverterDefault(rdf4j); 
		activityPubStore = new ActivityPubStoreDefault(vocabContainer, owner, store, activityPubObjectFactory, activityPubObjectIdBuilder);		
	}

	@Test
	void test() {
		String published = "2015-02-10T15:04:55Z";

		Actor actor = getSampleActor(owner.asActor().orElseThrow());
		RdfPubObjectIdActivity activitySubject = activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow());
		RdfPubBlankNodeOrIRI objectSubject = activityPubObjectIdBuilder.createResource(actor.getSubject());
		
		ActorsStore actorsStore = getActorsStore(actor);
 		
		// Create Activity from sample json
		Graph graphToAdd = stringToGraphConverter.convert(RdfFormat.JSONLD, getJsonLdSample(activitySubject, objectSubject, actor.getSubject(), published));
		ActivityPubObject note1Activity = activityPubObjectFactory.create(activitySubject, graphToAdd);
		Activity asActivity = note1Activity.asConvertable().asActivity();
		asActivity.setActor(Set.of(actor.getSubject()));
		assertEquals(activitySubject.toString(), asActivity.getSubject().toString());

		/////////////////////
		// Save initial Activity
		actorsStore.addToOutbox(asActivity, "Save initial Activity");
		
		// Check outbox item
		List<RdfPubIRI> outbox = actorsStore.getOutbox();
		assertEquals(1, outbox.size());
		
		ActivityPubObject rereadInitialActivity = actorsStore.findBySubject(outbox.get(0)).orElseThrow();
		Activity rereadCreateActivity = rereadInitialActivity.asConvertable().asActivity();
		RdfPubBlankNodeOrIRI obj = rereadCreateActivity.getObject().stream().findFirst().orElseThrow();
		ActivityPubObject note = actorsStore.findBySubject(obj).orElseThrow();
		RdfPubBlankNodeOrIRI sameAs = note.getSameAs().orElseThrow().asRdfPubObjectId().orElseThrow().getSubject();
		
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("Note used for update: " + note);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		
		Instant publishedNote = Instant.now();
		note.setPublished(publishedNote);
		note.setContent("Upadted Note");
		RdfPubObjectIdActivity activitySubject2 = activityPubObjectIdBuilder.createActivity(actor.getSubject().asActor().orElseThrow());
		
		Activity updateActivity = getUpdateActivity(activitySubject2, publishedNote);
		updateActivity.addObject(note);
		/////////////////
		// Update 1
		actorsStore.addToOutbox(updateActivity, "Update 1");
		
		///////////////////////
		// Read Create activity
		ActivityPubObject activityAO = actorsStore.findBySubject(outbox.get(0)).orElseThrow();
		Activity activity = activityAO.asConvertable().asActivity();		
		assertTrue(activity.getTypes().contains(vocabContainer.vocAs().Create()));
		
		assertEquals(actor.getSubject().getIRIString(), activity.getActor().stream().findFirst().orElseThrow().getIRIString());		
		assertEquals(Set.of(rdf4j.createIRI("http://example.org/~john/")), activity.getTo() );
		assertEquals(Set.of(rdf4j.createIRI("http://example.com/~erik/followers"),
				rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Public")), activity.getCc());
		assertEquals(Instant.parse(published), activity.getPublished().orElseThrow());
		
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("activity: " + activity);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		
		// Check outbox item
		outbox = actorsStore.getOutbox();
		assertEquals(2, outbox.size());

		/////////////////////////
		// Read Update activity
		Activity updateActivityReRead = actorsStore.findBySubject(outbox.get(1)).orElseThrow().asConvertable().asActivity();
		assertTrue(updateActivityReRead.getTypes().contains(vocabContainer.vocAs().Update()));
		assertEquals(actor.getSubject().getIRIString(), updateActivityReRead.getActor().stream().findFirst().orElseThrow().getIRIString());		
		assertEquals(publishedNote, updateActivityReRead.getPublished().orElseThrow());
		
		////////////////////////////////
		// Read Object of Update activity
		RdfPubBlankNodeOrIRI object = updateActivityReRead.getObject().stream().findFirst().orElseThrow();
		ActivityPubObject objectAO = actorsStore.findBySubject(object).orElseThrow();
		
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("objectAOReRead: " + objectAO);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		
		assertEquals(updateActivityReRead.getSubject().getIRIString(),  objectAO.getWasGeneratedBy().orElseThrow().getIRIString());
		assertEquals(actor.getSubject().getIRIString(), objectAO.getWasAttributedTo().orElseThrow().getIRIString());
		assertTrue(objectAO.getWasRevisionOf().isPresent());
		
		assertEquals(sameAs, objectAO.getWasRevisionOf().orElseThrow());
		
		assertEquals(objectSubject.getIRIString(), objectAO.getSubject().getIRIString());
		assertEquals(Set.of(rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Note"), rdf4j.createIRI("http://www.w3.org/ns/prov#Entity")), objectAO.getTypes());
		assertEquals(Set.of(rdf4j.createIRI("http://example.net/~mallory")), objectAO.getAttributedtTo());
		assertEquals("\"Upadted Note\"", objectAO.getContent().orElseThrow());
		assertEquals(publishedNote, objectAO.getPublished().orElseThrow());
		assertEquals(Set.of(rdf4j.createIRI("http://example.org/~john/")), activity.getTo() );
		assertEquals(Set.of(rdf4j.createIRI("http://example.com/~erik/followers"),
				rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Public")), activity.getCc());

		/////////////////////////
		// Read revision Object (which is initial object)
		ActivityPubObject revision = actorsStore.findBySubject(objectAO.getWasRevisionOf().orElseThrow()).orElseThrow();
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("historicizedObject: " + revision);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		assertEquals(activitySubject.getIRIString(), revision.getWasGeneratedBy().orElseThrow().getIRIString());
		assertEquals(actor.getSubject().getIRIString(), revision.getWasAttributedTo().orElseThrow().getIRIString());
		assertTrue(revision.getWasRevisionOf().isEmpty());
		assertEquals(Set.of(rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Note"), rdf4j.createIRI("http://www.w3.org/ns/prov#Entity")), revision.getTypes());
		assertEquals("\"This is a note\"", revision.getContent().orElseThrow());
		assertEquals(Instant.parse(published), revision.getPublished().orElseThrow());
		assertEquals(Set.of(rdf4j.createIRI("http://example.org/~john/")), activity.getTo() );
		assertEquals(Set.of(rdf4j.createIRI("http://example.com/~erik/followers"),
				rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Public")), activity.getCc());
		
		RdfPubBlankNodeOrIRI deleteActivitySubject = activityPubObjectIdBuilder.createActivity(owner);
		Activity delete = getDeleteActivity(deleteActivitySubject, publishedNote);
		delete.addObject(objectAO.getSubject());
		actorsStore.addToOutbox(delete, "delete");
		
		log.debug("##########################################################################################");
		log.debug("##########################################################################################");
		log.debug("DUMP #####################################################################################");
		log.debug(new GraphToStringConverterDefault(null).convert(RdfFormat.TURTLE, actorsStore.dump()));
	}

	private Activity getUpdateActivity(RdfPubBlankNodeOrIRI activitySubjectIRI, Instant published) {
		Graph graph = rdf4j.createGraph();
		Activity activity = activityPubObjectFactory.create(activitySubjectIRI, graph).asConvertable().asActivity();
		activity.addType(vocabContainer.vocAs().Update());
		activity.addActor(owner);
		activity.setPublished(published);
		return activity;
	}

	private Activity getDeleteActivity(RdfPubBlankNodeOrIRI activitySubjectIRI, Instant published) {
		Graph graph = rdf4j.createGraph();
		Activity activity = activityPubObjectFactory.create(activitySubjectIRI, graph).asConvertable().asActivity();
		activity.addType(vocabContainer.vocAs().Delete());
		activity.addActor(owner);
		activity.setPublished(published);
		return activity;
	}

	private String getJsonLdSample(RdfPubBlankNodeOrIRI activitySubject, RdfPubBlankNodeOrIRI objectSubject, RdfPubBlankNodeOrIRI actor, String published) {
		return """
				{
				  "@context": "https://www.w3.org/ns/activitystreams",
				  "type": "Create",
				  "id": "%s",
				  "actor": "%s",
				  "object": {
				    "id": "%s",
				    "type": "Note",
				    "attributedTo": "http://example.net/~mallory",
				    "content": "This is a note",
				    "published": "%s",
				    "to": ["http://example.org/~john/"],
				    "cc": ["http://example.com/~erik/followers",
				           "http://www.w3.org/ns/activitystreams#Public"]
				  },
				  "published": "%s",
				  "to": ["http://example.org/~john/"],
				  "cc": ["http://example.com/~erik/followers",
				         "https://www.w3.org/ns/activitystreams#Public"]
				}
				""".formatted(activitySubject, actor, objectSubject, published, published);
	}
	
	
	private Actor getSampleActor(RdfPubObjectIdActor actorSubject) {
		String actorString = """
			{
			  "@context": ["https://www.w3.org/ns/activitystreams",
			               {"@language": "ja"}],
			  "type": "Person",
			  "id": "%s",
			  "following": "http://kenzoishii.example.com/following.json",
			  "followers": "http://kenzoishii.example.com/followers.json",
			  "liked": "http://kenzoishii.example.com/liked.json",
			  "inbox": "${inbox}",
			  "outbox": "${outbox}",
			  "preferredUsername": "kenzoishii",
			  "name": "石井健蔵",
			  "summary": "この方はただの例です",
			  "icon": [
			    "http://kenzoishii.example.com/image/165987aklre4"
			  ]
			}				
				""".formatted(actorSubject.getIRIString());
		
		actorString = actorString.replace("${inbox}", activityPubObjectIdBuilder.createCollection(actorSubject, "inbox").getIRIString());
		actorString = actorString.replace("${outbox}", activityPubObjectIdBuilder.createCollection(actorSubject, "outbox").getIRIString());
		
		ActivityPubObject actor = activityPubObjectFactory.create(actorSubject, stringToGraphConverter.convert(RdfFormat.JSONLD, actorString));
		
		Actor asActor = actor.asConvertable().asActor();
		return asActor;
	}
	
	private ActorsStore getActorsStore(Actor owner) {
		return new ActorsStoreDefault(rdf4j, owner, activityPubStore, activityPubObjectFactory, new ServiceForSavingActivities(vocabContainer, rdf4j, activityPubObjectIdBuilder));
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}
