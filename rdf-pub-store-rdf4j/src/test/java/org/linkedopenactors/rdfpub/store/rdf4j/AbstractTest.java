package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Optional;

import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

abstract class AbstractTest {

	private ActivityPubStore activityPubStore;
	private RDF4J rdf4j;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private StringToGraphConverter stringToGraphConverter;

	private StoreRepository storeRepository;
	private RdfPubObjectId owner;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;

	private Store store;
	private VocabContainer vocabContainer;
	
	@BeforeEach
	public void setup() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);
		String external = "http://localhost:8081";
		String internal = "urn:rdfpub:";

		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn(external);
		when(prefixMapper.getInternaleIriPrefix()).thenReturn(internal);
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);

		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);
		owner = activityPubObjectIdBuilder.createActor();
		store = getStore(rdf4j, storeRepository, owner.toString(), owner.getIdentifier());
		
		activityPubObjectFactory = new ActivityPubObjectFactory(vocabContainer, rdf4j,
				new StringToGraphConverterDefault(rdf4j),
				new GraphToStringConverterDefault(prefixMapper), 
				activityPubObjectIdBuilder, prefixMapper);
		stringToGraphConverter = new StringToGraphConverterDefault(rdf4j);
//		Actor actotMock = Mockito.mock(Actor.class);
//		when(actotMock.getName()).thenReturn(Optional.ofNullable(storeOwnerName));
//		when(actotMock.getBaseSubject()).thenReturn(owner);
		activityPubStore = new ActivityPubStoreDefault(vocabContainer, owner, store, activityPubObjectFactory, activityPubObjectIdBuilder);
	}

	protected ActorsStore getActorsStore(Actor owner) {
		ServiceForSavingActivities createHadler = new ServiceForSavingActivities(vocabContainer, rdf4j, activityPubObjectIdBuilder);
		return new ActorsStoreDefault(rdf4j, owner, activityPubStore, activityPubObjectFactory, createHadler);
	}

	protected ActorsStore getOwnersStore() {
		return getActorsStore(getOwner());
	}

	protected Actor getOwner() {
		Actor ownerActor = getSampleActor(owner);
		return ownerActor;
	}

	protected StringToGraphConverter getStringToGraphConverter() {
		return stringToGraphConverter;
	}

	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}

	private Actor getSampleActor(RdfPubObjectId ownerId) {
		String owner = ownerId.toString();
		String outbox = activityPubObjectIdBuilder.createCollection(ownerId, "outbox").getIRIString();
		String actorString = """
			{
			  "@context": ["https://www.w3.org/ns/activitystreams",
			               {"@language": "ja"}],
			  "type": "Person",
			  "id": "%s",
			  "following": "%s/col/following.json",
			  "followers": "%s/col/followers.json",
			  "liked": "%s/col/liked.json",
			  "inbox": "%s/col/inbox.json",
			  "outbox": "%s",
			  "preferredUsername": "kenzoishii",
			  "name": "石井健蔵",
			  "summary": "この方はただの例です",
			  "icon": [
			    "http://kenzoishii.example.com/image/165987aklre4"
			  ]
			}				
				""".formatted(owner, owner, owner, owner, owner, outbox);
		
		ActivityPubObject actor = activityPubObjectFactory.create(ownerId, stringToGraphConverter.convert(RdfFormat.JSONLD, actorString));
		
		Actor asActor = actor.asConvertable().asActor();
		return asActor;
	}
	
	protected RdfPubIRI createIRI(String iri) {
		return activityPubObjectIdBuilder.createFromUrl(iri);
	}

	protected ActivityPubObjectFactory getActivityPubObjectFactory() {
		return activityPubObjectFactory;
	}

	protected Store getStore() {
		return store;
	}
	
	protected ActivityPubObjectIdBuilder getActivityPubObjectIdBuilder() {
		return activityPubObjectIdBuilder;
	}
}
