package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;

public class TestStoreSaveReadCollectionAndAddItem {

	private RDF4J rdf4j;
	private static StoreRepository storeRepository;
	private VocabContainer vocabContainer;
	
	public TestStoreSaveReadCollectionAndAddItem() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);		
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);
	}
	
	@Test
	void test() {
		String storeOwnerName = "actor_" + UUID.randomUUID().toString();
		IRI owner1 = rdf4j.createIRI("https://example.org/" + storeOwnerName);
		Store store = getStore(rdf4j, storeRepository, owner1.toString(), storeOwnerName);
		
		IRI outbox = rdf4j.createIRI(owner1.toString() + "/outbox");
		
		IRI item1 = rdf4j.createIRI(owner1.toString() + "/activity1");
		IRI item2 = rdf4j.createIRI(owner1.toString() + "/activity2");
		IRI item3 = rdf4j.createIRI(owner1.toString() + "/activity3");
		IRI item4 = rdf4j.createIRI(owner1.toString() + "/activity4");
		IRI item5 = rdf4j.createIRI(owner1.toString() + "/activity5");
		IRI item6 = rdf4j.createIRI(owner1.toString() + "/activity6");
		IRI item7 = rdf4j.createIRI(owner1.toString() + "/activity7");
		IRI item8 = rdf4j.createIRI(owner1.toString() + "/activity8");
		List<IRI> items = List.of(item1, item2, item3, item4, item5, item6, item7, item8);
		
		PersistableRdfPubCollection rdfPubCollection = new PersistableRdfPubCollectionDefault(storeOwnerName + ".outbox" ,outbox, items);
		store.saveCollection(owner1, outbox, items, "humanReadableName", "Test saveCollection");
		
		// Read created Collection		
		List<IRI> col = store.getCollection(owner1, rdfPubCollection.getSubject())
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());

		
		assertEquals(8, col.size());
		assertEquals(item1, col.get(0));
		assertEquals(item2, col.get(1));
		assertEquals(item3, col.get(2));
		assertEquals(item4, col.get(3));
		
		String addedBaseUrl = owner1.toString() + "/activity_added_";
		// Add Item
		for (int i = 0; i < 10; i++) {			
			IRI addedItem = rdf4j.createIRI(addedBaseUrl + i);
			store.addItem(owner1, outbox, "humanReadableName", addedItem, "Test addItem");
		}

		col = store.getCollection(owner1, rdfPubCollection.getCollectionSubject())
				.map(PersistableRdfPubCollection::getItems)
				.orElse(Collections.emptyList());
		
		assertEquals(items.size() + 10, col.size());
		assertEquals(item1, col.get(0));
		assertEquals(item2, col.get(1));
		assertEquals(item3, col.get(2));
		assertEquals(item4, col.get(3));
		assertEquals(item5, col.get(4));
		assertEquals(item6, col.get(5));
		assertEquals(item7, col.get(6));
		assertEquals(item8, col.get(7));
		
		for (int i = 0; i < 10; i++) {			
			IRI addedItem = rdf4j.createIRI(addedBaseUrl + i);
			assertEquals(addedItem, col.get(items.size()+i));
		}
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}

