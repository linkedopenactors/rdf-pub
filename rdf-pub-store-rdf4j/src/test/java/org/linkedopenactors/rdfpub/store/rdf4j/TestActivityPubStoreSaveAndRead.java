package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.Triple;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.Activity;
import org.linkedopenactors.rdfpub.domain.ActivityPubObject;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectFactory;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.ActivityPubStore;
import org.linkedopenactors.rdfpub.domain.Actor;
import org.linkedopenactors.rdfpub.domain.ActorsStore;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.commonsrdf.RdfFormat;
import org.linkedopenactors.rdfpub.domain.commonsrdf.StringToGraphConverter;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubBlankNodeOrIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubIRI;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestActivityPubStoreSaveAndRead {

	private ActivityPubStore activityPubStore;
	private RDF4J rdf4j;
	private ActivityPubObjectFactory activityPubObjectFactory;
	private StringToGraphConverter stringToGraphConverter;
	private StoreRepository storeRepository;
	private RdfPubObjectId owner;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private VocabContainer vocabContainer;
	
	@BeforeEach
	public void setup() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer , rdf4j, datasetRepository);

		String external = "http://localhost:8081";
		String internal = "urn:rdfpub:";
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn(external);
		when(prefixMapper.getInternaleIriPrefix()).thenReturn(internal);
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);

		owner = activityPubObjectIdBuilder.createActor();
//		String storeOwnerName = "actor_" + RandomStringUtils.random(6, true, false);
		Store store = getStore(rdf4j, storeRepository, owner.toString(), owner.getIdentifier());
	

		activityPubObjectFactory = new ActivityPubObjectFactory(vocabContainer , rdf4j, new StringToGraphConverterDefault(rdf4j),
				new GraphToStringConverterDefault(prefixMapper), activityPubObjectIdBuilder, prefixMapper);
		stringToGraphConverter = new StringToGraphConverterDefault(rdf4j);
//		Actor actotMock = Mockito.mock(Actor.class);
//		when(actotMock.getName()).thenReturn(Optional.ofNullable(storeOwnerName));
//		when(actotMock.getBaseSubject()).thenReturn(owner);
//		when(actotMock.getPreferredUsername()).thenReturn(Optional.of(storeOwnerName));
		activityPubStore = new ActivityPubStoreDefault(vocabContainer, owner, store, activityPubObjectFactory, activityPubObjectIdBuilder);
	}

	@Test
	void test() {
		String published = "2015-02-10T15:04:55Z";
		IRI activitySubject = rdf4j.createIRI("http://example.org/~mallory/" + UUID.randomUUID().toString());
		IRI objectSubject = rdf4j.createIRI("http://example.org/~mallory/note_" + UUID.randomUUID().toString());
		Actor actor = getSampleActor(owner);
		ActorsStore actorsStore = getActorsStore(actor);
		assertTrue(actorsStore.getOutbox().isEmpty());		
		
		// Create Activity from sample json
		String jsonLdSample = getJsonLdSample(activitySubject.toString(), objectSubject.toString(), actor.getSubject().toString(), published);
		ActivityPubObject note1Activity = activityPubObjectFactory.create(RdfFormat.JSONLD, jsonLdSample);
		Activity asActivity = note1Activity.asConvertable().asActivity();
		
//		asActivity.resolveObject().forEach(o->{
//			o.replaceSubjects(actor.getSubject().asActor().orElseThrow());
//			});
		
		
		asActivity.replaceSubjects(actor.getSubject().asActor().orElseThrow(), true);
		asActivity.setActor(Set.of(actor.getSubject()));

		// Save Activity
		actorsStore.addToOutbox(asActivity, "Save Activity");
		
		// Check outbox item
		List<RdfPubIRI> outbox = actorsStore.getOutbox();
		assertEquals(1, outbox.size());
		
		// Read activity
		ActivityPubObject activityAO = actorsStore.findBySubject(outbox.get(0)).orElseThrow();
		Activity activity = activityAO.asConvertable().asActivity();
		assertTrue(activity.getTypes().contains(rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Create")));
		assertEquals(actor.getSubject().getIRIString(), activity.getActor().stream().findFirst().orElseThrow().getIRIString());		
		assertEquals(Set.of(rdf4j.createIRI("http://example.org/~john/")), activity.getTo() );
		assertEquals(Set.of(rdf4j.createIRI("http://example.com/~erik/followers"),
				rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Public")), activity.getCc());
		assertEquals(Instant.parse(published), activity.getPublished().orElseThrow());
		
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("activity: " + activity);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		
		// Read Object of activity
		RdfPubBlankNodeOrIRI object = activity.getObject().stream().findFirst().orElseThrow();
		ActivityPubObject objectAO = actorsStore.findBySubject(object).orElseThrow();
		assertEquals(activity.getSubject().getIRIString(),  objectAO.getWasGeneratedBy().orElseThrow().getIRIString());
		assertEquals(actor.getSubject().getIRIString(), objectAO.getWasAttributedTo().orElseThrow().getIRIString());
		assertEquals(Optional.empty(), objectAO.getWasRevisionOf());
		assertFalse(objectAO.getSameAs().isEmpty());
		assertEquals(Set.of(rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Note"), rdf4j.createIRI("http://www.w3.org/ns/prov#Entity")), objectAO.getTypes());
		assertEquals(Set.of(rdf4j.createIRI("http://example.net/~mallory")), objectAO.getAttributedtTo());
		assertEquals("\"This is a note\"", objectAO.getContent().orElseThrow());
		assertEquals(Instant.parse(published), objectAO.getPublished().orElseThrow());
		assertEquals(Set.of(rdf4j.createIRI("http://example.org/~john/")), activity.getTo() );
		assertEquals(Set.of(rdf4j.createIRI("http://example.com/~erik/followers"),
				rdf4j.createIRI("https://www.w3.org/ns/activitystreams#Public")), activity.getCc());
		
		log.debug("\n\n\n\ngraph start #####################################################################################");
		log.debug("object: " + objectAO);
		log.debug("\ngraph end  #####################################################################################\n\n\n\n");
		
		
		RdfPubBlankNodeOrIRI sameAs = objectAO.getSameAs().orElseThrow();
		ActivityPubObject sameAsObject = actorsStore.findBySubject(sameAs).orElseThrow();
		Graph objectAOGraph = objectAO.asGraph();
		Graph asGraph = sameAsObject.asGraph();
		asGraph.stream().forEach(triple->{			
			Set<Triple> t = objectAOGraph.stream(objectAO.getSubject(), triple.getPredicate(), triple.getObject()).collect(Collectors.toSet());
			assertTrue(t.size()==1);
			assertEquals(triple.getObject(), t.stream().findFirst().orElseThrow().getObject());
		});
	}

	boolean isSame(Triple a, Triple b) {
		return a.getObject().equals(b.getObject());
	}
	
//	@Test
//	void testAlreadyExisting() {
//		String published = "2015-02-10T15:04:55Z";
//		IRI activitySubject = rdf4j.createIRI("http://example.net/~mallory/" + UUID.randomUUID().toString());
//		IRI objectSubject = rdf4j.createIRI("http://example.net/~mallory/note_" + UUID.randomUUID().toString());
//		Actor actor = getSampleActor(owner.toString());
//		ActorsStore actorsStore = getActorsStore(actor);
//		assertTrue(actorsStore.getOutbox().isEmpty());		
//		
//		// Create Activity from sample json
//		Graph graphToAdd = stringToGraphConverter.convert(RdfFormat.JSONLD, getJsonLdSample(activitySubject.toString(), objectSubject.toString(), actor.getSubject().toString(), published));
//		ActivityPubObject note1Activity = activityPubObjectFactory.create(activitySubject, graphToAdd);
//		Activity asActivity = note1Activity.asConvertable().asActivity();
//		asActivity.setActor(Set.of(actor.getSubject()));
//		assertEquals(activitySubject.toString(), asActivity.getSubject().toString());
//
//		// Save Activity
//		actorsStore.addToOutbox(asActivity);
//		
//		IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
//	            ()->{
//	            	actorsStore.addToOutbox(asActivity);
//	            } );
//		String expected = "You cannot create objects, that are already exists: [";
//		assertTrue(exception.getMessage().toString().startsWith(expected));
//	}

	private String getJsonLdSample(String activitySubject, String objectSubject, String actor, String published) {
		return """
				{
				  "@context": "https://www.w3.org/ns/activitystreams",
				  "type": "Create",
				  "id": "%s",
				  "actor": "%s",
				  "object": {
				    "id": "%s",
				    "type": "Note",
				    "name": "Sample Note",
				    "attributedTo": "http://example.net/~mallory",
				    "content": "This is a note",
				    "published": "%s",
				    "to": ["http://example.org/~john/"],
				    "cc": ["http://example.com/~erik/followers",
				           "http://www.w3.org/ns/activitystreams#Public"]
				  },
				  "published": "%s",
				  "to": ["http://example.org/~john/"],
				  "cc": ["http://example.com/~erik/followers",
				         "https://www.w3.org/ns/activitystreams#Public"]
				}
				""".formatted(activitySubject, actor, objectSubject, published, published);
	}
	
	
	private Actor getSampleActor(RdfPubBlankNodeOrIRI actorSubject) {
		String actorString = """
			{
			  "@context": ["https://www.w3.org/ns/activitystreams",
			               {"@language": "ja"}],
			  "type": "Person",
			  "id": "%s",
			  "following": "https://example.org/following.json",
			  "followers": "https://example.org/followers.json",
			  "liked": "urn:rdfpub:liked.json",
			  "inbox": "%s/col/INBOX",
			  "outbox": "%s/col/OUTBOX",
			  "preferredUsername": "kenzoishii",
			  "name": "石井健蔵",
			  "summary": "この方はただの例です",
			  "icon": [
			    "urn:rdfpub:image/165987aklre4"
			  ]
			}				
				""".formatted(actorSubject.getIRIString(), actorSubject.getIRIString(), actorSubject.getIRIString());
		
		ActivityPubObject actor = activityPubObjectFactory.create(actorSubject, stringToGraphConverter.convert(RdfFormat.JSONLD, actorString));
		
		Actor asActor = actor.asConvertable().asActor();
		return asActor;
	}
	
	private ActorsStore getActorsStore(Actor owner) {
		return new ActorsStoreDefault(rdf4j, owner, activityPubStore, activityPubObjectFactory, new ServiceForSavingActivities(vocabContainer , rdf4j, activityPubObjectIdBuilder));
	}
	
	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}
