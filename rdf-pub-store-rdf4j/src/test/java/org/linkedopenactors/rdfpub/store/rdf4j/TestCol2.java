package org.linkedopenactors.rdfpub.store.rdf4j;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Optional;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.rdfpub.domain.ActivityPubObjectIdBuilder;
import org.linkedopenactors.rdfpub.domain.PersistableRdfPubCollection;
import org.linkedopenactors.rdfpub.domain.commonsrdf.PrefixMapper;
import org.linkedopenactors.rdfpub.domain.iri.ActivityPubObjectIdBuilderFactory4Test;
import org.linkedopenactors.rdfpub.domain.iri.RdfPubObjectId;
import org.linkedopenactors.rdfpub.domain.iri.VocabContainer;
import org.linkedopenactors.rdfpub.domain.iri.vocab.VocabContainerDefault;
import org.linkedopenactors.rdfpub.store.DatasetRepository;
import org.linkedopenactors.rdfpub.store.Store;
import org.linkedopenactors.rdfpub.store.StoreRepository;
import org.mockito.Mockito;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 */
@Slf4j
public class TestCol2 {

	private RDF4J rdf4j;
	private StoreRepository storeRepository;
	private RdfPubObjectId owner;
	private ActivityPubObjectIdBuilder activityPubObjectIdBuilder;
	private Store store;
	private VocabContainer vocabContainer;
	
	/**
	 * 
	 */
	@BeforeEach
	public void setup() {
		rdf4j = new RDF4J();
		vocabContainer = new VocabContainerDefault(rdf4j);
		DatasetRepository datasetRepository = new Rdf4JRepositoryManagerWrapper(rdf4j, new File("target/rdf4jstore/datadir/"));
		storeRepository = new StoreRepositoryDefault(vocabContainer, rdf4j, datasetRepository);

		String external = "http://localhost:8081";
		String internal = "urn:rdfpub:";
		
		PrefixMapper prefixMapper = Mockito.mock(PrefixMapper.class);
		when(prefixMapper.getExternalIriPrefix()).thenReturn(external);
		when(prefixMapper.getInternaleIriPrefix()).thenReturn(internal);
		activityPubObjectIdBuilder = ActivityPubObjectIdBuilderFactory4Test.create(rdf4j, prefixMapper);

		owner = activityPubObjectIdBuilder.createActor();
		store = getStore(rdf4j, storeRepository, owner.toString(), owner.getIdentifier());
	}

	@Test
	void test() {
		IRI colIri = rdf4j.createIRI("urn:test/collection");
		IRI item1 = rdf4j.createIRI("urn:test/item1");
		IRI item2 = rdf4j.createIRI("urn:test/item2");
		IRI item3 = rdf4j.createIRI("urn:test/item3");
		
		PersistableRdfPubCollection col1 = store.getCollection(owner, colIri).orElseThrow();		
		assertTrue(col1.getItems().isEmpty());
		
		store.addItem(owner, colIri, "myItem", item1, "adding item1");
		store.addItem(owner, colIri, "myItem", item2, "adding item2");
		store.addItem(owner, colIri, "myItem", item3, "adding item3");
		col1 = store.getCollection(owner, colIri).orElseThrow();
		assertEquals(3, col1.getItems().size());
		assertEquals(item1, col1.getItems().get(0));
		assertEquals(item2, col1.getItems().get(1));
		assertEquals(item3, col1.getItems().get(2));
		
		store.removeItem(owner, colIri, "was?", item2, "rem i2");
		col1 = store.getCollection(owner, colIri).orElseThrow();
		assertEquals(2, col1.getItems().size());
		assertEquals(item1, col1.getItems().get(0));
		assertEquals(item3, col1.getItems().get(1));
	}

	private org.linkedopenactors.rdfpub.store.Store getStore(RDF4J rdf4j, StoreRepository storeRepository,
			String datasetName, String storeOwnerName) {
		org.linkedopenactors.rdfpub.store.Store store;
		Optional<org.linkedopenactors.rdfpub.store.Store> storeOpt = storeRepository.findStore(rdf4j.createIRI(datasetName), storeOwnerName);
		if(storeOpt.isEmpty()) {
			store = storeRepository.createStore(rdf4j.createIRI(datasetName), storeOwnerName);
		} else {
			store = storeOpt.get();
		}
		return store;
	}
}
