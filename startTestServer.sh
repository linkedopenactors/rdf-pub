#! /bin/bash
set -x
cd ~/git
mkdir -p test-server
cd test-server
git clone https://gitlab.com/linkedopenactors/rdf-pub.git
cd rdf-pub
#git checkout 0.0.6
git checkout feature/pending-following
mvn clean install -DskipTests
cd rdf-pub-spring-boot
cd target
java -jar -Dspring.profiles.active=test-server -Dinstance.domain=https://server.test.rdf-pub.org -Dinstance.identifier=68a7043f-9604-42ed-bba9-f78426d613f4 -Dserver.port=8903 app.jar

# achtung, das profil gibt es ja in älteren versionen gar nicht!


